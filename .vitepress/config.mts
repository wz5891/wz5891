import { defineConfig } from 'vitepress'
import nav from './nav.mjs'
import sidebar from './sidebar.mjs'


// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "IT杂记",
  description: "IT杂记，记录学习",
  appearance: 'dark',
  ignoreDeadLinks:true,
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav,
    sidebar:[],
    footer: {
      message: '<a href="https://beian.miit.gov.cn" target="_blank">粤ICP备2024301743号</a>',
      copyright: 'Copyright © 2024 wz'
    },

    socialLinks: [
      { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
    ]
  }
})
