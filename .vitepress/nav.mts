export default [
    {
        text: '开发',
        items: [
            { text: 'Maven打包可执行jar', link: 'dev/java/maven打包可执行jar.md' },
            { text: 'Java读取命令行参数', link: 'dev/java/读取命令行参数.md' },
            { text: 'Maven使用${revision}实现多模块版本统一管理', link: 'dev/java/mavenRevision.md' },
            { text: 'Ftp中文路径乱码处理', link: 'dev/java/ftp_encod_problem.md' },
            { text: 'rabbitmq', link: 'dev/middleware/rabbitmq.md' },
            { text: 'pdf解析', link: 'dev/python/pdf解析.md' },
            { text: '依赖注入', link: 'dev/spring/dependencyInjection.md' },
            { text: 'Springboot启动后执行方法', link: 'dev/spring/afterstart.md' },
            { text: 'MyBatis日志', link: 'dev/spring/mybatis_log.md' },
            { text: 'HTTP Server', link: 'dev/node/http-server.md' }
        ]
    },
    {
        text: '部署',
        items: [
            { text: 'Linux维护', link: 'deploy/linux.md' },
            { text: 'Docker-compose部署Mongo', link: 'deploy/mongo.md' },
            { text: 'Windows安装Nginx', link: 'deploy/nginx-win-service.md' },
            { text: 'Windows安装docker desktop', link: 'deploy/windows-docker.md' },
            { text: '修改Docker存储目录', link: 'deploy/docker/change_docker_data_path.md' },
            { text: 'RabbitMq Docker entrypoint', link: 'deploy/docker/rabbit_mq_docker_entrypoint.md' },
            { text: 'RabbitMq SSL', link: 'deploy/ssl/rabbitmq.md' },
            { text: 'Jenkins安装', link: 'deploy/jenkins/setup.md' },
            { text: 'Jenkins流水线', link: 'deploy/jenkins/pipline.md' },
            { text: 'Jenkins权限管理', link: 'deploy/jenkins/permission.md' }
        ]
    },
    {
        text: '电子',
        items: [
            { text: 'DT830B', link: 'electronic/instrument/dt830b.md' }
        ]
    },
    {
        text: 'Docker',
        items: [
            { text: '调整Docker存储目录', link: 'docker/change_docker_data_path.md' },
            { text: '离线安装 Docker Rootless 准备篇', link: 'docker/centos_install_rootless_docker_offline_prepare.md' }
        ]
    },
    {
        text: 'AI',
        items: [
            { text: 'Qwen', link: 'ai/qwen.md' },
            { text: 'Ollama', link: 'ai/ollama.md' },
            { text: 'CodeShell', link: 'ai/codeshell.md' }
        ]
    },
    {
        text: 'Android',
        items: [
            { text: 'Note', link: 'android/note.md' }
        ]
    },
    {
        text: 'Business',
        items: [
            { text: 'PM', link: 'pm.md' }
        ]
    },
    {
        text: 'CentOS',
        items: [
            { text: 'Add New Disk', link: 'centos/add_new_disk.md' },
            { text: 'FTP Put Folder', link: 'centos/ftp_put_folder.md' },
            { text: 'Top', link: 'centos/top.md' },
            { text: 'Tree', link: 'centos/tree.md' }
        ]
    },
    {
        text: 'Design',
        items: [
            { text: 'Html基础', link: 'ppt/Html基础.md' },
            { text: 'Css基础', link: 'ppt/Css基础.md' }
        ]
    },
    {
        text: 'Go',
        items: [
            { text: 'How to Build', link: 'go/how_to_build.md' }
        ]
    },
    {
        text: 'Jenkins',
        items: [
            { text: 'API', link: 'deploy/jenkins/api/api.md' }
        ]
    },
    {
        text: 'Life',
        items: [
            { text: 'Home Movie', link: 'life/home-movice.md' }
        ]
    },
    {
        text: 'Lowcode',
        items: [
            { text: 'Yida', link: 'lowcode/yida.md' }
        ]
    },
    {
        text: 'Maven',
        items: [
            { text: 'System Scope', link: 'maven/system_scope.md' }
        ]
    },
    {
        text: 'Mongo',
        items: [
            { text: 'MongoDB Setup', link: 'mongo/mongodb-4-0-10-setup.md' }
        ]
    },
    {
        text: 'MyBatis',
        items: [
            { text: 'MyBatis Adapter Multiple DB', link: 'mybatis/mybatis_adapter_multiple_db.md' }
        ]
    },
    {
        text: 'MySQL',
        items: [
            { text: 'MySQL Setup', link: 'mysql/mysql_setup.md' }
        ]
    },
    {
        text: 'Network',
        items: [
            { text: 'Network Setup', link: 'network/network_setup.md' }
        ]
    },
    {
        text: 'NFS',
        items: [
            { text: 'NFS Setup', link: 'nfs/nfs_setup.md' }
        ]
    },
    {
        text: 'Nginx',
        items: [
            { text: 'Nginx Setup', link: 'nginx/nginx_setup.md' }
        ]
    },
    {
        text: 'Node',
        items: [
            { text: 'HTTP Server', link: 'dev/node/http-server.md' }
        ]
    },
    {
        text: 'Numpy',
        items: [
            { text: 'Numpy Setup', link: 'numpy/numpy_setup.md' }
        ]
    },
    {
        text: 'OrangePi',
        items: [
            { text: 'Start', link: 'orangepi/start.md' }
        ]
    },
    {
        text: 'Other',
        items: [
            { text: 'WebDAV', link: 'other/webdav.md' }
        ]
    },
    {
        text: 'PCSoft',
        items: [
            { text: 'PCSoft Framework', link: 'pcsoft/pcsoft_framework.md' }
        ]
    },
    {
        text: 'PDF',
        items: [
            { text: 'Start', link: 'pdf/start.md' }
        ]
    },
    {
        text: 'Performance',
        items: [
            { text: 'Arthas SpringBoot', link: 'perfomance/arthas_springboot.md' }
        ]
    },
    {
        text: 'Pi',
        items: [
            { text: 'Start', link: 'pi/start.md' }
        ]
    },
    {
        text: 'PowerDesign',
        items: [
            { text: 'PowerDesign Setup', link: 'powerdesign/powerdesign_setup.md' }
        ]
    },
    {
        text: 'Printer',
        items: [
            { text: 'Printer Setup', link: 'printer/printer_setup.md' }
        ]
    },
    {
        text: 'Proguard',
        items: [
            { text: 'Begin', link: 'proguard/begin.md' }
        ]
    },
    {
        text: 'Python',
        items: [
            { text: 'Anaconda', link: 'python/anaconda.md' },
            { text: 'FastAPI Docs CDN', link: 'python/fastapi/docs-cdn.md' },
            { text: 'Jupyter Notebook', link: 'python/jupyter_notebook.md' },
            { text: 'PDF解析', link: 'dev/python/pdf解析.md' },
            { text: 'Set Index URL', link: 'python/set_index_url.md' }
        ]
    },
    {
        text: 'RabbitMQ',
        items: [
            { text: 'RabbitMQ Setup', link: 'rabbitmq/rabbitmq_setup.md' }
        ]
    },
    {
        text: 'Secret',
        items: [
            { text: 'Secret Setup', link: 'secret/secret_setup.md' }
        ]
    },
    {
        text: 'SpringBoot',
        items: [
            { text: 'Config File', link: 'springboot/config_file.md' },
            { text: 'Logging Config', link: 'springboot/logging_config.md' }
        ]
    },
    {
        text: 'Superset',
        items: [
            { text: 'Superset Setup', link: 'superset/superset_setup.md' }
        ]
    },
    {
        text: 'Test',
        items: [
            { text: 'Test', link: 'test.md' }
        ]
    },
    {
        text: 'Ubuntu',
        items: [
            { text: 'Ubuntu Setup', link: 'ubuntu/ubuntu_setup.md' }
        ]
    },
    {
        text: 'UOS',
        items: [
            { text: 'Add New Disk', link: 'uos/add_new_disk.md' }
        ]
    },
    {
        text: 'Xingchuan',
        items: [
            { text: 'DM DB', link: 'xingchuan/dm_db.md' },
            { text: 'Project1', link: 'xingchuan/project1.md' },
            { text: 'ZKFD', link: 'xingchuan/zkfd.md' }
        ]
    }
]