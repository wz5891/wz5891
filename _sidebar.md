* 部署
    * [Linux维护](deploy/linux.md)
    * [Docker-compose部署Mongo](deploy/mongo.md)
    * [Windows安装Nginx](deploy/nginx-win-service.md)
    * [Windows安装docker desktop](deploy/windows-docker.md)
    * [修改Docker存储目录](deploy/docker/change_docker_data_path.md)
    * [RabbitMq Docker entrypoint](deploy/docker/rabbit_mq_docker_entrypoint.md)
    * [RabbitMq SSL](deploy/ssl/rabbitmq.md)
    * Jenkins
        * [Jenkins安装](deploy/jenkins/setup.md)
        * [Jenkins流水线](deploy/jenkins/pipline.md)
        * [Jenkins权限管理](deploy/jenkins/permission.md)
* 开发
    * Java
        * [Maven打包可执行jar](dev/java/maven打包可执行jar.md)
        * [Java读取命令行参数](dev/java/读取命令行参数.md)
        * [Ftp中文路径乱码处理](dev/java/ftp_encod_problem.md)
        * [Maven使用${revision}实现多模块版本统一管理](dev/java/mavenRevision.md)
    * 中间件
        * [rabbitmq](dev/middleware/rabbitmq.md)
    * Python
        * [pdf解析](dev/python/pdf解析.md)
    * Spring
        * [依赖注入](dev/spring/dependencyInjection.md)
        * [Springboot启动后执行方法](dev/spring/afterstart.md)
        * [MyBatis日志](dev/spring/mybatis_log.md)
    * Node
        * [HTTP Server](dev/node/http-server.md)
* 电子
    * 万用表
        * [DT830B](electronic/instrument/dt830b.md)
* Docker
    * [调整Docker存储目录](docker/change_docker_data_path.md)
    * [离线安装 Docker Rootless 准备篇](docker/centos_install_rootless_docker_offline_prepare.md)
* AI
    * [Qwen](ai/qwen.md)
    * [Ollama](ai/ollama.md)
    * [CodeShell](ai/codeshell.md)
* Android
    * [Note](android/note.md)
* Business
    * [PM](pm.md)
* CentOS
    * [Add New Disk](centos/add_new_disk.md)
    * [FTP Put Folder](centos/ftp_put_folder.md)
    * [Top](centos/top.md)
    * [Tree](centos/tree.md)
* Design
    * [Html基础](ppt/Html基础.md)
    * [Css基础](ppt/Css基础.md)
* Go
    * [How to Build](go/how_to_build.md)
* Jenkins
    * [API](deploy/jenkins/api/api.md)
* Life
    * [Home Movie](life/home-movice.md)
* Lowcode
    * [Yida](lowcode/yida.md)
* Maven
    * [System Scope](maven/system_scope.md)
* Mongo
    * [MongoDB Setup](mongo/mongodb-4-0-10-setup.md)
* MyBatis
    * [MyBatis Adapter Multiple DB](mybatis/mybatis_adapter_multiple_db.md)
* MySQL
    * [MySQL Setup](mysql/mysql_setup.md)
* Network
    * [Network Setup](network/network_setup.md)
* NFS
    * [NFS Setup](nfs/nfs_setup.md)
* Nginx
    * [Nginx Setup](nginx/nginx_setup.md)
* Node
    * [HTTP Server](dev/node/http-server.md)
* Numpy
    * [Numpy Setup](numpy/numpy_setup.md)
* OrangePi
    * [Start](orangepi/start.md)
* Other
    * [WebDAV](other/webdav.md)
* PCSoft
    * [PCSoft Framework](pcsoft/pcsoft_framework.md)
* PDF
    * [Start](pdf/start.md)
* Performance
    * [Arthas SpringBoot](perfomance/arthas_springboot.md)
* Pi
    * [Start](pi/start.md)
* PowerDesign
    * [PowerDesign Setup](powerdesign/powerdesign_setup.md)
* Printer
    * [Printer Setup](printer/printer_setup.md)
* Proguard
    * [Begin](proguard/begin.md)
* Python
    * [Anaconda](python/anaconda.md)
    * [FastAPI Docs CDN](python/fastapi/docs-cdn.md)
    * [Jupyter Notebook](python/jupyter_notebook.md)
    * [PDF解析](dev/python/pdf解析.md)
    * [Set Index URL](python/set_index_url.md)
* RabbitMQ
    * [RabbitMQ Setup](rabbitmq/rabbitmq_setup.md)
* Secret
    * [Secret Setup](secret/secret_setup.md)
* SpringBoot
    * [Config File](springboot/config_file.md)
    * [Logging Config](springboot/logging_config.md)
* Superset
    * [Superset Setup](superset/superset_setup.md)
* Test
    * [Test](test.md)
* Ubuntu
    * [Ubuntu Setup](ubuntu/ubuntu_setup.md)
* UOS
    * [Add New Disk](uos/add_new_disk.md)
* Xingchuan
    * [DM DB](xingchuan/dm_db.md)
    * [Project1](xingchuan/project1.md)
    * [ZKFD](xingchuan/zkfd.md)