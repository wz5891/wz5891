


关于查看自己电脑上的CUDA版本，首先需要区分CUDA的两种API，即运行时API(Runtime API)和驱动API(Driver API)

1. 驱动API(Driver API)
驱动API(Driver API)由GPU driver installer安装

命令行输入nvidia-smi

```
C:\Users\Administrator>nvidia-smi
Mon May 20 17:09:47 2024
+---------------------------------------------------------------------------------------+
| NVIDIA-SMI 531.26                 Driver Version: 531.26       CUDA Version: 12.1     |
|-----------------------------------------+----------------------+----------------------+
| GPU  Name                      TCC/WDDM | Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf            Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                                         |                      |               MIG M. |
|=========================================+======================+======================|
|   0  NVIDIA GeForce RTX 3060 L...  WDDM | 00000000:03:00.0  On |                  N/A |
| N/A   34C    P8               13W /  N/A|    795MiB / 12288MiB |     14%      Default |
|                                         |                      |                  N/A |
+-----------------------------------------+----------------------+----------------------+

+---------------------------------------------------------------------------------------+
| Processes:                                                                            |
|  GPU   GI   CI        PID   Type   Process name                            GPU Memory |
|        ID   ID                                                             Usage      |
|=======================================================================================|
|    0   N/A  N/A      5012    C+G   ...5n1h2txyewy\ShellExperienceHost.exe    N/A      |
|    0   N/A  N/A      5484    C+G   ...1.22.6019\updated_web\WXWorkWeb.exe    N/A      |
|    0   N/A  N/A      6360    C+G   D:\Program Files\Motrix\Motrix.exe        N/A      |
|    0   N/A  N/A      6920    C+G   ...am Files\Microsoft VS Code\Code.exe    N/A      |
|    0   N/A  N/A      9936    C+G   C:\Windows\explorer.exe                   N/A      |
|    0   N/A  N/A     11788    C+G   ....Search_cw5n1h2txyewy\SearchApp.exe    N/A      |
|    0   N/A  N/A     12780    C+G   ...CBS_cw5n1h2txyewy\TextInputHost.exe    N/A      |
|    0   N/A  N/A     13700    C+G   ...GeForce Experience\NVIDIA Share.exe    N/A      |
|    0   N/A  N/A     14808    C+G   ...crosoft\Edge\Application\msedge.exe    N/A      |
|    0   N/A  N/A     15904      C   ...ta\Local\Programs\Ollama\ollama.exe    N/A      |
|    0   N/A  N/A     16020    C+G   ...siveControlPanel\SystemSettings.exe    N/A      |
|    0   N/A  N/A     18144    C+G   ...9\FlutterPlugins\FlutterPlugins.exe    N/A      |
+---------------------------------------------------------------------------------------+



```

GeForce RTX 3060 Laptop GPU

右上角的CUDA Version便是CUDA Driver API版本



https://blog.csdn.net/bruce_zhao1407/article/details/109580835
https://blog.csdn.net/chen565884393/article/details/127905428


https://blog.csdn.net/rxxacm/article/details/133828727

https://blog.51cto.com/u_15746412/7002097

2. 运行时API(Runtime API)
```
C:\Users\Administrator>nvcc -V
nvcc: NVIDIA (R) Cuda compiler driver
Copyright (c) 2005-2022 NVIDIA Corporation
Built on Wed_Sep_21_10:41:10_Pacific_Daylight_Time_2022
Cuda compilation tools, release 11.8, V11.8.89
Build cuda_11.8.r11.8/compiler.31833905_0
```

最下边一行便是CUDA Runtime API的版本


注意：Driver API和Runtime API可以不同，具体应用要看使用场景




## 安装
https://developer.nvidia.cn/cuda-downloads
https://developer.nvidia.com/cudnn-archive
https://www.bilibili.com/video/BV19e411a7Rn/?spm_id_from=autoNext&vd_source=40510ac0404b7447890f3e60cbf0dee8

```
conda install pytorch torchvision torchaudio pytorch-cuda=11.8 -c pytorch -c nvidia
```

```
import torch
>>> print(torch.__version__)
2.3.0+cu121
>>> print(torch.cuda.is_available())
True
```


https://blog.csdn.net/qq_23341175/article/details/121539231



## fastai 使用gpu
安装pytorch
```
pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118
```

安装 fastai
```
pip install fastai
```

查看是否支持GPU
```
import torch
print(torch.cuda.is_available())
print(torch.__version__)
print(torch.version.cuda)
print(torch.backends.cudnn.version())
```

运行
```
from fastai.text.all import *

path = untar_data(URLs.IMDB)
dls = TextDataLoaders.from_folder(path, valid='test', bs=16)
learn = text_classifier_learner(dls, AWD_LSTM, drop_mult=0.5, metrics=accuracy)
learn.fine_tune(4, 1e-2)
learn.predict("I really liked that movie!")
```



GPU Monitoring
Watch the usage stats as their change

```
nvidia-smi --query-gpu=timestamp,pstate,temperature.gpu,utilization.gpu,utilization.memory,memory.total,memory.free,memory.used --format=csv -l 1
```

https://docs.fast.ai/dev/gpu.html