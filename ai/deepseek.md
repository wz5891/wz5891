## Ollama

```sh
curl -fsSL https://ollama.com/install.sh | sh
```

### 修改模型存放位置

#### 一、关闭Ollama服务

在更改模型路径之前，需要先停止Ollama服务。可以通过以下命令停止服务：

1. 使用 `systemctl` 命令：
    ```sh
    sudo systemctl stop ollama
    sudo systemctl disable ollama.service
    ```
2. 或者在运行Ollama的终端中按 `Ctrl+C` 手动停止服务。

#### 二、默认模型路径

在Linux系统中，Ollama默认的模型存储路径为：
```sh
/usr/share/ollama/.ollama/models
```

#### 三、更改模型路径

1. 创建新的模型存储目录：
    首先，创建一个新的目录作为模型存储路径。例如，创建 `/data/ollama/models` 目录：
    ```sh
    sudo mkdir -p /data/ollama/models
    ```
2. 更改目录权限：
    确保新目录的权限设置正确，允许Ollama访问和写入：
    ```sh
    sudo chown -R root:root /data/ollama/models
    sudo chmod -R 775 /data/ollama/models
    ```

#### 四、修改Ollama服务配置文件

1. 编辑服务配置文件：
    使用文本编辑器（如 `vim`）编辑Ollama服务的配置文件：
    ```sh
    sudo gedit /etc/systemd/system/ollama.service
    ```
2. 修改配置内容：
    在 `[Service]` 部分的 `Environment` 字段后，添加新的 `Environment` 字段，指定新的模型路径：
    ```ini
    Environment="OLLAMA_MODELS=/data/ollama/models"
    ```
    完整的配置示例如下：
    ```ini
    [Unit]
    Description=Ollama Service
    After=network-online.target

    [Service]
    ExecStart=/usr/local/bin/ollama serve
    User=root
    Group=root
    Restart=always
    RestartSec=3
    Environment="PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
    Environment="OLLAMA_MODELS=/data/ollama/models"

    [Install]
    WantedBy=default.target
    ```

#### 五、重载配置并重启Ollama服务

1. 重载系统服务配置：
    ```sh
    sudo systemctl daemon-reload
    ```
2. 重启Ollama服务：
    ```sh
    sudo systemctl restart ollama.service
    ```
3. 查看服务状态：
    ```sh
    sudo systemctl status ollama
    ```

### olama局域网访问
与前面一样，在/etc/systemd/system/ollama.service 文件添加
```
Environment="OLLAMA_HOST=0.0.0.0"
```
#### 六、验证更改

1. 检查默认路径：
    进入默认的模型路径 `/usr/share/ollama/.ollama/models`，会发现 `models` 文件夹已经消失。
2. 检查新路径：
    在新的路径 `/data/ollama/models` 下，会看到生成了 `blobs` 和 `manifests` 文件夹，这表明模型路径已经成功更改。

## OpenWebUI

https://zhuanlan.zhihu.com/p/694422278


### Step 1: Pull the Open WebUI Image

Start by pulling the latest Open WebUI Docker image from the GitHub Container Registry.

```sh
docker pull ghcr.io/open-webui/open-webui:main
```

### Step 2: Run the Container

Run the container with default settings. This command includes a volume mapping to ensure persistent data storage.

```sh
docker run -d -p 3000:8080 -v open-webui:/app/backend/data --name open-webui ghcr.io/open-webui/open-webui:main
```

#### Important Flags

- Volume Mapping (`-v open-webui:/app/backend/data`): Ensures persistent storage of your data. This prevents data loss between container restarts.
- Port Mapping (`-p 3000:8080`): Exposes the WebUI on port 3000 of your local machine.

For more details, visit the [OpenWebUI documentation](https://docs.openwebui.com/getting-started/quick-start/).