如何理解深度学习中的端到端（End-to-end）概念

在最近的论文阅读中，又一次见到作者提到 “端到端”（End-to-end) 这个概念，但是在专业课的学习和以往查的资料中，这个名词并没有被特意地强调过。

端到端，是相对于 非端到端 而言的。在机器学习最开始的时候，人们并不是直接输入原始数据，获得最终结果；而是首先通过特征提取，对原始数据进行初步的处理，然后再对于得到的特征进行学习，得出分类or回归的结果。比如使用一些hand-crafted functions作为特征描述符等等。

因此，在这种情况下（非端到端），特征的提取会对模型的最终表现有着巨大的影响。而特征描述符的书写又具有很大的经验成分，所以是一件比较困难的任务。

端到端的学习，就是把特征提取的任务也交给模型去做，直接输入原始数据或者经过些微预处理的数据，让模型自己进行特征提取。 这一种设计风格在神经网络，尤其是深层的神经网络出现后开始被广泛应用，无疑是得益于算力的提升。并且，神经网络可以很好地学习到特征的描述，之前需要人工设计的特征算子，本身也可以通过神经网络的方式，让模型自己习得，从而可以用于其他的任务以及更深的研究。

在图像领域，CNN就是一个很典型的端到端结构。输入图像，输出可以做分类，可以做补全，做各种任务。这之中的具体细节都不需要人工干预，这就是End-to-end。

具体场景下的例子
https://www.cnblogs.com/zeze/p/7798080.html

对于视觉领域而言，end-end一词多用于基于视觉的机器控制方面，具体表现是，神经网络的输入为原始图片，神经网络的输出为（可以直接控制机器的）控制指令，如：

Nvidia的基于CNNs的end-end自动驾驶，输入图片，直接输出steering angle。从视频来看效果拔群，但其实这个系统目前只能做简单的follow lane，与真正的自动驾驶差距较大。亮点是证实了end-end在自动驾驶领域的可行性，并且对于数据集进行了augmentation。链接：https://devblogs.nvidia.com/parallelforall/deep-learning-self-driving-cars/

Google的paper: Learning Hand-Eye Coordination for Robotic Grasping with Deep Learning and Large-Scale Data Collection，也可以算是end-end学习：输入图片，输出控制机械手移动的指令来抓取物品。这篇论文很赞，推荐：https://arxiv.org/pdf/1603.02199v4.pdf

DeepMind神作Human-level control through deep reinforcement learning，其实也可以归为end-end，深度增强学习开山之作，值得学习：http://www.nature.com/nature/journal/v518/n7540/full/nature14236.html

Princeton大学有个Deep Driving项目，介于end-end和传统的model based的自动驾驶之间，输入为图片，输出一些有用的affordance（实在不知道这词怎么翻译合适…）例如车身姿态、与前车距离、距路边距离等，然后利用这些数据通过公式计算所需的具体驾驶指令如加速、刹车、转向等。链接：http://deepdriving.cs.princeton.edu/

参考：
https://www.cnblogs.com/zeze/p/7798080.html