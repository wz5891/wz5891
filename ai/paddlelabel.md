安装
```
conda create -n paddlelabel python=3.11
conda activate paddlelabel

pip install --upgrade paddlelabel
```

启动
```
paddlelabel
```

启动时可能报错
```
(paddlelabel) C:\Users\Administrator>paddlelabel
Starting PaddleLabel
INFO  [paddlelabel.config.19]: Database path: sqlite:///C:\Users\Administrator\.paddlelabel\paddlelabel.db
Traceback (most recent call last):
  File "<frozen runpy>", line 198, in _run_module_as_main
  File "<frozen runpy>", line 88, in _run_code
  File "D:\ProgramData\anaconda3\envs\paddlelabel\Scripts\paddlelabel.exe\__main__.py", line 7, in <module>
  File "D:\ProgramData\anaconda3\envs\paddlelabel\Lib\site-packages\paddlelabel\__main__.py", line 107, in run
    from paddlelabel import api, task
  File "D:\ProgramData\anaconda3\envs\paddlelabel\Lib\site-packages\paddlelabel\api\__init__.py", line 7, in <module>
    from paddlelabel.config import app
  File "D:\ProgramData\anaconda3\envs\paddlelabel\Lib\site-packages\paddlelabel\config.py", line 21, in <module>
    connexion_app = connexion.App("PaddleLabel")
                    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  File "D:\ProgramData\anaconda3\envs\paddlelabel\Lib\site-packages\connexion\utils.py", line 284, in _delayed_error
    raise type(exc)(msg).with_traceback(exc.__traceback__)
  File "D:\ProgramData\anaconda3\envs\paddlelabel\Lib\site-packages\connexion\__init__.py", line 19, in <module>
    from connexion.apps.flask import FlaskApi, FlaskApp
  File "D:\ProgramData\anaconda3\envs\paddlelabel\Lib\site-packages\connexion\apps\flask.py", line 11, in <module>
    from a2wsgi import WSGIMiddleware
ModuleNotFoundError: Please install connexion using the 'flask' extra
```

解决办法
需要安装 connexion 库，并确保它是与 Flask 框架兼容的版本。可以使用以下命令来安装带有 flask 额外依赖的 connexion：
```
pip install connexion[flask]
```
如果你已经安装了 connexion，但是仍然遇到这个错误，可能是因为安装过程中某些东西出了问题。在这种情况下，你可以尝试重新安装：
```
pip uninstall connexion
pip install connexion[flask]
```





https://paddlecv-sig.github.io/PaddleLabel/CN/install.html