## Centos安装Python3.10

安装编译所需的依赖
```
sudo yum -y update
sudo yum -y install openssl-devel libffi-devel bzip2-devel
sudo yum -y groupinstall "Development Tools"
```

2. 安装 Python 3.10 必需的 openssl >= 1.1.1
```
wget https://www.openssl.org/source/openssl-1.1.1q.tar.gz --no-check-certificate
tar zxf openssl-1.1.1q.tar.gz
cd openssl-1.1.1q
./config --prefix=/usr/local/openssl-1.1.1
sudo make && sudo make install
```
3. 安装 Python 3.10
```
wget https://www.python.org/ftp/python/3.10.5/Python-3.10.5.tgz
tar zxf Python-3.10.5.tgz 
cd Python-3.10.5
./configure --enable-optimizations --with-openssl=/usr/local/openssl-1.1.1 --with-openssl-rpath=auto
sudo make install
```

添加软链接
先查看系统python的位置在哪儿
```
whereis python
```


python2.7默认安装是在 /usr/bin目录中，切换到/usr/bin/

```
cd /usr/bin/
ll python*
```

我这里已经软链接了，正常未链接之前是python->python2.7而不是python3.10

添加软链接，将原来的链接备份
```
mv /usr/bin/python /usr/bin/python.bak
```
添加python3的软链接
```
ln -s /usr/local/bin/python3.10 /usr/bin/python
```

测试是否安装成功了
```
python -V
```

默认的centos7是没有安装pip，先添加epel扩展源
```
yum -y install epel-release
```
安装pip
```
yum install python-pip
```
更改yum配置，因为其要用到python2才能执行，否则会导致yum不能正常使用
```
vi /usr/bin/yum
```
把第一行的#! /usr/bin/python 修改为如下
```
#! /usr/bin/python2
```

```
vi /usr/libexec/urlgrabber-ext-down
```

修改如下
```
#! /usr/bin/python2
```


## 安装过程
https://zhuanlan.zhihu.com/p/677986800

https://github.com/QwenLM/Qwen/blob/main/README_CN.md
https://zhuanlan.zhihu.com/p/674758564

git clone https://github.com/QwenLM/Qwen.git


进入到Qwen文件夹，安装运行需要的包。
```
cd Qwen
python3 -m pip install --upgrade pip (升级pip到最新版本，最好把pip源改成国内镜像)
pip3 install -r requirements.txt
pip3 install -r requirements_web_demo.txt
```


下载模型
建立models文件夹
```
mkdir models
cd models
```
开始下载（时间和你的网络速度相关）
```
yum install git-lfs
git lfs install  
git clone https://www.modelscope.cn/qwen/Qwen-1_8B-Chat.git
```

三，运行Qwen

从models目录回到你的Qwen目录下，修改文件web_demo.py，将默认模型文件的路径修改到你的路径。
```
DEFAULT_CKPT_PATH = '/Qwen/models/Qwen-1_8B-Chat' （最好写成绝对路径）
```

运行模型
```
python web_demo.py
或
python cli_demo.py
```


https://zhuanlan.zhihu.com/p/674758564