spacy安装 | 下载太慢了 | 下载超时 | 本地安装 Python 包


请按顺序执行以下推荐方式

1.pip install 在 github 上下载你需要的spacy
这个页面，按需自取下载
```
https://github.com/explosion/spacy-models/releases
```
2.pip本地安装
```
pip install /YOUR_PATH/en_core_web_md-xxxxxxxxxxxtar.gz
```
3.建立 en 链接
安装完语言包后再运行之前的下载命令就不会下载了，而是直接建立短链接，支持load("en")
```
python -m spacy download en
```