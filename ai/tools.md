## Offine-Text-Translate 本地离线文字翻译

支持多语言的本地离线文字翻译Api工具

https://github.com/jianchang512/ott


## 声音克隆
https://github.com/jianchang512/clone-voice

## 扣图
https://github.com/ihmily/image-matting

## 背景声
https://github.com/geekyouth/moodist

## nvidia ai 铸造厂
https://ai.nvidia.com