
系统版本：Ubuntu24.04.2.LTS 
显卡：RTX3060 
现象：安装后，启动黑屏，需要等待5分钟左右。登录后，UI无法操作，卡死。


问题原因：显卡驱动冲突
解决办法：

## 第一步：卸载驱动

启动时，选择高级模式，并进行命令行：
```
sudo apt-get --purge remove nvidia*
sudo apt autoremove
```
To remove CUDA Toolkit:
```
sudo apt-get --purge remove "*cublas*" "cuda*"
```
To remove NVIDIA Drivers:
```
sudo apt-get --purge remove "*nvidia*"
```

然后重启
```
reboot
```
## 第二步：下载驱动
https://www.nvidia.cn/Download/index.aspx?lang=cn


到官网搜索并下载　RTX3060 对应的deb驱动，并下载


### 1. 禁用nouveau

打开终端，输入：
```
sudo gedit /etc/modprobe.d/blacklist.conf 
```
在blacklist.conf文件末尾加上这两行，并保存：
```
blacklist nouveau
options nouveau modeset=0
```

然后执行命令：
```
sudo update-initramfs -u  //应用更改
```
重启电脑，验证是否禁用nouveau：
```
lsmod | grep nouveau
```

### ２.　安装NVIDIA驱动

打开terminal卸载旧版本NVIDIA驱动：
```
sudo apt-get remove --purge nvidia*
```
进入到下载好的.run文件夹下给驱动run文件赋予执行权限：
```
sudo chmod  a+x NVIDIA-Linux-x86_64-455.45.01.run 
```

注：替换自己下载的驱动名称 cd 下载目录 sudo chmod a+x 你下载的文件名

安装NVIDIA驱动
```
sudo ./NVIDIA-Linux-x86_64-455.45.01.run -no-x-check -no-nouveau-check -no-opengl-files
```

只有禁用opengl这样安装才不会出现循环登陆的问题

>　-no-x-check：安装驱动时关闭X服务
>　-no-nouveau-check：安装驱动时禁用nouveau
>　-no-opengl-files：只安装驱动文件，不安装OpenGL文件

安装过程中的选项：

The distribution-provided pre-install script failed! Are you sure you want to continue? 选择 yes 继续。
Would you like to register the kernel module souces with DKMS? This will allow DKMS to automatically build a new module, if you install a different kernel later?  选择 No 继续。
问题没记住，选项是：install without signing
问题大概是：Nvidia's 32-bit compatibility libraries? 选择 No 继续。
Would you like to run the nvidia-xconfigutility to automatically update your x configuration so that the NVIDIA x driver will be used when you restart x? Any pre-existing x confile will be backed up.  选择 Yes

这些选项如果选择错误可能会导致安装失败，没关系，只要前面不出错，多尝试几次就好。

重启之后
```
sudo reboot
```

查看NVIDIA版本检验是否安装完成 `nvidia-smi`


## 安装CUDA
到官网下载，选择相应的版本

https://developer.nvidia.com/cuda-downloads

会提示执行类似下面的命令安装
```
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2404/x86_64/cuda-ubuntu2404.pin

sudo mv cuda-ubuntu2404.pin /etc/apt/preferences.d/cuda-repository-pin-600

wget https://developer.download.nvidia.com/compute/cuda/12.8.0/local_installers/cuda-repo-ubuntu2404-12-8-local_12.8.0-570.86.10-1_amd64.deb


sudo dpkg -i cuda-repo-ubuntu2404-12-8-local_12.8.0-570.86.10-1_amd64.deb

sudo cp /var/cuda-repo-ubuntu2404-12-8-local/cuda-*-keyring.gpg /usr/share/keyrings/

sudo apt-get update

sudo apt-get -y install cuda-toolkit-12-8
```

```
sudo apt install nvidia-cuda-toolkit
nvcc -V
```


pip3 install torch torchvision torchaudio


## 安装aconda
到官网下载最新版本
https://www.anaconda.com/download

执行下载后的文件，即可完成安装
```
bash  Anaconda3-2018.12-Linux-x86_64.sh
```

