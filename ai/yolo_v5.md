https://zhuanlan.zhihu.com/p/269587479


## 安装

### 安装Python
安装 python 3.10（不要使用3.12,也不要使用3.8版本，都会有问题）
https://www.python.org/ftp/python/3.10.11/python-3.10.11-amd64.exe


### 下载代码


YoloV5 v3.1版本
https://github.com/ultralytics/yolov5/tree/v3.1
https://github.com/ultralytics/yolov5/archive/refs/tags/v3.1.zip

下载到 `d:\yolov5``


下载模型
https://github.com/ultralytics/yolov5/releases/download/v7.0/yolov5s.pt

到目录 `d:\yolov5`

下载训练图片
https://github.com/ultralytics/yolov5/releases/download/v1.0/coco128.zip

到目录 `d:\coco128`

### 安装依赖
```
cd d:\yolov5
pip install -r requirements.txt
```

### 测试模型
```
python detect.py --source inference/images/ --weights ./yolov5s.pt
```

### 训练模型
```
python train.py --img 640 --batch 16 --epochs 5 --data ./data/coco128.yaml --cfg ./models/yolov5s.yaml --weights ./yolov5s.pt
```

#### 错误处理

1. RuntimeError: a view of a leaf Variable that requires grad is being used in an in-place operation

这个错误是由于在执行in-place操作时，使用了一个需要梯度的叶节点变量视图（view）。在PyTorch中，如果一个变量需要梯度，那么它的视图也会继承这个属性。而in-place操作是对变量进行原地操作，即直接修改变量的值，这样会导致梯度信息的丢失或不一致。


最简单粗暴的方法是找到models文件夹下的yolo.py文件，在第149下面添加with torch.no_grad(): 
```
def _initialize_biases(self, cf=None):  # initialize biases into Detect(), cf is class frequency
        # https://arxiv.org/abs/1708.02002 section 3.3
        # cf = torch.bincount(torch.tensor(np.concatenate(dataset.labels, 0)[:, 0]).long(), minlength=nc) + 1.
        m = self.model[-1]  # Detect() module
        for mi, s in zip(m.m, m.stride):  # from
            b = mi.bias.view(m.na, -1)  # conv.bias(255) to (3,85)
            with torch.no_grad():
                b[:, 4] += math.log(8 / (640 / s) ** 2)  # obj (8 objects per 640 image)
                b[:, 5:] += math.log(0.6 / (m.nc - 0.99)) if cf is None else torch.log(cf / cf.sum())  # cls
            mi.bias = torch.nn.Parameter(b.view(-1), requires_grad=True)
```

2. AttributeError: module numpy has no attribute int 

在训练YOLO模型时突然发现这个报错，后来发现是numpy版本问题，yolo官方给的是大于等于1.18.5，当你执行pip install -r requirements.txt命令时，他默认安装为1.24，但是再numpy版本更新时numpy.int在NumPy 1.20中已弃用，在NumPy 1.24中已删除。

解决办法：
```
pip uninstall numpy
pip install numpy==1.22
```

3. 安装 numpy1.22 时报 microsoft visual c++ 14.0 or greater is required

下载 vs2015 安装
http://download.microsoft.com/download/7/6/d/76dd809a-d4ae-4e0e-9a24-ad55576e5c8a/vs2015.3.com_cht.iso



## 自己训练

https://blog.csdn.net/zyw2002/article/details/122995823

数据集
https://gitee.com/bravePatch/labeled-mask-dataset


实例
https://cloud.tencent.com/developer/article/2117864?areaSource=102001.11&traceId=sq26m7vRsJsjeMKczWqn6



## 优秀教程

目标检测之YOLOv5项目快速实战教程
https://www.bilibili.com/video/BV1L54y1Z7YE/
