
## Centos 配置swap
1、检查是否有交换分区了，如果有输出交换分区，关闭
```
swapon --show
swapoff -a
```
2、创建16G的交换分区
```
# 方式一
fallocate -l 16G /swapfile
# 方式二，如果是centos 7 并且是xfs文件格式，使用dd
dd if=/dev/zero of=/swapfile bs=1G count=16
```
3、设置交换分区
```
chmod 600 /swapfile
mkswap /swapfile
```
4、启用交换分区
```
swapon /swapfile
```
5、配置系统启动时自动挂载分区
```
"/swapfile   swap    swap    defaults   0   0 " >> /etc/fstab
```
6、重启系统
```
reboot
```
要注意的就是第二步，如果不是centos 6/7 或者是ext文件系统，还是建议用fallocate