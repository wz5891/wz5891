Linux查看Bios版本信息
```
[root@localhost web-server]# dmidecode -t bios
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 3.2.1 present.
# SMBIOS implementations newer than version 3.2.0 are not
# fully supported by this version of dmidecode.

Handle 0x0000, DMI type 0, 26 bytes
BIOS Information
        Vendor: Lenovo
        Version: -[TEE166D-2.90]-
        Release Date: 04/28/2021
        Address: 0xF0000
        Runtime Size: 64 kB
        ROM Size: 32 MB
        Characteristics:
                PCI is supported
                PNP is supported
                BIOS is upgradeable
                BIOS shadowing is allowed
                Boot from CD is supported
                Selectable boot is supported
                EDD is supported
                Serial services are supported (int 14h)
                ACPI is supported
                USB legacy is supported
                BIOS boot specification is supported
                Targeted content distribution is supported
                UEFI is supported
        BIOS Revision: 2.90
        Firmware Revision: 5.10

Handle 0x00CA, DMI type 13, 22 bytes
BIOS Language Information
        Language Description Format: Long
        Installable Languages: 11
                en|US|iso8859-1
                zh|CN|unicode
                zh|TW|unicode
                fr|FR|iso8859-1
                de|DE|iso8859-1
                it|IT|iso8859-1
                ja|JP|unicode
                ko|KR|unicode
                pt|BR|iso8859-1
                es|ES|iso8859-1
                ru|RU|iso8859-5
        Currently Installed Language: zh|CN|unicode

[root@localhost web-server]# 

```

Windows查看BIOS版本信息

```
C:\Users\Administrator>wmic bios get /format:list


BiosCharacteristics={7,9,11,12,15,16,19,28,32,33,40,42,43}
BIOSVersion={"LENOVO - 100","-[TEE166D-2.90]-","American Megatrends - 5000E"}
BuildNumber=
Caption=-[TEE166D-2.90]-
CodeSet=
CurrentLanguage=zh|CN|unicode
Description=-[TEE166D-2.90]-
EmbeddedControllerMajorVersion=5
EmbeddedControllerMinorVersion=10
IdentificationCode=
InstallableLanguages=11
InstallDate=
LanguageEdition=
ListOfLanguages={"en|US|iso8859-1","zh|CN|unicode","zh|TW|unicode","fr|FR|iso8859-1","de|DE|iso8859-1","it|IT|iso8859-1","ja|JP|unicode","ko|KR|unicode","pt|BR|iso8859-1","es|ES|iso8859-1","ru|RU|iso8859-5"}
Manufacturer=Lenovo
Name=-[TEE166D-2.90]-
OtherTargetOS=
PrimaryBIOS=TRUE
ReleaseDate=20210428000000.000000+000
SerialNumber=J6000W9Y
SMBIOSBIOSVersion=-[TEE166D-2.90]-
SMBIOSMajorVersion=3
SMBIOSMinorVersion=2
SMBIOSPresent=TRUE
SoftwareElementID=-[TEE166D-2.90]-
SoftwareElementState=3
Status=OK
SystemBiosMajorVersion=2
SystemBiosMinorVersion=90
TargetOperatingSystem=0
Version=LENOVO - 100
```
 

电源通用后电脑自动启动
https://zhidao.baidu.com/question/467328928994309845.html