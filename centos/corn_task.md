## 编辑corn任务
```
crontab -e
```

使用 crontab -e 可以编辑定时任务，自动打开 vim 编辑器，直接修改即可。编辑完成后，立即生效，不需要重新添加。


## 查看crontab任务
```
crontab -l
```


## corn 语法
就像使用任何语言一样，当你了解cron的语法时，使用cron会容易得多，它的语法有两种格式：

```
A B C D E /path/to/command arg1 arg2
OR
A B C D E /root/backup.sh
```
说明：
```
A：分钟范围：0 – 59
B：时间范围：0 – 23
C：天数范围：1 – 31
D：月范围：1 – 12 or JAN-DEC
E：星期几：0 – 6 or SUN-SAT，Sunday=0 or 7。
USERNAME： 用户名
/path/to/command – 你要计划的脚本或命令的名称
```

```
┌───────────── minute (0 - 59)
│ ┌───────────── hour (0 - 23)
│ │ ┌───────────── day of the month (1 - 31)
│ │ │ ┌───────────── month (1 - 12 or JAN-DEC)
│ │ │ │ ┌───────────── day of the week (0 - 6 or SUN-SAT, Sunday=0 or 7)
│ │ │ │ │
│ │ │ │ │
│ │ │ │ │
* * * * *
```
此外，Cron使用3个运算符，可以在字段中指定多个值：
```
星号(*)：指定字段的所有可能值，* * * * * 在每天的每分钟运行。
逗号(,)：指定值列表，2,10 4,5 * * *在每天第 4 和第 5 小时的第 2 和第 10 分钟运行。
破折号(-)：指定值范围，0 4-6 * * * 在第 4、5、6 小时的第 0 分钟运行。
分隔符(/)：指定步长值，20/15 * * * * 从第 20 分钟到第 59 分钟每隔 15 分钟运行（第 20、35 和 50 分钟）。
```

```
0 0 * * * /data/docker/docker-compose/db-server/backup/bin/backup.sh
```

## 查看日志
```
tail -f /var/log/cron

tail -f /var/spool/mail/root
```

## 环境变量

我们可以通过cron执行env看看里面的环境变量

crontab -e加入如下代码
```
* * * * * env > /env.output
```
之后等文件出来后cat一下就可以了
```
# cat env.output
HOME=/root
LOGNAME=root
PATH=/usr/bin:/bin
LANG=en_US.UTF-8
SHELL=/bin/sh
PWD=/root
```
可以看到PATH就两个，所以保险一点就是使用绝对路径执行程序，或者修改环境变量再执行