Linux将剩余未分区的磁盘空间挂载

查看已挂载磁盘的使用情况
```
df -h
```


可以看到，我这里已经挂载了sda磁盘，并只有sda1这1个分区。


查看所有磁盘信息（包括未挂载磁盘）
```
fdisk -l
lsblk
```


这里可以看到sda磁盘总共有64.4GB，但sda1分区分了40G，所以还有剩余的空间可以分配。


创建新的磁盘分区
首先进入磁盘：
```
fdisk /dev/sda

输入m可以获取提示（可以不执行），然后按下列操作。

输入n添加新分区
输入p选择分区类型（默认回车就是p）
选择分区号，一般1-4，默认的就行
选择起始扇区，默认的就行
输入t
输入分区类型，输入8e表示虚拟逻辑卷分区，后期硬盘分区空间不足可以在线扩容
输入命令w，重写分区表

```




查看创建的分区
```
fdisk -l
```

如果看不到可以使用partprobe重新读取分区表，可以不用重启。
```
partprobe /dev/sda
```
将物理硬盘分区初始化为物理卷，以便LVM使用
```
pvcreate /dev/sda2
```




创建卷组和逻辑卷并格式化
```
vgcreate test_vg /dev/sda2
```
创建卷组test_vg
```
lvcreate -l +100%FREE -n test_lv test_vg
```
创建逻辑卷test_lv
`df -hT` 查看空间使用情况
格式化逻辑卷
```
mkfs.ext4 /dev/test_vg/test_lv
```






创建目录将新的分区挂载到创建的目录
创建目录
```
mkdir /data
```

挂载目录
```
mount /dev/test_vg/test_lv /data
```



设置分区在系统重启后自动挂载
```
vim /etc/fstab
``````

最底下添加
```
/dev/test_vg/test_lv /data ext4 defaults 0 0
```



这样就把磁盘挂载成功了。

作者：code泷
链接：https://juejin.cn/post/6998762369346174990
来源：稀土掘金
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。