## 防火墙
```
firewall-cmd --zone=public --add-port=8081/tcp --permanent
firewall-cmd --zone=public --add-port=8082-8085/tcp --permanent
firewall-cmd --reload
firewall-cmd --list-port


# 删除
firewall-cmd --zone=public --remove-port=8081/tcp --permanent
```
firewall-cmd --zone=public --add-port=11201-11500/tcp --permanent

## rich-rule
```
firewall-cmd --permanent --add-rich-rule '规则列表'
firewall-cmd --permanent --add-rich-rule 'rule family="ipv4" source address="0.0.0.0/0" forward-port port="8077" protocol="tcp" to-port="80" to-addr="192.168.4.245"'
firewall-cmd --permanent --remove-rich-rule '规则列表'
firewall-cmd --permanent --remove-rich-rule 'rule family="ipv4" source address="0.0.0.0/0" forward-port port="8077" protocol="tcp" to-port="80" to-addr="192.168.4.245"'
firewall-cmd --reload
firewall-cmd --list-rich-rules
```

## 实现只允许来自 192.168.1.121 的客户端访问 MySQL 服务器的 3306 端口，并且不允许访问其他端口
为了实现只允许来自 `192.168.1.121` 的客户端访问 MySQL 服务器的 3306 端口，并且不允许访问其他端口，你可以使用 `firewall-cmd` 命令来添加富规则（rich rules）。这些规则可以非常精确地控制网络流量。以下是具体的步骤：

### 步骤 1: 添加富规则

首先，添加一条富规则，允许来自 `192.168.1.121` 的客户端访问 3306 端口。这将确保只有该 IP 地址可以连接到 MySQL。

```bash
sudo firewall-cmd --permanent --add-rich-rule='rule family="ipv4" source address="192.168.1.121" port port="3306" protocol="tcp" accept'
```

### 步骤 2: 确保没有其他规则允许 `192.168.1.121` 访问其它端口

你需要检查现有的防火墙规则，确保没有其他规则允许 `192.168.1.121` 访问除 3306 以外的端口。如果有，你可能需要移除或调整那些规则。如果你希望更加严格控制，可以考虑拒绝所有来自 `192.168.1.121` 的非 3306 端口流量。

要拒绝所有来自 `192.168.1.121` 的非 3306 端口的流量，可以添加以下规则：

```bash
sudo firewall-cmd --permanent --add-rich-rule='rule family="ipv4" source address="192.168.1.121" not port port="3306" protocol="tcp" reject'
```

### 步骤 3: 重新加载防火墙配置

完成上述配置后，记得重新加载防火墙配置以使更改生效：

```bash
sudo firewall-cmd --reload
```

### 步骤 4: 验证规则

你可以通过下面的命令来验证规则是否正确添加：

```bash
sudo firewall-cmd --list-all
```

或者查看特定的富规则：

```bash
sudo firewall-cmd --list-rich-rules
```

### 注意事项

- **顺序**：请注意，规则的顺序很重要。通常来说，更具体的规则应该放在更一般的规则之前。在本例中，我们先允许 3306 端口的访问，然后再拒绝其他的端口访问。
- **永久性**：我们使用了 `--permanent` 参数，这意味着规则会在防火墙重启后仍然有效。但请记住，在应用永久性规则时，必须调用 `firewall-cmd --reload` 来让规则立即生效。
- **测试**：在生产环境中实施这样的变更前，请务必在一个测试环境中进行充分的测试，以确保不会意外封锁合法的流量或开放不必要的访问权限。

