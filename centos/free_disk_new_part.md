centos对磁盘剩余空间进行分区，并将其挂载到指定目录

https://blog.csdn.net/m0_63004677/article/details/138961564

查看/dev/sda 还有大量未分配的空间。当前有两个分区：/dev/sda1 和 /dev/sda2 
```
fdisk -l
```

创建一个新的分区并将其挂载到 /app 目录。以下是具体步骤：

1、创建新分区
```
fdisk /dev/sda
```

在 fdisk 交互模式下进行以下操作：

```
输入 n 创建一个新分区。
选择 p 创建主分区。
选择分区号（例如 3）。
选择起始扇区（使用默认值）。
W 写入
```
使用 partprobe 重新扫描分区表：
```
partprobe /dev/sda 
```

2、格式化新分区
```
mkfs.xfs /dev/sda3
```
3、创建挂载目录
```
mkdir /app 
```
4、挂载新分区
```
mount /dev/sda3 /app 
```
5、编辑 /etc/fstab 持久化挂载
```
echo '/dev/sda3 /app xfs defaults 0 0' >> /etc/fstab 
```
