在LInux上使用ncftp上传整个目录到ftp服务器

安装
```
yum install ncftp
```

也可以直接下载对应的二进制文件
https://www.ncftp.com/public_ftp/ncftp/binaries/ncftp-3.2.7-linux-x86_64-glibc2.17-export.tar.gz

```
cp ./ncftp-3.2.7/bin/ncftpput /usr/local/sbin
chmod +x /usr/local/sbin/ncftpput
```


语法：
```
ncftpput ftp-host-name /path/to/remote/dir /path/to/local/dir
ncftpput -options ftp-host-name /path/to/remote/dir /path/to/local/dir
```


示例：
```
ncftpput -R -v -u 'username' -p 'passwordHere' -P 2021 ftp.website.com /remoteftp/dir /local/dir
```


-u ‘username’: ftp 服务器用户名。
-p ‘passwordHere’: ftp 服务器用户密码。
-P 2021: ftp 服务器端口。
-v: 显示上传过程。
-R: 递归模式，上传文件夹中的所有文件及子文件夹。
ftp.website.com: ftp服务器。
/remoteftp/dir: ftp服务器上的文件路径。
/local/dir: 待上传的文件夹或文件路径。