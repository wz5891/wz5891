https://www.cnblogs.com/sky-cheng/p/15262516.html



[centos7利用scl软件安装gcc7.3环境](https://www.cnblogs.com/sky-cheng/p/15262516.html)

Centos7 gcc版本默认4.8.3，Red Hat 为了软件的稳定和版本支持，yum 上版本也是4.8.3，所以无法使用yum进行软件更新，所以使用scl。

scl软件集(Software Collections),是为了给 RHEL/CentOS 用户提供一种以方便、安全地安装和使用应用程序和运行时环境的多个（而且可能是更新的）版本的方式，同时避免把系统搞乱。

### 一、安装scl源

```
[root@localhost Bringing-Old-Photos-Back-to-Life]# yum install centos-release-scl scl-utils-build scl-utils -y
```

### 二、查看scl有哪些源可以用



```
[root@localhost Bringing-Old-Photos-Back-to-Life]# yum list all --enablerepo='centos-sclo-rh'|grep gcc
gcc.x86_64                                   4.8.5-44.el7         @base         
gcc-c++.x86_64                               4.8.5-44.el7         @base         
libgcc.x86_64                                4.8.5-44.el7         @base         
compat-gcc-44.x86_64                         4.4.7-8.el7          base          
compat-gcc-44-c++.x86_64                     4.4.7-8.el7          base          
compat-gcc-44-gfortran.x86_64                4.4.7-8.el7          base          
devtoolset-10-gcc.x86_64                     10.2.1-11.1.el7      centos-sclo-rh
devtoolset-10-gcc-c++.x86_64                 10.2.1-11.1.el7      centos-sclo-rh
devtoolset-10-gcc-gdb-plugin.x86_64          10.2.1-11.1.el7      centos-sclo-rh
devtoolset-10-gcc-gfortran.x86_64            10.2.1-11.1.el7      centos-sclo-rh
devtoolset-10-gcc-plugin-devel.x86_64        10.2.1-11.1.el7      centos-sclo-rh
devtoolset-10-libgccjit.x86_64               10.2.1-11.1.el7      centos-sclo-rh
devtoolset-10-libgccjit-devel.x86_64         10.2.1-11.1.el7      centos-sclo-rh
devtoolset-10-libgccjit-docs.x86_64          10.2.1-11.1.el7      centos-sclo-rh
devtoolset-7-gcc.x86_64                      7.3.1-5.16.el7       centos-sclo-rh
devtoolset-7-gcc-c++.x86_64                  7.3.1-5.16.el7       centos-sclo-rh
devtoolset-7-gcc-gdb-plugin.x86_64           7.3.1-5.16.el7       centos-sclo-rh
devtoolset-7-gcc-gfortran.x86_64             7.3.1-5.16.el7       centos-sclo-rh
devtoolset-7-gcc-plugin-devel.x86_64         7.3.1-5.16.el7       centos-sclo-rh
devtoolset-7-libgccjit.x86_64                7.3.1-5.16.el7       centos-sclo-rh
devtoolset-7-libgccjit-devel.x86_64          7.3.1-5.16.el7       centos-sclo-rh
devtoolset-7-libgccjit-docs.x86_64           7.3.1-5.16.el7       centos-sclo-rh
devtoolset-8-gcc.x86_64                      8.3.1-3.2.el7        centos-sclo-rh
devtoolset-8-gcc-c++.x86_64                  8.3.1-3.2.el7        centos-sclo-rh
devtoolset-8-gcc-gdb-plugin.x86_64           8.3.1-3.2.el7        centos-sclo-rh
devtoolset-8-gcc-gfortran.x86_64             8.3.1-3.2.el7        centos-sclo-rh
devtoolset-8-gcc-plugin-devel.x86_64         8.3.1-3.2.el7        centos-sclo-rh
devtoolset-8-libgccjit.x86_64                8.3.1-3.2.el7        centos-sclo-rh
devtoolset-8-libgccjit-devel.x86_64          8.3.1-3.2.el7        centos-sclo-rh
devtoolset-8-libgccjit-docs.x86_64           8.3.1-3.2.el7        centos-sclo-rh
devtoolset-9-gcc.x86_64                      9.3.1-2.2.el7        centos-sclo-rh
devtoolset-9-gcc-c++.x86_64                  9.3.1-2.2.el7        centos-sclo-rh
devtoolset-9-gcc-gdb-plugin.x86_64           9.3.1-2.2.el7        centos-sclo-rh
devtoolset-9-gcc-gfortran.x86_64             9.3.1-2.2.el7        centos-sclo-rh
devtoolset-9-gcc-plugin-devel.x86_64         9.3.1-2.2.el7        centos-sclo-rh
devtoolset-9-libgccjit.x86_64                9.3.1-2.2.el7        centos-sclo-rh
devtoolset-9-libgccjit-devel.x86_64          9.3.1-2.2.el7        centos-sclo-rh
devtoolset-9-libgccjit-docs.x86_64           9.3.1-2.2.el7        centos-sclo-rh
gcc-gfortran.x86_64                          4.8.5-44.el7         base          
gcc-gnat.x86_64                              4.8.5-44.el7         base          
gcc-go.x86_64                                4.8.5-44.el7         base          
gcc-objc.x86_64                              4.8.5-44.el7         base          
gcc-objc++.x86_64                            4.8.5-44.el7         base          
gcc-plugin-devel.x86_64                      4.8.5-44.el7         base          
libgcc.i686                                  4.8.5-44.el7         base          
relaxngcc.noarch                             1.12-6.el7           base          
relaxngcc-javadoc.noarch                     1.12-6.el7           base          
[root@localhost Bringing-Old-Photos-Back-to-Life]# 
```



安装7.3版本的gcc、gcc-c++、gdb

```
[root@localhost Bringing-Old-Photos-Back-to-Life]# yum install devtoolset-7-gcc.x86_64 devtoolset-7-gcc-c++.x86_64 devtoolset-7-gcc-gdb-plugin.x86_64 -y
```

查看scl安装的包列表

```
[root@localhost Bringing-Old-Photos-Back-to-Life]# scl --list
devtoolset-7
[root@localhost Bringing-Old-Photos-Back-to-Life]# 
```

查看当前gcc版本



```
[root@localhost Bringing-Old-Photos-Back-to-Life]# gcc -v
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/usr/libexec/gcc/x86_64-redhat-linux/4.8.5/lto-wrapper
Target: x86_64-redhat-linux
Configured with: ../configure --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-bugurl=http://bugzilla.redhat.com/bugzilla --enable-bootstrap --enable-shared --enable-threads=posix --enable-checking=release --with-system-zlib --enable-__cxa_atexit --disable-libunwind-exceptions --enable-gnu-unique-object --enable-linker-build-id --with-linker-hash-style=gnu --enable-languages=c,c++,objc,obj-c++,java,fortran,ada,go,lto --enable-plugin --enable-initfini-array --disable-libgcj --with-isl=/builddir/build/BUILD/gcc-4.8.5-20150702/obj-x86_64-redhat-linux/isl-install --with-cloog=/builddir/build/BUILD/gcc-4.8.5-20150702/obj-x86_64-redhat-linux/cloog-install --enable-gnu-indirect-function --with-tune=generic --with-arch_32=x86-64 --build=x86_64-redhat-linux
Thread model: posix
gcc version 4.8.5 20150623 (Red Hat 4.8.5-44) (GCC) 
[root@localhost Bringing-Old-Photos-Back-to-Life]# 
```



切换版本

```
[root@localhost Bringing-Old-Photos-Back-to-Life]# scl enable devtoolset-7 bash
[root@localhost Bringing-Old-Photos-Back-to-Life]# 
```

查看切换后的版本



```
[root@localhost Bringing-Old-Photos-Back-to-Life]# gcc -v
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/opt/rh/devtoolset-7/root/usr/libexec/gcc/x86_64-redhat-linux/7/lto-wrapper
Target: x86_64-redhat-linux
Configured with: ../configure --enable-bootstrap --enable-languages=c,c++,fortran,lto --prefix=/opt/rh/devtoolset-7/root/usr --mandir=/opt/rh/devtoolset-7/root/usr/share/man --infodir=/opt/rh/devtoolset-7/root/usr/share/info --with-bugurl=http://bugzilla.redhat.com/bugzilla --enable-shared --enable-threads=posix --enable-checking=release --enable-multilib --with-system-zlib --enable-__cxa_atexit --disable-libunwind-exceptions --enable-gnu-unique-object --enable-linker-build-id --with-gcc-major-version-only --enable-plugin --with-linker-hash-style=gnu --enable-initfini-array --with-default-libstdcxx-abi=gcc4-compatible --with-isl=/builddir/build/BUILD/gcc-7.3.1-20180303/obj-x86_64-redhat-linux/isl-install --enable-libmpx --enable-gnu-indirect-function --with-tune=generic --with-arch_32=i686 --build=x86_64-redhat-linux
Thread model: posix
gcc version 7.3.1 20180303 (Red Hat 7.3.1-5) (GCC) 
[root@localhost Bringing-Old-Photos-Back-to-Life]#
```



目前是7.3.1了

使用exit命令退出scl环境

```
[root@localhost Bringing-Old-Photos-Back-to-Life]# exit
exit
```

再次查看版本



```
[root@localhost Bringing-Old-Photos-Back-to-Life]# exit
exit
[root@localhost Bringing-Old-Photos-Back-to-Life]# gcc -v
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/usr/libexec/gcc/x86_64-redhat-linux/4.8.5/lto-wrapper
Target: x86_64-redhat-linux
Configured with: ../configure --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-bugurl=http://bugzilla.redhat.com/bugzilla --enable-bootstrap --enable-shared --enable-threads=posix --enable-checking=release --with-system-zlib --enable-__cxa_atexit --disable-libunwind-exceptions --enable-gnu-unique-object --enable-linker-build-id --with-linker-hash-style=gnu --enable-languages=c,c++,objc,obj-c++,java,fortran,ada,go,lto --enable-plugin --enable-initfini-array --disable-libgcj --with-isl=/builddir/build/BUILD/gcc-4.8.5-20150702/obj-x86_64-redhat-linux/isl-install --with-cloog=/builddir/build/BUILD/gcc-4.8.5-20150702/obj-x86_64-redhat-linux/cloog-install --enable-gnu-indirect-function --with-tune=generic --with-arch_32=x86-64 --build=x86_64-redhat-linux
Thread model: posix
gcc version 4.8.5 20150623 (Red Hat 4.8.5-44) (GCC) 
[root@localhost Bringing-Old-Photos-Back-to-Life]# 
```



变为原来的4.8.5了