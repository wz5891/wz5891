经常需要测试两台服务器间某个端口是否通畅，最简单的方法是在目标服务器上启动一个服务并监听某个端口。
然后通过浏览器或curl命令访问

```
curl http://ip:port
```

启动服务可使用下面三种简单的方法

## 使用nc
```
yum install nc
```
监听12345端口
```
nc -l 12345
```


## 使用 node
可使用 node 的 http-server
安装
```
npm install -g http-server
```

将当前目录发布为http服务
```
http-server .
```

## 使用python
Python3
```
python -m http.server 8000
```

Python2
```
python -m SimpleHTTPServer 8000
```




