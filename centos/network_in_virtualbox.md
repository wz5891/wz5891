软件环境：Centos7.9, Oracle VirtualBox 7.0.12


在VirtaulBox中安装Centos虚拟机后，网络需要配置两张网卡实现两个目标

目标1：虚拟机可以上网
要求在虚拟机网络配置中，启用网卡1，连接方式为“NAT网络”

目标2：虚拟机与主机可以相互访问
要求在虚拟机网络配置中，启用网卡2，连接方式为“仅主机（Host-Only）网络”


> NAT网络，仅主机（Host-Only）网各有什么区别？
> NAT网络：NAT网络是一种虚拟局域网技术，它将局域网的私有IP地址转换成公网IP地址，这样，局域网中的主机就可以通过公网IP地址访问互联网。
> 仅主机（Host-Only）网络：主机可以访问虚拟机，同时虚拟机也可以访问主机的IP地址，但不能访问其他主机的IP地址。

>网络可以在VirtualBox 的管理->工具->网络管理器 中进行设置


虚拟机中配置网络后，启动虚拟机，接下来在CentOS虚拟中配置网络：

首先通过 ip address 查看配置好的网卡信息
```
[root@localhost ~]# ip address
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:4b:53:a0 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.5/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 508sec preferred_lft 508sec
    inet6 fe80::a00:27ff:fe4b:53a0/64 scope link 
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:d0:d2:1f brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.150/24 brd 192.168.56.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fed0:d21f/64 scope link 
       valid_lft forever preferred_lft forever
[root@localhost ~]# 
```

配置网卡enp0s8（host-only）：
vi /etc/sysconfig/network-scripts/ifcfg-enp0s8

```
TYPE=Ethernet
DEVICE=enp0s8
BOOTPROTO=static
ONBOOT=yes
IPADDR=192.168.56.150
NETMASK=255.255.255.0
GATEWAY=192.168.56.100
```

> 上面 IPADDR,NETMASK,GATEWAY配置，取决于 Host-Only 网络中的设置


配置网卡enp0s3（Nat）:
vi /etc/sysconfig/network-scripts/ifcfg-enp0s3

```
TYPE=Ethernet
DEVICE=enp0s3
BOOTPROTO=dhcp
ONBOOT=yes
```


重启网卡
```
service network restart
```