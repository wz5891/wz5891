rm删除文件，并排除指定文件
```
//开启通配符功能
shopt -s  extglob
// 删除全部文件，保留1.txt和2.txt
rm -rf !(1.txt)
// 删除全部文件，保留1.txt和2.txt
rm -rf !(1.txt|2.txt)
```


或者
```
// 删除全部文件，保留1.txt
find * | grep -v 1.txt | xargs rm
```