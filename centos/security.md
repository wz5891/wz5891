# Diffie-Hellman Key Agreement Protocol 资源管理错误漏洞（CVE-2002-20001）
## 漏洞描述
Diffie-Hellman Key Agreement Protocol（DH协议）是一种用于密钥交换的协议，它允许两个通信方在公开通道上安全地交换密钥。然而，2002年发现了一个资源管理错误漏洞（CVE-2002-20001），该漏洞可能导致攻击者获取密钥信息或触发拒绝服务攻击。
 
这个漏洞是由于资源管理不当导致的，具体来说，是由于在处理某些特定情况时，协议没有正确地释放内存资源。攻击者可以利用这一点，通过发送特制的数据包使目标系统耗尽资源，从而导致拒绝服务攻击。此外，攻击者还可能利用这个漏洞获取密钥信息，从而破解通信双方的安全连接。
 
为了修复这个漏洞，建议使用Diffie-Hellman协议的系统和应用程序需要更新到修复了此漏洞的版本。同时，建议用户定期更新和维护其软件，以确保其安全性。 


## 解决方式
由于低版本的OpenSSH使用了过时不安全的加密算法协议，通常OpenSSH在版本迭代更新时会弃用这些不安全的加密算法。 如果我们仍要继续使用旧版本的OpenSSH，可以根据实际情况，考虑屏蔽掉不安全的加密算法，以降低安全风险

查看客户端支持的kexalgorithms
```
[root@localhost ~]# ssh -Q kex
diffie-hellman-group1-sha1
diffie-hellman-group14-sha1
diffie-hellman-group14-sha256
diffie-hellman-group16-sha512
diffie-hellman-group18-sha512
diffie-hellman-group-exchange-sha1
diffie-hellman-group-exchange-sha256
ecdh-sha2-nistp256
ecdh-sha2-nistp384
ecdh-sha2-nistp521
curve25519-sha256
curve25519-sha256@libssh.org
gss-gex-sha1-
gss-group1-sha1-
gss-group14-sha1-
```

查看服务端支持的kexalgorithms
```
[root@localhost ~]# sshd -T | grep -w kexalgorithms
kexalgorithms curve25519-sha256,curve25519-sha256@libssh.org,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group-exchange-sha1,diffie-hellman-group14-sha256,diffie-hellman-group14-sha1,diffie-hellman-group1-sha1
```

通过修改sshd_config配置文件，屏蔽掉不安全的KexAlgorithjms，其中具体的sshd_config配置参数可以从man文档中查看
```
man sshd_config |grep -A 40 -w KexAlgorithms
```

去掉所有Diffie-Hellman算法
```
echo "KexAlgorithms curve25519-sha256,curve25519-sha256@libssh.org,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521" >> /etc/ssh/sshd_config
```

重启sshd
```
systemctl restart sshd
```

再次查看服务端支持的kexalgorithms
```
[root@localhost ~]# sshd -T | grep -w kexalgorithms
kexalgorithms curve25519-sha256,curve25519-sha256@libssh.org,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521
```

参考：
https://www.cnblogs.com/autopwn/p/16363160.html



# OpenSSH 安全漏洞（CVE-2023-38408）
## 漏洞综述
OpenSSH是SSH（Secure SHell）协议的免费开源实现，通过 SSH协议进行加密通讯。OpenSSH ssh-agent是一种SSH密钥管理工具，可以帮助用户更安全地管理SSH密钥。近日，新华三盾山实验室监测到OpenSSH官方发布了安全公告，修复了一个存在于OpenSSH ssh-agent中的远程代码执行漏洞（CVE-2023-38408），且漏洞利用代码已公开，攻击者利用该漏洞可在目标系统上执行任意代码。

当开启ssh-agent转发时，具有转发到服务器权限的恶意攻击者成功利用此漏洞可在ssh-agent上执行任意代码。（需要受害者系统上存在特定的库）

影响范围
5.5 < OpenSSH <= 9.3p1


## 解决办法
为了有效解决和防范CVE-2023-38408，请遵循以下综合步骤：

首先升级到OpenSSH 9.3p2或更高版本：升级到最新版本的OpenSSH至关重要，因为它包含缓解漏洞的关键补丁。确保所有相关系统和服务器及时更新至推荐版本或更高版本。

另外采取预防措施来避免被利用：

建议在仅仅OpenSSH用于远程主机管理的机器，通过Openssh配置（sshd_config）、防火墙，安全组ACL等限制来源访问IP为白名单仅可信IP地址，同时，非必要，关闭SSH代理转发功能，禁止在有关主机启用ssh隧道等。

关闭SSH代理转发功能方法为：

配置/etc/ssh/sshd_config
```
AllowTcpForwarding NO
```
查看openssh版本
```
[root@localhost ~]# ssh -V
OpenSSH_7.4p1, OpenSSL 1.0.2k-fips  26 Jan 2017
```




centos7在线升级openssh
要在CentOS 7上升级OpenSSH，可以按照以下步骤进行操作：

首先，确保系统已经更新到最新版本。使用以下命令来更新软件包列表并安装所有可用的更新：

sudo yum update -y
接下来，查看当前安装的OpenSSH版本。运行以下命令获取信息：

ssh -V
如果需要升级OpenSSH，则需要添加EPEL存储库（Extra Packages for Enterprise Linux）。运行以下命令来安装EPEL存储库：

sudo yum install epel-release -y
然后，再次运行ssh -V命令来确认现在的OpenSSH版本。

若要升级OpenSSH，可以通过以下两种方式之一完成：

a) 使用YUM工具直接升级OpenSSH：

sudo yum upgrade openssh -y


参考：
https://www.h3c.com/cn/d_202309/1930993_30003_0.htm
https://baijiahao.baidu.com/s?id=1771989274391205588&wfr=spider&for=pc
https://blog.csdn.net/weixin_45087884/article/details/130970145


# 目标主机showmount -e信息泄露(CVE-1999-0554)【原理扫描】
## 漏洞描述
大多数NFS实现在其文件处理中都有可以猜测的特定模式。大多数NFS实现依赖于文件处理的保密性来提高文件的实际安全性。攻击者可以猜测文件处理方式，以绕过挂载的安全性，并未经授权访问NFS资源和NFS卷上的所有文件。

可对目标主机进行"showmount -e"操作，此操作将泄露目标主机大量敏感信息，比如目录结构。如果访问控制不严格，攻击者可直接访问到目标主机上的数据。

## 解决办法
关闭 NFS 服务，或者限制 IP 对服务器进行访问
```
在 ubuntu 上利用系统 IP 限制配置文件进行编写 控制 IP 访问

建议对源文件进行修改时先进行备份操作
备份原配置文件
cp /etc/hosts.deny /etc/hosts.deny_bak
cp /etc/hosts.allow /etc/hosts.allow_bak

拒绝所有挂载主机（加黑）
echo “rpcbind:ALL:deny” >>/etc/hosts.deny
echo “mountd:all” >>/etc/hosts.deny
echo “Portmap:ALL:deny” >>/etc/hosts.deny

只允许某些 IP 挂载（加白） 
echo “rpcbind:IP:allow” >>/etc/hosts.allow
echo “mountd:IP” >>/etc/hosts.allow
echo “Portmap:IP:allow” >>/etc/hosts.allow"


**测试用例**
vim /etc/hosts.allow
mountd：IP
# 将 IP 加入白名单
vim /etc/hosts.deng
mountd：all
# 将所有 IP 加入黑名单
```


```
停止 NFS 服务：

sudo systemctl stop nfs-server nfs-lock nfs-idmap
禁用 NFS 服务，防止它在下次启动时自动启动：

sudo systemctl disable nfs-server nfs-lock nfs-idmap
如果你还想确保 NFS 服务在重启后不会自动启动，可以禁用它的依赖服务：

sudo systemctl disable rpcbind
请注意，禁用 NFS 服务会影响系统对 NFS 共享的使用，因此请确保这是你真正想要的操作，或者你有其他的解决方案来修复这个漏洞。如果你需要使用 NFS 服务，你应该安装安全更新或者升级到不受此漏洞影响的 NFS 版本。
```

配置共享目录
修改/etc/exports，将/mnt/kvm/添加进去：

```
cat /etc/exports
```

查看共享目录列表
```
showmount -e localhost
```

参考：
https://blog.csdn.net/weixin_52084568/article/details/129159277

https://www.cnblogs.com/zcg-cpdd/p/14505731.html

https://blog.51cto.com/u_16213638/10078322


CentOS 7 安装配置 NFS
https://www.cnblogs.com/wangmo/p/15048032.html


# OpenSSH 安全漏洞（CVE-2023-51384）

CVE-2023-51384 是 OpenSSH 中的一个安全漏洞，它影响了 OpenSSH 8.0 到 8.9 版本。该漏洞是由于 OpenSSH 服务在处理某些 SSH 连接请求时未能正确验证数据导致的。攻击者可以利用这个漏洞发起中间人攻击，获取敏感信息或者执行未授权的命令。

解决方法：

1. 升级 OpenSSH 到安全版本：更新到 OpenSSH 8.9p1 或更高版本。可以通过系统包管理器或直接从 OpenSSH 官方网站下载源码进行编译安装。

2. 临时修补措施：如果不能立即更新，可以采取以下措施暂时防御此漏洞：

更改默认端口号。

使用 SSH 密钥认证而非密码认证。

配置 SSH 服务器禁止使用密码认证。

使用防火墙规则来限制可能的攻击源。

监控安全更新：定期关注 OpenSSH 的安全更新和漏洞信息，及时应用安全补丁。

示例代码：

更新 OpenSSH 到安全版本（以 Ubuntu 为例）：
```
sudo apt-get update
sudo apt-get install openssh-server
```
临时修补措施示例（修改 SSH 配置文件 /etc/ssh/sshd_config）：
```
Port 2222  # 更改默认端口号
PasswordAuthentication no  # 禁止使用密码认证
```
重启 SSH 服务以应用更改：
```
sudo systemctl restart ssh
```
请注意，具体的修复步骤可能会根据您的操作系统和环境有所不同。建议参考官方文档或者您所用系统的官方指南进行操作。

# OpenSSH 安全漏洞（CVE-2023-51385）
CVE-2023-51385 是针对 OpenSSH 软件包的一个安全漏洞，具体涉及到命令注入问题。以下是对该漏洞的详细概述：

漏洞概述：

OpenSSH 是一个广泛使用的开源软件，实现了 SSH (Secure Shell) 协议，提供安全的远程登录、命令执行以及数据传输服务。CVE-2023-51385 描述了一个命令注入漏洞，存在于 OpenSSH 9.6 版本之前。

漏洞细节：

受影响组件： 该漏洞主要与 ssh_config 文件中的特定配置选项有关，特别是 ProxyCommand 和/或 LocalCommand。

漏洞原理： 当用户的用户名或主机名中包含特定的 shell 元字符（如 |、"、' 等）时，如果 ssh_config 中启用了易受攻击的 ProxyCommand 或 LocalCommand 设置，并且这些设置没有妥善转义或限制输入，攻击者可能能够通过精心构造的用户名或主机名注入恶意 Shell 命令。这可能导致攻击者在目标系统上执行未经授权的操作或获取敏感信息。

风险等级与影响： 根据提供的信息，该漏洞的风险等级被标记为“中危险”，CVSS 分值为 5.5，表明其具有一定的严重性。成功利用此漏洞的攻击者能够在受影响的 OpenSSH 服务器或客户端上执行任意命令，威胁到系统的安全性、数据隐私和整体完整性。

缓解措施与修复建议：

软件更新： 最直接且有效的缓解措施是升级到 OpenSSH 的最新版本，至少应升级至 9.6 或之后的版本，以确保包含了针对 CVE-2023-51385 的补丁。例如，CentOS 7 用户可以通过升级到 OpenSSH 9.6p1 来解决包括 CVE-2023-51385 在内的相关安全漏洞。

配置审查与调整： 对 ssh_config 文件中的 ProxyCommand 和 LocalCommand 设置进行审查，确保它们不会直接使用未经充分验证和转义的用户输入。可能需要修改这些配置，使用更安全的命令结构或添加适当的转义机制，以防止潜在的命令注入。

输入过滤与验证： 如果无法立即升级，可以尝试在应用程序层面对用户提供给 OpenSSH 的用户名和主机名进行严格的过滤和验证，拒绝包含特定 shell 元字符的输入。

防火墙与入侵检测： 加强网络层面的防护，如配置防火墙规则限制非必要的 SSH 连接，使用入侵检测系统（IDS）或入侵防御系统（IPS）监控异常的 SSH 流量模式，及时发现并阻止潜在的攻击尝试。

后续行动：

对于系统管理员和安全团队来说，应对所有运行 OpenSSH 的服务器和客户端进行排查，确认是否受到 CVE-2023-51385 影响，并按照上述建议采取相应的补救措施。同时，保持关注官方的安全公告和更新，以应对任何新出现的安全威胁或漏洞修复情况。定期进行安全审计和漏洞扫描也是维持系统安全的重要手段。

# OpenSSH 安全漏洞（CVE-2023-48795）

CVE-2023-48795 是一个针对 OpenSSH 软件的安全漏洞，具体涉及 SSH（Secure Shell）加密网络协议。以下是对该漏洞的概述、影响、以及修复方法的总结：

### 概述与影响：
- **漏洞名称**：CVE-2023-48795
- **漏洞类型**：SSH 协议前缀截断攻击（Terrapin Attack）
- **发现者**：德国波鸿鲁尔大学的研究人员
- **影响组件**：OpenSSH，一个广泛使用的 Secure Shell (SSH) 协议的开源实现
- **威胁级别**：高危（根据关联信息，未明确提及具体 CVSS 分值，但通常针对 OpenSSH 的严重漏洞会被评级为高危）
- **攻击利用**：攻击者能够通过此漏洞攻击 SSH 连接的完整性，从而破坏安全性。具体来说，该漏洞允许攻击者实施所谓的“Terrapin Attack”，这可能导致连接的加密级别被降级，或者可能影响其他连接参数，从而削弱保护措施，使攻击者能够在未经授权的情况下访问或操纵受保护的资源。

### 描述：
Terrapin Attack 是一种针对 SSH 协议的新攻击手段，它利用了协议处理过程中的某个弱点，能够干扰正常的协商过程，使得客户端和服务端之间的通信受到不利影响。攻击者可能借此绕过预期的加密设置，降低加密强度，或者利用其他协议层面的妥协来达到其攻击目的。

### 修复方法与应对措施：
- **升级 OpenSSH 版本**：官方已通过发布 OpenSSH 9.6 版本来修复 CVE-2023-48795 漏洞。用户应尽快将系统中运行的 OpenSSH 升级到至少 9.6 版本，以消除此特定漏洞的风险。
- **备份与配置更新**：在升级 OpenSSH 之前，建议备份现有的相关配置文件和密钥。升级完成后，可能需要根据新的软件版本调整或更新配置，并确保所有必要的权限设置和访问控制保持有效。
- **复扫描与验证**：完成升级后，应进行安全复扫描以确认漏洞已得到修复，并且系统的 SSH 服务已按预期工作，没有引入新的安全问题。

### 补充说明：
- 除了 CVE-2023-48795，上述信息还提到了其他 OpenSSH 的安全漏洞，如 CVE-2023-38408、CVE-2023-51384 和 CVE-2023-51385，这些也需要关注并采取相应的更新措施以保障系统整体的安全性。
- 定期检查和应用安全更新是维护系统安全的重要步骤。对于关键基础设施和敏感环境，应当密切关注官方安全公告、漏洞数据库（如 National Vulnerability Database, NVD），以及安全研究机构发布的警报，及时响应并采取措施应对新发现的安全威胁。

综上所述，要防护 CVE-2023-48795 漏洞，管理员应立即升级到 OpenSSH 9.6 或更高版本，并遵循常规的安全管理流程，包括备份、配置更新、以及验证修复效果。同时，持续关注并修补其他相关的 OpenSSH 安全漏洞以全面加强系统的 SSH 服务防护。

# 目标主机使用了不受支持的SSL加密算法【原理扫描】



# 升级到最新Openssh版本
首先安装 telnet（见：./telnet.md)，避免ssh升级失败后无法连接的问题。


其次，按 openssh-rpms 项目的说明升级
https://github.com/boypt/openssh-rpms


## Usage

### Build RPMs

1. Install build requirements listed above.
2. Edit `version.env` file if necessary.
3. Download source packages.
```bash
./pullsrc.sh
```
if any error comes up, manally download the source files into the `downloads` dir.
4. Run the script to build RPMs. 
```bash
./compile.sh
```

### Install RPMs

```bash
# Go go the generated RPMS directory.
cd $(./compile.sh RPMDIR)
pwd
ls
# you will find multiple RPM files in this directory.
# you may copy them to other machines, and continue following steps there.

# Backup current SSH config
[[ -f /etc/ssh/sshd_config ]] && mv /etc/ssh/sshd_config /etc/ssh/sshd_config.$(date +%Y%m%d)

# Install rpm packages. Exclude all debug packages.
find . ! -name '*debug*' -name '*.rpm' | xargs sudo yum --disablerepo=* localinstall -y

# in case host key files got permissions too open.
chmod -v 600 /etc/ssh/ssh_host_*_key

# For CentOS7+:
# in some cases previously installed systemd unit file is left on disk after upgrade.
# causes systemd mixing unit files and initscripts units provided by this package.
if [[ -d /run/systemd/system && -f /usr/lib/systemd/system/sshd.service ]]; then
    mv /usr/lib/systemd/system/sshd.service /usr/lib/systemd/system/sshd.service.$(date +%Y%m%d)
    systemctl daemon-reload
fi

# Check Installed version:
ssh -V && /usr/sbin/sshd -V

# Restart service
service sshd restart
```


## 升级后无法远程登录处理
1.查看/etc/ssh/sshd_config配置
```
常见的修改配置有：PermitRootLogin yes、PubkeyAuthentication yes、PasswordAuthentication yes

注意：如果是   #PermitRootLogin yes ，请将注释取消
```

2.使用yum直接安装openssh的rpm包，会修改/etc/pam.d/sshd文件，会导致服务器无法远程。（有些安装过后没有影响，有些不行）
如果不行，需要修改/etc/pam.d/sshd文件内容如下：
```
#%PAM-1.0
auth       required     pam_sepermit.so
auth       include      password-auth
account    required     pam_nologin.so
account    include      password-auth
password   include      password-auth
# pam_selinux.so close should be the first session rule
session    required     pam_selinux.so close
session    required     pam_loginuid.so
# pam_selinux.so open should only be followed by sessions to be executed in the user context
session    required     pam_selinux.so open env_params
session    optional     pam_keyinit.so force revoke
session    include      password-auth
```


## nginx禁用WebDAV

WebDAV （Web-based Distributed Authoring and Versioning）是基于 HTTP 1.1 的一个通信协议。它为 HTTP 1.1 添加了一些扩展（就是在 GET、POST、HEAD 等几个 HTTP 标准方法以外添加了一些新的方法），使得应用程序可以直接将文件写到 Web Server 上，并且在写文件时候可以对文件加锁，写完后对文件解锁，还可以支持对文件所做的版本控制。这个协议的出现极大地增加了 Web 作为一种创作媒体对于我们的价值。基于 WebDAV 可以实现一个功能强大 的内容管理系统或者配置管理系统。

NGINX中如何禁止DELETE、PUT、OPTIONS、TRACE、HEAD等协议访问应用程序应用程序呢？

在server范围加入

if ($request_method !~* GET|HEAD|POST) {
    return 403;
}

