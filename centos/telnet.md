# CentOS7开启telnet服务端，配合进行ssh升级

默认情况下，linux'操作系统我们都是通过ssh进行远程操作，开启telnet一般是在升级ssh的时候需要，开启telnet是为了防止升级ssh之后，无法通过ssh登录系统，可用telnet登录。

### **1.查找telnet安装包**

**前置条件：服务器可访问互联网**

安装之前首先查找telnet的安装包

```text
[root@localhost ~]# yum list | grep telnet-server
telnet-server.x86_64                      1:0.17-66.el7            updates
[root@localhost ~]# yum list | grep xinetd
xinetd.x86_64                               2:2.3.15-14.el7          base
```

### 2.执行安装（yum）

使用`yum -y install telnet-server.x86_64`命令安装telnet服务器

```text
[root@localhost ~]# yum -y install telnet-server.x86_64
Loaded plugins: fastestmirror
········（过程省略）
Installed:telnet-server.x86_64 1:0.17-66.el7

Complete!
```

使用`yum -y install xinetd.x86_64`命令安装xinetd守护进程

```text
[root@localhost ~]# yum -y install xinetd.x86_64
Loaded plugins: fastestmirror
········（过程省略）
Installed:xinetd.x86_64 2:2.3.15-14.el7

Complete!
```

### 3.**配置并启动**

### 3.1 配置开机启动

使用`systemctl enable xinetd.service`与`systemctl enable telnet.socket`命令将telnet服务设置为开机自动启动。

```text
[root@localhost ~]# systemctl enable xinetd.service
[root@localhost ~]# systemctl enable telnet.socket
Created symlink from /etc/systemd/system/sockets.target.wants/telnet.socket to /usr/lib/systemd/system/telnet.socket.
```

### 3.2 启动服务

使用`systemctl start telnet.socket`与`systemctl start xinetd`命令启动telnet服务

```text
[root@localhost ~]# systemctl start telnet.socket
[root@localhost ~]# systemctl start xinetd
```

使用`netstat -ntlp`命令查看运行中的端口（如果提示command not found，可运行`yum -y install net-tools`命令安装网络工具包。）

```text
[root@localhost ~]# yum -y install net-tools#安装网络工具包
Loaded plugins: fastestmirror
········（过程省略）
Installed:net-tools.x86_64 0:2.0-0.25.20131004git.el7

Complete!
[root@localhost ~]# netstat -ntlp   #查看端口
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp      0    0 0.0.0.0:22            0.0.0.0:*               LISTEN    987/sshd
tcp      0    0 127.0.0.1:25          0.0.0.0:*               LISTEN    1146/master
tcp6       0    0 :::22                   :::*                  LISTEN    987/sshd
tcp6       0    0 :::23                   :::*                  LISTEN    1/systemd
tcp6       0    0 ::1:25                :::*                  LISTEN    1146/master

```

可以看到，telnet服务已经开启，端口号为23。

**注意：完成上述步骤后，还不能通过telnet进行登录，因为centos初始安装后默认开启了firewalld防火墙服务，且默认只打开了22端口。还需要进行防火墙配置。**

### **3.3 配置防火墙规则**

通过`firewall-cmd --list-all`命令查看防火墙配置

```text
[root@localhost ~]# firewall-cmd --list-all
public (active)target: defaulticmp-block-inversion: nointerfaces: ens33sources:services: dhcpv6-client sshports:protocols:masquerade: noforward-ports:source-ports:icmp-blocks:rich rules:
```

添加23端口，并重启防火墙。配置防火墙有两种方式可选：

- 直接对外开放23端口
- 只对特定地址的终端开启23端口（推荐）

第一种方式比较简单，安全性低，操作如下：

```text
[root@localhost ~]# firewall-cmd --add-port=23/tcp --permanent   #永久开启23端口，--permanent 为永久开启，不加此参数重启防火墙后规则不保存
success
[root@localhost ~]# firewall-cmd --reload   #重启防火墙
success
[root@localhost ~]# firewall-cmd --list-all  #查看规则
public (active)target: defaulticmp-block-inversion: nointerfaces: ens33sources:services: dhcpv6-client sshports: 23/tcp
```

第二种方式安全性高，甚至如果只对某一台终端开启23端口，后续可以不卸载（当然建议还是卸载了吧），操作如下：

```text
[root@localhost ~]# firewall-cmd--permanent --add-rich-rule='rule family=ipv4 source address=192.168.12.1 port protocol=tcp port=23 accept'  #--permanent 为永久开启，不加此参数重启防火墙后规则不保存
success
[root@localhost ~]# firewall-cmd --reload
success
[root@localhost ~]# firewall-cmd --list-all   #查看规则
public (active)target: defaulticmp-block-inversion: nointerfaces: ens33
 ······（省略）rich rules:
      rule family="ipv4" source address="192.168.12.1" port port="23" protocol="tcp" accept

```

其中source address就是需要访问服务器的终端的ip地址。**第二种防火墙配置非常实用，在服务器安全中，强烈推荐多实用第二种防火墙配置方式，比如数据库服务器配置只允许特定的应用服务器访问，能大幅提高服务器环境安全。**

### 3.4 安全配置

此时还无法使用root登录，会提示登录错误。因为默认root无法远程访问。

```text
Kernel 3.10.0-1127.el7.x86_64 on an x86_64
localhost login: root
Password:
Login incorrect
```

还需要修改`/etc/securetty`文件

```text
[root@localhost ~]# vim /etc/securetty
```

在末尾添加以下内容：

```text
pts/0
pts/1
```

最终内容如下：

```text
console
·······（省略）
xvc0
pts/0
pts/1
~
```

### 4 测试验证

在windows上测试通过telnet登录linux

```text
C:\Users\11>telnet 192.168.12.101                
Kernel 3.10.0-1127.el7.x86_64 on an x86_64 
localhost login:

(输入账户密码)
Last failed login: Wed Mar 30 22:45:11 CST 2022 from ::ffff:192.168.12.1 on pts/0 
There was 1 failed login attempt since the last successful login.  
Last login: Wed Mar 30 19:04:51 from 192.168.12.1
```

### 5 关闭telnet

关闭telnet很简单，有很多种方式可以关闭。

- 去除防火墙配置
- 停止telnet服务并取消开机启动
- 彻底移除telnet安装包

### 5.1 去除防火墙配置

去除防火墙配置根据不容的配置方式，使用不同的命令进行移除。**核心实际上就是将添加防火墙配置的命令中的`add`替换为`remove`即可。**

```text
[root@localhost ~]# firewall-cmd --remove-port=23/tcp --permanent   #针对直接对外开放23端口的配置的移除
success
[root@localhost ~]#firewall-cmd--permanent --remove-rich-rule='rule family=ipv4 source address=192.168.12.1 port protocol=tcp port=23 accept'   #针对第二种方式的防火墙配置的移除
success
[root@localhost ~]# firewall-cmd --reload   #重启防火墙
success
[root@localhost ~]# firewall-cmd --list-all   #查看配置
public (active)target: defaulticmp-block-inversion: nointerfaces: ens33sources:services: dhcpv6-client sshports:protocols:masquerade: noforward-ports:source-ports:icmp-blocks:rich rules:
```

### 5.2 停止telnet服务并取消开机启动

停止服务与开启服务命令类似，将开启服务的`start`换为`stop`即可。

```text
[root@localhost ~]# systemctl stop telnet.socket
[root@localhost ~]# systemctl stop xinetd
```

取消开机启动原理一样，将enable替换为disable即可。

```text
[root@localhost ~]# systemctl disable xinetd.service
Removed symlink /etc/systemd/system/multi-user.target.wants/xinetd.service.
[root@localhost ~]#systemctl disable telnet.socket
Removed symlink /etc/systemd/system/sockets.target.wants/telnet.socket.
```

### 5.3 彻底移除telnet安装包

移除安装包与安装安装包类似，将install替换为remove即可。（是不是发现了什么新大陆）

```text
[root@localhost ~]# yum -y remove telnet-server.x86_64
Loaded plugins: fastestmirror
········（过程省略）
Installed:telnet-server.x86_64 1:0.17-66.el7

Complete!

[root@localhost ~]# yum -y remove xinetd.x86_64
Loaded plugins: fastestmirror
········（过程省略）
Installed:xinetd.x86_64 2:2.3.15-14.el7

Complete!
```

至此，关于在CentOS7上安装telnet、卸载telnet的方法就完全介绍完了，希望对大家有用。