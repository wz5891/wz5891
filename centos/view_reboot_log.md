查看 Linux 重启记录


方法 1：使用 `last` 命令
```
last reboot
```
方法 2：使用 `uptime` 命令
```
uptime
```
方法 3：使用 `journalctl` 命令
```
sudo journalctl --list-boots
```

方法 4：使用 `who -b` 命令
```
who -b
```