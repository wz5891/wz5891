```
@ehco off
del /Q db-server
del /Q engine-serve
del /Q web-server

mkdir db-server
mkdir engine-server
mkdir web-server


xcopy /E /I /Y ..\docker-setup db-server\docker-setup
xcopy /E /I /Y ..\docker-compose\db-server db-server\docker-compose\db-server
xcopy /E /I /Y ..\docker-compose\keys db-server\docker-compose\keys
copy /Y ..\docker-compose\app.ini db-server\docker-compose
copy /Y ..\docker-compose\common.yml db-server\docker-compose


xcopy /E /I /Y ..\docker-setup engine-server\docker-setup
xcopy /E /I /Y ..\docker-compose\engine-server engine-server\docker-compose\engine-server
xcopy /E /I /Y ..\docker-compose\keys engine-server\docker-compose\keys
copy /Y ..\docker-compose\app.ini engine-server\docker-compose
copy /Y ..\docker-compose\common.yml engine-server\docker-compose


xcopy /E /I /Y ..\docker-setup web-server\docker-setup
xcopy /E /I /Y ..\docker-compose\web-server web-server\docker-compose\web-server
xcopy /E /I /Y ..\docker-compose\keys web-server\docker-compose\keys
copy /Y ..\docker-compose\app.ini web-server\docker-compose
copy /Y ..\docker-compose\common.yml web-server\docker-compose
```


xcopy ..\ D:\backup /E /I

..\ 表示上一级目录。
/E 参数表示包括所有子目录、子目录下的所有文件。
/I 如果目标目录不存在，则创建。