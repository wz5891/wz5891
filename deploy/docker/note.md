## docker镜像导出

```bash
docker save -o image.tar image_name

docker save image_name|gzip > image.tar.gz
```

## docker镜像导入

```bash
docker load -i image.tar
```

## docker镜像删除

```bash
docker rmi image_name
```

## docker镜像查看

```bash
docker images
```


## 将容器提交为镜像
```
docker commit -m "添加windows常用字体" -a "liwei" pdfconver_rest pdfconver_rest:v1.0.0
```
