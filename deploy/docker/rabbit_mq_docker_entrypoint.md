

## RabbitMq配置

https://github.com/docker-library/rabbitmq.git

Dockerfile
```bash
FROM debian:jessie

# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
RUN groupadd -r rabbitmq && useradd -r -d /var/lib/rabbitmq -m -g rabbitmq rabbitmq

# grab gosu for easy step-down from root
ENV GOSU_VERSION 1.7
RUN set -x \
	&& apt-get update && apt-get install -y --no-install-recommends ca-certificates wget && rm -rf /var/lib/apt/lists/* \
	&& wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture)" \
	&& wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture).asc" \
	&& export GNUPGHOME="$(mktemp -d)" \
	&& gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
	&& gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
	&& rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc \
	&& chmod +x /usr/local/bin/gosu \
	&& gosu nobody true \
	&& apt-get purge -y --auto-remove ca-certificates wget

# Add the officially endorsed Erlang debian repository:
# See:
#  - http://www.erlang.org/download.html
#  - https://www.erlang-solutions.com/downloads/download-erlang-otp
RUN apt-key adv --keyserver ha.pool.sks-keyservers.net --recv-keys 434975BD900CCBE4F7EE1B1ED208507CA14F4FCA
RUN echo 'deb http://packages.erlang-solutions.com/debian jessie contrib' > /etc/apt/sources.list.d/erlang.list

# get logs to stdout (thanks @dumbbell for pushing this upstream! :D)
ENV RABBITMQ_LOGS=- RABBITMQ_SASL_LOGS=-
# https://github.com/rabbitmq/rabbitmq-server/commit/53af45bf9a162dec849407d114041aad3d84feaf

# http://www.rabbitmq.com/install-debian.html
# "Please note that the word testing in this line refers to the state of our release of RabbitMQ, not any particular Debian distribution."
RUN apt-key adv --keyserver ha.pool.sks-keyservers.net --recv-keys 0A9AF2115F4687BD29803A206B73A36E6026DFCA
RUN echo 'deb http://www.rabbitmq.com/debian testing main' > /etc/apt/sources.list.d/rabbitmq.list

ENV RABBITMQ_VERSION 3.6.2
ENV RABBITMQ_DEBIAN_VERSION 3.6.2-1

RUN apt-get update && apt-get install -y --no-install-recommends \
		erlang-nox erlang-mnesia erlang-public-key erlang-crypto erlang-ssl erlang-asn1 erlang-inets erlang-os-mon erlang-xmerl erlang-eldap \
		rabbitmq-server=$RABBITMQ_DEBIAN_VERSION \
	&& rm -rf /var/lib/apt/lists/*

# /usr/sbin/rabbitmq-server has some irritating behavior, and only exists to "su - rabbitmq /usr/lib/rabbitmq/bin/rabbitmq-server ..."
ENV PATH /usr/lib/rabbitmq/bin:$PATH

RUN echo '[{rabbit, [{loopback_users, []}]}].' > /etc/rabbitmq/rabbitmq.config

# set home so that any `--user` knows where to put the erlang cookie
ENV HOME /var/lib/rabbitmq

RUN mkdir -p /var/lib/rabbitmq /etc/rabbitmq \
	&& chown -R rabbitmq:rabbitmq /var/lib/rabbitmq /etc/rabbitmq \
	&& chmod 777 /var/lib/rabbitmq /etc/rabbitmq
VOLUME /var/lib/rabbitmq

# add a symlink to the .erlang.cookie in /root so we can "docker exec rabbitmqctl ..." without gosu
RUN ln -sf /var/lib/rabbitmq/.erlang.cookie /root/

RUN ln -sf /usr/lib/rabbitmq/lib/rabbitmq_server-$RABBITMQ_VERSION/plugins /plugins

COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 4369 5671 5672 25672
CMD ["rabbitmq-server"]
```



docker-entrypoint.sh
```bash
#!/bin/bash
set -e

# allow the container to be started with `--user`
if [ "$1" = 'rabbitmq-server' -a "$(id -u)" = '0' ]; then
	chown -R rabbitmq /var/lib/rabbitmq
	exec gosu rabbitmq "$BASH_SOURCE" "$@"
fi

ssl=
if [ "$RABBITMQ_SSL_CERT_FILE" -a "$RABBITMQ_SSL_KEY_FILE" -a "$RABBITMQ_SSL_CA_FILE" ]; then
	ssl=1
fi

# If long & short hostnames are not the same, use long hostnames
if [ "$(hostname)" != "$(hostname -s)" ]; then
	export RABBITMQ_USE_LONGNAME=true
fi

if [ "$RABBITMQ_ERLANG_COOKIE" ]; then
	cookieFile='/var/lib/rabbitmq/.erlang.cookie'
	if [ -e "$cookieFile" ]; then
		if [ "$(cat "$cookieFile" 2>/dev/null)" != "$RABBITMQ_ERLANG_COOKIE" ]; then
			echo >&2
			echo >&2 "warning: $cookieFile contents do not match RABBITMQ_ERLANG_COOKIE"
			echo >&2
		fi
	else
		echo "$RABBITMQ_ERLANG_COOKIE" > "$cookieFile"
		chmod 600 "$cookieFile"
	fi
fi

if [ "$1" = 'rabbitmq-server' ]; then
	configs=(
		# https://www.rabbitmq.com/configure.html
		default_pass
		default_user
		default_vhost
		ssl_ca_file
		ssl_cert_file
		ssl_key_file
	)

	haveConfig=
	for conf in "${configs[@]}"; do
		var="RABBITMQ_${conf^^}"
		val="${!var}"
		if [ "$val" ]; then
			haveConfig=1
			break
		fi
	done

	if [ "$haveConfig" ]; then
		cat > /etc/rabbitmq/rabbitmq.config <<-'EOH'
			[
			  {rabbit,
			    [
		EOH
		
		if [ "$ssl" ]; then
			cat >> /etc/rabbitmq/rabbitmq.config <<-EOS
			      { tcp_listeners, [ ] },
			      { ssl_listeners, [ 5671 ] },
			      { ssl_options,  [
			        { certfile,   "$RABBITMQ_SSL_CERT_FILE" },
			        { keyfile,    "$RABBITMQ_SSL_KEY_FILE" },
			        { cacertfile, "$RABBITMQ_SSL_CA_FILE" },
			        { verify,   verify_peer },
			        { fail_if_no_peer_cert, true } ] },
			EOS
		else
			cat >> /etc/rabbitmq/rabbitmq.config <<-EOS
			      { tcp_listeners, [ 5672 ] },
			      { ssl_listeners, [ ] },
			EOS
		fi
		
		for conf in "${configs[@]}"; do
			[ "${conf#ssl_}" = "$conf" ] || continue
			var="RABBITMQ_${conf^^}"
			val="${!var}"
			[ "$val" ] || continue
			cat >> /etc/rabbitmq/rabbitmq.config <<-EOC
			      {$conf, <<"$val">>},
			EOC
		done
		cat >> /etc/rabbitmq/rabbitmq.config <<-'EOF'
			      {loopback_users, []}
		EOF

		# If management plugin is installed, then generate config consider this
		if [ "$(rabbitmq-plugins list -m -e rabbitmq_management)" ]; then
			cat >> /etc/rabbitmq/rabbitmq.config <<-'EOF'
				    ]
				  },
				  { rabbitmq_management, [
				      { listener, [
			EOF

			if [ "$ssl" ]; then
				cat >> /etc/rabbitmq/rabbitmq.config <<-EOS
				      { port, 15671 },
				      { ssl, true },
				      { ssl_opts, [
				          { certfile,   "$RABBITMQ_SSL_CERT_FILE" },
				          { keyfile,    "$RABBITMQ_SSL_KEY_FILE" },
				          { cacertfile, "$RABBITMQ_SSL_CA_FILE" },
				      { verify,   verify_none },
				      { fail_if_no_peer_cert, false } ] } ] }
				EOS
			else
				cat >> /etc/rabbitmq/rabbitmq.config <<-EOS
				        { port, 15672 },
				        { ssl, false }
				        ]
				      }
				EOS
			fi
		fi

		cat >> /etc/rabbitmq/rabbitmq.config <<-'EOF'
			    ]
			  }
			].
		EOF
	fi

	if [ "$ssl" ]; then
		# Create combined cert
		cat "$RABBITMQ_SSL_CERT_FILE" "$RABBITMQ_SSL_KEY_FILE" > /tmp/combined.pem
		chmod 0400 /tmp/combined.pem

		# More ENV vars for make clustering happiness
		# we don't handle clustering in this script, but these args should ensure
		# clustered SSL-enabled members will talk nicely
		export ERL_SSL_PATH="$(erl -eval 'io:format("~p", [code:lib_dir(ssl, ebin)]),halt().' -noshell)"
		export RABBITMQ_SERVER_ADDITIONAL_ERL_ARGS="-pa '$ERL_SSL_PATH' -proto_dist inet_tls -ssl_dist_opt server_certfile /tmp/combined.pem -ssl_dist_opt server_secure_renegotiate true client_secure_renegotiate true"
		export RABBITMQ_CTL_ERL_ARGS="$RABBITMQ_SERVER_ADDITIONAL_ERL_ARGS"
	fi
fi

exec "$@"
```


## ENTRYPOINT说明
ENTRYPOINT指定镜像的默认入口命令，该入口命令会在启动容器时作为根命令执行，所有传入值作为该命令的参数。
支持两种格式：
ENTRYPOINT ["executable", "param1", "param2"]（exec调用执行）；
ENTRYPOINT command param1 param2（shell中执行）。
此时，CMD指令指定值将作为根命令的参数。

每个Dockerfile中只能有一个ENTRYPOINT，当指定多个时，只有最后一个有效。
在运行时，可以被--entrypoint参数覆盖掉，如docker run --entrypoint。



见：
Docker技术入门与实战（第2版）
[第8章 使用Dockerfile创建镜像](https://weread.qq.com/web/reader/27f328105d02fe27f9bd2e5kc20321001cc20ad4d76f5ae)



## Shell命令行参数
bash shell会将所有的命令行参数都指派给称作位置参数（positional parameter）的特殊变量。这也包括shell脚本名称。位置变量[插图]的名称都是标准数字：$0对应脚本名，$1对应第一个命令行参数，$2对应第二个命令行参数，以此类推，直到$9。
```
$ cat positional1.sh
#!/bin/bash
# Using one command-line parameter
#
factorial=1
for (( number = 1; number <= $1; number++ ))
do
     factorial=$[ $factorial * $number ]
done
echo The factorial of $1 is $factorial
exit
$
$ ./positional1.sh 5
The factorial of 5 is 120
$
```
见：
Linux命令行与shell脚本编程大全（第4版）
[14.1　传递参数](https://weread.qq.com/web/reader/48432660813ab6fe3g0106fdk98f3284021498f137082c2e)