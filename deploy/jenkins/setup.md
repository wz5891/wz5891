https://www.jenkins.io/zh

```
https://www.jenkins.io/zh
```

官方镜像
```
docker pull jenkins/jenkins
```


https://blog.csdn.net/admin_2022/article/details/130348038

```
chown -R 1000:1000 /home/cxs-jenkins/jenkins_home
chown -R 1000:1000 /usr/local/maven3.6.0
chown -R 1000:1000 /usr/local/jdk8


```

```
version: '3.1'
services:
  cxs-jenkins:
    image: jenkins/jenkins
    container_name: cxs-jenkins
    ports:
      - "8090:8080"
      - "8091:50000"
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /home/cxs-jenkins/jenkins_home:/var/jenkins_home
      - /usr/local/apache-maven-3.6.3:/usr/local/apache-maven-3.6.3
      - /usr/local/jdk1.8:/usr/local/jdk1.8
```

监听日志，获取管理员密码
```
docker-compose logs -f
```

