# Linux系统时间不同步
现象：服务器不能连网，通过`timedatectl set-time`设置时间后，没过多久又会慢下来，最终会慢2分多钟。查看硬件时间`hwclock`，可以看到这个时间是准的，不会慢。

方案：创建定时任务，定时将硬件时间同步为系统时间
```
crontab -e
```
添加
```
# 每分钟将硬件时间同步为系统时间
*/1 * * * * sh hwclock -s
```

> 执行`timedatectl set-time` 报错，需要先执行 `timedatectl set-ntp no` 关闭自动同步

## 查看Linux版本
```
# 查看操作系统版本
cat /etc/os-release

# 查看内核版本
uname -r
```

# 查看Linux最近重启时间
```
# 查看最近一次重启
[root@localhost ~]# who -b
         system boot  2023-05-22 04:37
# 查看最近n次重启日志
[root@localhost ~]# last reboot | head -2
reboot   system boot  3.10.0-862.el7.x Mon May 22 04:37 - 04:46  (00:09)    
reboot   system boot  3.10.0-862.el7.x Mon May 22 04:36 - 04:37  (00:01)   

# 查看最近一次关机 
[root@localhost ~]# last -x|grep shutdown | head -1 
shutdown system down  3.10.0-862.el7.x Mon May 22 04:37 - 04:37  (00:00)   

# 查看最近启动时间 
[root@localhost ~]# uptime -s
2023-05-22 04:37:30
[root@localhost ~]# uptime
 04:48:16 up 10 min,  1 user,  load average: 0.00, 0.08, 0.10
```

## Centos 配置swap
1、检查是否有交换分区了，如果有输出交换分区，关闭
```
swapon --show
swapoff -a
```
2、创建16G的交换分区
```
# 方式一
fallocate -l 16G /swapfile
# 方式二，如果是centos 7 并且是xfs文件格式，使用dd
dd if=/dev/zero of=/swapfile bs=1G count=16
```
3、设置交换分区
```
chmod 600 /swapfile
mkswap /swapfile
```
4、启用交换分区
```
swapon /swapfile
```
5、配置系统启动时自动挂载分区
```
"/swapfile   swap    swap    defaults   0   0 " >> /etc/fstab
```
6、重启系统
```
reboot
```
要注意的就是第二步，如果不是centos 6/7 或者是ext文件系统，还是建议用fallocate

## 如何在 CentOS 中下载包含所有依赖项的 RPM 包
Yumdownloader 可以一次性下载任何 RPM 包以及所有必需的依赖项

安装 Yumdownloader
```
yum install yum-utils
```

要将软件包和所有依赖项一起下载到特定位置（若不使用destdir参数，则下载到当前目录）
```
yumdownloader --resolve --destdir=/root/mypackages/ httpd
```

## RPM安装

```
rpm -ivh *.rpm
rpm -Uvh *.rpm
```

-i：安装（install）;
-v：显示更详细的信息（verbose）
-h：打印 #，显示安装进度（hash）
-U（大写）：如果该软件没安装过则直接安装；若已经安装则升级至最新版本

RPM 软件包的卸载
```
rpm -e 包名
```
-e：卸载，是 erase 的首字母

## virt-manger安装教程

```
yum install -y qemu-kvm
yum install -y libvirt
yum install -y virt-manager
```

重启libvirtd服务
``` 
[root@loaclhost Mirror]# service libvirtd restart
Redirecting to /bin/systemctl restart libvirtd.service
```

验证libvirt是否正常启动

``` 
[root@loaclhost Mirror]#  virsh version
根据库编译：libvirt 4.5.0
使用库：libvirt 4.5.0
使用的 API: QEMU 4.5.0
运行管理程序: QEMU 1.5.3
```

输入命令：virt-manager，就可以自动弹出kvm管理软件

## ntfs支持
```
yum install epel-release
yumdownloader --resolve ntfs-3g
```

安装
```
rpm -ivh *.rpm
```

```
fdisk  -l
```

```
mount -t ntfs-3g /dev/sdaX /mnt/data1
```
/dev/sdaX是你要挂载的NTFS分区，/mnt/data1 是你要挂载的目录。

## U盘启动盘制作
https://rufus.ie/zh/

## 查找端口对应的进程
```
netstat -ap|grep 端口号
ps -ef|grep 进行号
```

## 查找大文件
```
find . -size +100M
```

## 防火墙
```
firewall-cmd --zone=public --add-port=80/tcp --permanent

firewall-cmd --reload
firewall-cmd --list-port
```

## yumdownloader
yum install安装完之后会自动清理安装包，如果只想通过yum下载软件的安装包，但是不需要进行安装的话，可以使用 yumdownloader 命令。

yumdownloader 命令在软件包 yum-utils 里面。先安装 yum-utils ：
```
yum install yum-utils -y
```
查看 yum-utils 软件包有没有 yumdownloader，如果有输出代表可用：
```
rpm -ql yum-utils |grep yumdownloader
```

```
# yumdownloader vim --resolve --destdir=/home/vim
yumdownloader vim --resolve
# 也可以指定下载的目录
yumdownloader vim --resolve --destdir=/home/vim

rpm -Uvh /home/vim/*.rpm
```

## sed
```
sed -n '$p' 1.txt #打印文件的最后一行
sed -n '1,3p' 1.txt #打印1到3行
sed -n '3,$p' 1.txt #打印从第3行到最后一行的内容
sed -n '1,/too/p' 1.txt #打印第一行到匹配too的行
sed -n '/too/,$p' 1.txt #打印从匹配too的行到最后一行的内容
```

## 查看系统信息
```
# 查看cpu
lscpu

# 查看内存
free -h

# 查看磁盘
df -h
```

## 查看目录大小

```
# 汇总-s（只看一级子目录），类似于--max-depth=1
du -s *

#限制目录深度--max-depth=1（相当于-s），0只看当前目录。
du --max-depth=1 *

#可读方式显示大小-h（显示GB/MB/KB）
du -sh *

#排序（注意排序不能加-h）
du -s * | sort -rn

# 查看磁盘挂载
df -h
```

## Linux grep 显示前后几行的信息
```
grep -A 5 可以显示匹配内容以及后面的5行内容
grep -B 5 可以显示匹配内容以及前面的5行内容
grep -C 5 可以显示匹配内容以及前后面的5行内容

$ cat size.txt | grep 'a022021' -C 2
m7187998
m7282064
a022021
a061048
m9324822
```

## centos安装字体
```
yum install -y mkfontscale
yum install -y fontconfig

cd /usr/share/fonts

mkfontscale
mkfontdir
fc-cache -fv
fc-list
```

## 创建用户
https://www.myfreax.com/how-to-create-users-in-linux-using-the-useradd-command/

```
useradd -m appdeploy
passwd appdeploy

# 加入docker组才有权限运行docker
usermod -aG docker appdeploy
```