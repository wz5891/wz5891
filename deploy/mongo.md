# 创建Mongo

## 目录
```
E:.
|   docker-compose.yml
+---data
\---init
        create_mongo_user.js
        create_mongo_user.sh
```

## docker-compose.yml
```
version: '3'
services:
  mongo:
    image: mongo:4.4.5
    container_name: mongo
    ports:
      - 27017:27017
    privileged: true
    restart: always
    environment:
      TZ: Asia/Shanghai
      MONGO_INITDB_ROOT_USERNAME: admin
      MONGO_INITDB_ROOT_PASSWORD: "admin#123Abc"
    command: --auth
    volumes:
      - ./data:/data/db
```

## create_mongo_user.js
```
// 连接URL
var url = "mongodb://admin:admin#123Abc@127.0.0.1:27017/admin"
var con = new Mongo(url)

// 切换到数据库
var db = con.getDB('admin')

db.createUser(
{
    user:'form',
    pwd:'form#12@345Ab!',
    customData:{
        desc:'The read write user for form database'
    },
    roles: [{ role: "readWrite", db: "form" }]
}
);
```

## create_mongo_user.sh
```
#!/bin/bash
docker cp ./create_mongo_user.js mongo:/create_mongo_user.js
docker exec -it mongo mongo /create_mongo_user.js
```

## 执行
```
cd /data/mongo
docker-compose up -d
chmod +x init/create_mongo_user.sh
init/create_mongo_user.sh
```