# windows 安装Nginx服务

# 1、下载windows版的 Nginx
[Nginx安装包](http://nginx.org/en/download.html),进入类似下图页面，选择合适版本下载。

![Alt text](assets/c3760c32b876bc180cbbc73627ecdbb3.png)

下载后解压，如下图

![Alt text](assets/4d759269e353da867f54cfd79f7f3327.png)

在C:\Program Files路径下创建Nginx文件夹，把解压后的内容放在Nginx文件下

# 2.下载 Windows Service Wrapper
[Windows Service Wrapper](http://repo.jenkins-ci.org/releases/com/sun/winsw/winsw/)工具，选择合适版本下载，该工具可设置nginx为windows服务。[Github源码地址](https://github.com/kohsuke/winsw)，下载页面如下图;

![image-20230526182402235](assets/image-20230526182402235.png)

下载后把下载的[WinSW-x64.exe](https://github.com/winsw/winsw/releases/download/v2.12.0/WinSW-x64.exe) 文件放在Nginx安装目录(C:\Program Files\Nginx)，
并修改名称为nginx-service.exe，然后分别创建nginx-service.exe.config，nginx-service.xml文件,把这两个文件放在Nginx安装目录下。如下图

![Alt text](assets/0b2f2788512e5ffa8698f71e2bd4cead.png)


nginx-service.exe.config内容如下:

```
<configuration>
  <startup>
    <supportedRuntime version="v2.0.50727" />
    <supportedRuntime version="v4.0" />
  </startup>
  <runtime>
    <generatePublisherEvidence enabled="false"/> 
  </runtime>
</configuration>
```



nginx-service.xml内容如下：
```
<service>
  <id>nginx</id>
  <name>Nginx Service</name>
  <description>High Performance Nginx Service</description>
  <logpath>C:\Program Files\Nginx\logs</logpath>
  <log mode="roll-by-size">
    <sizeThreshold>10240</sizeThreshold>
    <keepFiles>8</keepFiles>
  </log>
  <executable>C:\Program Files\Nginx\nginx.exe</executable>
  <startarguments>-p C:\Program Files\Nginx</startarguments>
  <stopexecutable>C:\Program Files\Nginx\nginx.exe</stopexecutable>
  <stoparguments>-p C:\Program Files\Nginx -s stop</stoparguments>
</service>
```

# 3.安装nginx服务

点击"开始"--"运行",输入"cmd"后点击确定按钮,进入DOS窗口,接下来分别运行以下命令:

```
cd C:\Program Files\Nginx
```

进入Nginx安装目录，然后执行命令

```
nginx-service.exe install
```

此时查看windows服务目录有Niginx Service服务，然后右键启动。

![Alt text](assets/dbf06d4b5e1faaf8ff714a52ca313eba.png)

# 4.常见问题处理以及注意事项

Niginx Service服务无法启动

![Alt text](assets/d65a6e85a53e232c973434d9cb131ef6.png)

原因是默认80端口被占用，在C:\Program Files\Nginx\conf目录下修改nginx.conf配置文件，修改为其他端口号；
如下图

![Alt text](assets/fa5bb28ac367aa6d822a1fe568690d2a.png)

windows检查80端口是否被占用：

点击"开始"--"运行",输入"cmd"后点击确定按钮,进入DOS窗口,接下来分别运行以下命令:

```
netstat -aon | findstr "80"
```

若是出现如下,可以看出80端口被进程号为1688的程序占用。

```
Proto  Local Address          Foreign Address        State            PID
====  ============      ==============  ==========  ======
TCP    0.0.0.0:80                    0.0.0.0:0                LISTENING      1688

   
若遇到其他问题可在C:\Program Files\Nginx\logs下查看错误日志，然后根据错误日志查找解决方案。
SQL 复制 全屏
```

**注意：每次修改完nginx.conf文件，需重新启动Niginx Service服务，方可生效。**



## 其它
Java应用做为服务
```
<service>
  <id>TEST-Server</id>
  <name>测试服务</name>
  <description>这是一个测试服务</description>
  <executable>java</executable>
  <arguments>-Dfile.encoding=utf-8 -jar D:\test.jar</arguments> 
  <startmode>Automatic</startmode>
  <log mode="none"/>
  <logpath>D:\test\logs</logpath>
</service>
```


Command
```
安装服务：install
启动服务：start
停止服务：stop
重启服务：restart
服务状态：status
卸载服务：uninstall
```