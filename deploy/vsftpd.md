vsftpd的一点SELinux设置


## 500 OOPS: cannot change directory
在开了SELinux和防火墙的情况下，使用chroot_local_user=YES锁定用户在自己的home目录中，没想到 vsftp的问题。FTP登录报错：500 OOPS: cannot change directory。

下面来看看产生这个问题的原因和对策。

打开SELinux后，SELinux会阻止ftp daemon读取用户home目录。也就是说当ftp目录被锁定在home时FTP会报错： “500 OOPS: cannot change directory”。

无法进入目录，也就会出错退出。


解决方法
1、降低SELinux安全级别，把enforcing降低到permissive。

此方法有效，但不可取，会降低系统的安全程度。

2、修改SELINUX策略（推荐）

getsebool -a | grep ftp
此命令可查看当前策略。


centos6 将ftp_home_dir设为on即可

```
setsebool -P ftp_home_dir 1
service vsftpd restart
```

centos7 将ftpd_full_access设为on即可。

```
 setsebool -P ftpd_full_access 1
 service vsftpd restart
```


## FTP服务慢
与ssh一样，vsftp的配置文件默认开启了DNS反向解析，这可能会造成用户在登陆到FTP服务器的时候奇慢无比，只要在配置文件中禁用DNS反向解析即可解决文件。
如下解决  

vim /etc/vsftpd/vsftpd.conf

在vsftpd.conf文件中加入：reverse_lookup_enable=NO

保存后重新启动vsftpd：  service vsftpd restart