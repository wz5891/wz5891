
按tutorial先创建项目

接下来

## 安装gojenkins
```
go get -u github.com/bndr/gojenkins
```

## 基本使用
```
package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/bndr/gojenkins"
)

func CustomCreateFolder(ctx context.Context, jenkins gojenkins.Jenkins, id string, parents ...string) (*gojenkins.Folder, error) {
	folder, err := jenkins.GetFolder(ctx, id, parents...)

	if err != nil {
		//return nil, fmt.Errorf("trouble get folder: %v", err)
		fmt.Printf("err: %v\n", err)
	}
	if folder != nil {
		return folder, nil
	}

	folder2, err := jenkins.CreateFolder(ctx, id, parents...)

	if err != nil {
		return nil, fmt.Errorf("trouble create folder: %v", err)
	}

	return folder2, nil
}

func main() {
	projectName := "lichengFood"

	// 连接Jenkins
	ctx := context.Background()
	jenkins := gojenkins.CreateJenkins(nil, "http://127.0.0.1:8090/", "admin", "123456")
	_, err := jenkins.Init(ctx)

	if err != nil {
		log.Printf("连接Jenkins失败, %v\n", err)
		return
	}
	log.Println("Jenkins连接成功")

	folder, err := CustomCreateFolder(ctx, *jenkins, projectName)

	if err != nil {
		fmt.Printf("err: %v\n", err)
	}
	fmt.Printf("folder: %v\n", folder)

	// 创建job
	configString := `<?xml version='1.1' encoding='UTF-8'?>
	<flow-definition plugin="workflow-job">
	<actions/>
	<description></description>
	<keepDependencies>false</keepDependencies>
	<properties/>
	<definition class="org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition" plugin="workflow-cps">
	<script>
	
	pipeline {
		agent any

		tools {
			jdk 'JDK1.8'
			maven 'Maven3.9.5'
		}
		
		stages {
			stage('pull test3 code') {
				steps {
					git credentialsId: 'gitee', url: 'https://gitee.com/wz5891/jenkins-java-helloword.git'
				}
			}
			
			stage('build test3 code') {
				steps {
					sh 'mvn clean package'
				}
			}
		}
	}
	
	</script>
	<sandbox>true</sandbox>
	</definition>
	<triggers/>
	<disabled>false</disabled>
	</flow-definition>`

	jobName := "lichengFood-all2"

	job, err := jenkins.CreateJob(ctx, configString, jobName)

	//job, err := jenkins.CreateJobInFolder(ctx, configString, jobName, projectName)
	if err != nil {
		fmt.Printf("err: %v\n", err)
		//panic(err)
	}

	if job != nil {
		log.Println("Job has been created in child folder")
	}

	log.Println("build job")
	//jenkins.BuildJob(ctx, jobName, nil)

	queueid, err := jenkins.BuildJob(ctx, jobName, nil)
	if err != nil {
		fmt.Printf("err: %v\n", err)

		panic(err)
	}
	build, err := jenkins.GetBuildFromQueueID(ctx, queueid)
	if err != nil {
		panic(err)
	}

	// Wait for build to finish
	for build.IsRunning(ctx) {
		time.Sleep(5000 * time.Millisecond)
		build.Poll(ctx)
	}

	fmt.Printf("build number %d with result: %v\n", build.GetBuildNumber(), build.GetResult())
}
```