## 创建项目
Go1.12版本之后，开始使用go mod模式来管理依赖环境

go mod init 项目名命令对项目进行初始化，该命令会在项目根目录下生成go.mod文件。
```
mkdir go_jenkins
cd go_jenkins
go mod init jenkins
```

运行之前可以使用​go mod tidy​命令将所需依赖添加到go.mod文件中，并且能去掉项目中不需要的依赖


## 编写代码并运行
```
touch test.go
```

编写内容
```
package main

import "fmt"

func main() {
	fmt.Println("Hello,world!")
}

```

编译并运行
```
E:\labs\go\jenkins>go run test.go
Hello,world!
```

## 调试
打开main函数所在文件

在VSCode中，按下F5键或点击调试面板的绿色播放按钮以启动调试器。
