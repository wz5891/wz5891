Aspose.Cells for Java 是一个强大的库，可以处理 Excel 文件的读写操作。要判断一个文件是 .xls 还是 .xlsx，可以通过文件扩展名来判断，或者尝试加载文件来判断。


```
import com.aspose.cells.Workbook;
 
public class AsposeExample {
    public static void main(String[] args) {
        String filePath = "path/to/your/file.xlsx"; // 替换为你的文件路径
        try {
            Workbook workbook = new Workbook(filePath);
            if (workbook.getFileFormat() == Workbook.FileFormat.XLSX) {
                System.out.println("文件是 .xlsx 格式");
            } else if (workbook.getFileFormat() == Workbook.FileFormat.XLS) {
                System.out.println("文件是 .xls 格式");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```