FTP上传和下载文件名称中文乱码问题

## 上传时文件名称乱码
FTP Server 协议里规定文件名编码是 ISO-8859-1，因此传输文件时需要转换编码
```
    //本地字符编码
    static String LOCAL_CHARSET = "UTF-8";
    // FTP协议里面，规定文件名编码为iso-8859-1
    static String SERVER_CHARSET = "ISO-8859-1";

    public Boolean storeFile(String filename, FileInputStream inputStream) throws IOException {
        // 开启服务器对UTF-8的支持，如果服务器支持就用UTF-8编码，否则就使用本地编码（GBK）.
        if (FTPReply.isPositiveCompletion(ftpClient.sendCommand("OPTS UTF8", "ON"))) {
            LOCAL_CHARSET = "UTF-8";
        }
        ftpClient.setControlEncoding(LOCAL_CHARSET);
        return ftpClient.storeFile(new String(filename.getBytes(LOCAL_CHARSET),
            SERVER_CHARSET),inputStream);
    }



```


## 下载时文件名称乱码
```
    public FTPFile[] listFiles() throws IOException {
        if (FTPReply.isPositiveCompletion(ftpClient.sendCommand("OPTS UTF8", "ON"))) {
            LOCAL_CHARSET = "UTF-8";
        }
        ftpClient.setControlEncoding(LOCAL_CHARSET);
        return ftpClient.listFiles();
    }
```


## 统一处理
也可以登陆的时候设置字符编码
```
public Boolean ftpLogin() throws IOException {
    ftpClient.setAutodetectUTF8(true);
    ftpClient.setCharset(CharsetUtil.UTF_8);
    ftpClient.setControlEncoding(CharsetUtil.UTF_8.name());
    //服务器地址和端口
    ftpClient.connect(HOSTNAME,PORT);

    //登录的用户名和密码
    Boolean isLogin = ftpClient.login(USERNAME, PWD);
    return isLogin;
}
```


https://blog.csdn.net/weixin_42254034/article/details/122007674