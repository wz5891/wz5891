利用maven将springboot的配置文件打到jar包外面


## 1. 为了部署时更加方便,将spring项目的配置文件和依赖jar包打到外面,方便修改.使用maven插件进行打包,打包完后压缩成zip,方便传输.首先配置项目maven,添加两个插件.


```
<build>
        <finalName>app</finalName>
        <plugins>
            <plugin>
            <!--必须使用apache的打包工具，因为spring的打包插件功能不够-->
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-jar-plugin</artifactId>
            <configuration>
                <archive>
                    <manifest>
                        <addClasspath>true</addClasspath>
                        <classpathPrefix>lib</classpathPrefix>
                        <!-- 程序启动入口 -->
                        <mainClass>com.lp.Application</mainClass>
                    </manifest>
                    <manifestEntries>
                        <Class-Path>./</Class-Path>
                    </manifestEntries>
                </archive>
                <excludes>
                    <!--设置不打进jar包的文件-->
                    <exclude>*.properties</exclude>
                    <exclude>*.xml</exclude>
                    <exclude>*.bat</exclude>
                </excludes>
            </configuration>
        </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <executions>
                    <execution>
                        <id>make-zip</id>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                        <configuration>
                            <!--配置文件中定义的id不包含到最终文件名里-->
                            <appendAssemblyId>false</appendAssemblyId>
                            <descriptors>
                                <!--配置文件所在的位置-->
                                <descriptor>src/main/resources/assembly.xml</descriptor>
                            </descriptors>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
</build>

```



## 2. 接下来在resources目录下新建一个assembly.xml,里面设置打包的具体策略
```
<?xml version="1.0" encoding="UTF-8"?>
<assembly
        xmlns="http://maven.apache.org/plugins/maven-assembly-plugin/assembly/1.1.2"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/plugins/maven-assembly-plugin/assembly/1.1.2 http://maven.apache.org/xsd/assembly-1.1.2.xsd">
    <id>package</id>
    <formats>
        <format>zip</format>
    </formats>
    <!-- 改为false不会出现两层相同的目录 -->
    <includeBaseDirectory>false</includeBaseDirectory>
    <fileSets>
        <fileSet>
            <directory>bin</directory>
            <outputDirectory>${file.separator}</outputDirectory>
        </fileSet>
        <fileSet>
            <directory>src/main/resources</directory>
            <outputDirectory>${file.separator}</outputDirectory>
            <!--设置要打进jar包的文件-->
            <excludes>
                <exclude>static/**</exclude>
                <exclude>templates/**</exclude>
                <exclude>application.yml</exclude>
            </excludes>
            <!--设置要打到jar包外面的文件-->
            <includes>
                <include>application-database.properties</include>
                <include>assembly.xml</include>
                <include>logback-spring.xml</include>
                <include>run.bat</include>
            </includes>
        </fileSet>
        <fileSet>
            <directory>${project.build.directory}</directory>
            <outputDirectory>${file.separator}</outputDirectory>
            <includes>
                <include>*.jar</include>
            </includes>
        </fileSet>
    </fileSets>
    <dependencySets>
        <dependencySet>
            <outputDirectory>lib</outputDirectory>
            <scope>runtime</scope>
            <excludes>
                <exclude>${groupId}:${artifactId}</exclude>
            </excludes>
        </dependencySet>
    </dependencySets>
</assembly>

```


## 3. run.bat中的内容是运行jar的命令,方便解压后双击运行.
```
@echo off
java -jar app.jar
pause
```