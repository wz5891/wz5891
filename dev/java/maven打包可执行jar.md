
## 打包可执行Jar
使用maven assembly-plugin打包可执行的jar，依赖的jar也会打进包里面

`pom.xml` 配置

```
<build>
        <plugins>
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>3.3.0</version>
                <configuration>
                    <appendAssemblyId>false</appendAssemblyId>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                    <archive>
                        <manifest>
                            <mainClass>xxxx.xxxx.RunJob</mainClass>
                        </manifest>
                    </archive>
                </configuration>
                <executions>
                    <execution>
                        <id>make-assembly</id>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
```

mainClass 填写启动的 main方法所在的类。

打包命令
```
mvn clean package
//跳过测试 
mvn clean package -Dmaven.test.skip=true
```



其它方案
maven-plugin-shade 插件提供了两个能力：

把整个项目（包含它的依赖）都打包到一个 "uber-jar" 中
shade - 即重命名某些依赖的包
https://www.cnblogs.com/lkxed/p/maven-plugin-shade.html




## 问题
maven-assembly打包中文乱码问题
通过 `mvn -v` 可以看到，window中mvn的默认编码是GBK。需要改为 UTF-8


在mvn.cmd中搜索-D，在带-D的行后添加一行："-Dfile.encoding=UTF-8" ^
```
"%JAVACMD%" ^
  %JVM_CONFIG_MAVEN_PROPS% ^
  %MAVEN_OPTS% ^
  %MAVEN_DEBUG_OPTS% ^
  -classpath %CLASSWORLDS_JAR% ^
  "-Dclassworlds.conf=%MAVEN_HOME%\bin\m2.conf" ^
  "-Dfile.encoding=UTF-8" ^
  "-Dmaven.home=%MAVEN_HOME%" ^
  "-Dlibrary.jansi.path=%MAVEN_HOME%\lib\jansi-native" ^
  "-Dmaven.multiModuleProjectDirectory=%MAVEN_PROJECTBASEDIR%" ^
  %CLASSWORLDS_LAUNCHER% %MAVEN_CMD_LINE_ARGS%
```
