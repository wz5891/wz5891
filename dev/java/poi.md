https://poi.apache.org/components/document/

Apache POI Project Components
The Apache POI project is the master project for developing pure Java ports of file formats based on Microsoft's OLE 2 Compound Document Format. OLE 2 Compound Document Format is used by Microsoft Office Documents, as well as by programs using MFC property sets to serialize their document objects.

Apache POI is also the master project for developing pure Java ports of file formats based on Office Open XML (ooxml). OOXML is part of an ECMA / ISO standardisation effort. 

Apache POI - HWPF and XWPF - Java API to Handle Microsoft Word Files

HWPF is the name of our port of the Microsoft Word 97(-2007) file format to pure Java. It also provides limited read only support for the older Word 6 and Word 95 file formats.

The partner to HWPF for the new Word 2007 .docx format is XWPF. Whilst HWPF and XWPF provide similar features, there is not a common interface across the two of them at this time.

Both HWPF and XWPF could be described as "moderately functional". For some use cases, especially around text extraction, support is very strong. For others, support may be limited or incomplete, and it may be necessary to dig down into low-level code. Error checking may be missing in places, so it may be possible to accidentally generate invalid files. Enhancements to fix such things are generally very well received!

As detailed in the Components Page, HWPF is contained within the poi-scratchpad-XXX.jar, while XWPF is in the poi-ooxml-XXX.jar. You will need to ensure you include the appropriate jars (and their dependencies!) in your classpath to use HWPF or XWPF.

Please note that in version 3.12, due to a bug, you might need to include poi-scratchpad-XXX.jar when using XWPF. This has been fixed again for the next release as there should not be such a dependency.