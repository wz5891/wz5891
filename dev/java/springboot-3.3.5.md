## 安装Jdk21
https://openjdk.org/?spm=5176.28103460.0.0.40f75d27gmH6AE

https://openjdk.org/projects/jdk/

https://jdk.java.net/archive/



## 关于idea报错：Cannot determine path to ‘tools.jar‘ library for 17 (E:\java-jdk\jdk17)

idea出现这个报错的原因是因为idea当前版本过低，不支持jdk17，也就是说jdk版本过高

https://blog.csdn.net/qq_64827052/article/details/140672186



IDEA 2023.2.2已支持JDK21
Java 21发布，IntelliJ IDEA 2023.2.2已完美支持。
想要开发Java 21代码的开发者可以升级了！


IntelliJ IDEA 2023.2.2 x64 安装破解
工具提取

破解步骤：

　　1：安装IDEA

　　2：解压jetbra.rar

　　3：执行 /jetbra/scripts/install-current-user.vbs

　　4：复制 /jetbra/idea激活码.txt

　　5：打开idea，选择activetion code，粘贴激活码，点击activate

## 2.7升级到3.3.6
https://blog.csdn.net/pumpkin84514/article/details/142371957



## spring.factories失效问题解决
从 Spring Boot 2.6 升级到Spring Boot 2.7后，自动配置注册有更改。

在Spring Boot 2.7还是可以兼容使用spring.factories
到了SpringBoot3 spring.factories就不兼容使用了。
tips: 在SpringBoot3中spring.factories使用org.springframework.boot.env.EnvironmentPostProcessor动态加载配置文件配置文件却还生效的。

使用spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports代替spring.factories中的org.springframework.boot.autoconfigure.EnableAutoConfiguration


原spring.factories文件
在resource/META-INF目录下新建spring目录，并添加org.springframework.boot.autoconfigure.AutoConfiguration.imports文件


## 升级spring-boot 3.3.X启动报错：Invalid value type for attribute ‘factoryBeanObjectType‘: java.lang.String解决
该问题是使用了 错误的mybatis-plus  starter版本导致的，

mybatis-plus-boot-starter

原依赖
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>${mybatis-plus.version}</version>
</dependency>

替换依赖为


<dependency>
     <groupId>com.baomidou</groupId>
     <artifactId>mybatis-plus-spring-boot3-starter</artifactId>
     <version>3.5.9</version>
</dependency>

升级Mybatis plus及mybatis
<dependencies>
        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-bom</artifactId>
            <version>3.5.9</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>


分页插件
    ```
    <dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-jsqlparser</artifactId>
</dependency>
    ```