
## CREATE TABLE AS SELECT
```
CREATE TABLE [新表名] AS SELECT [SELECT语句];
```


## CTE
```
WITH [CTE名] AS (
    [子查询]
)
SELECT [列名] FROM [CTE名];
```
