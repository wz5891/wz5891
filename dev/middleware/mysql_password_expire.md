mysql密码过期问题
密码自动过期是mysql 5.7.4引入的新功能。
由参数default_password_lifetime控制。从5.7.4到5.7.10，默认是360天。设置为0，即不开启密码过期设置。

取消某个用户密码过期设置：
```
alter user 'username'@'localhost' password expire never;
```

全局设置密码过期时间
1.在配置文件中设置
```
[mysqld]
default_password_lifetime=180
```
2.动态设置
```
set global default_password_lifetime = 180;
```

针对个别用户设置

1.设置用户密码过期时间
```
alter user 'username'@'localhost' password expire interval 90 day;
```
2.取消用户密码过期设置
```
alter user 'username'@'localhost' password expire never;
```
3.设置用户密码过期策略为默认策略(即全局默认设置)
```
alter user 'username'@'localhost' password expire default;
```


密码过期后处理
先使用过期账号登录Mysql，然后更新密码。（若不希望修改密码，也可以将新密码设置为与旧密码一致）
```
SET PASSWORD = PASSWORD('新密码');
```