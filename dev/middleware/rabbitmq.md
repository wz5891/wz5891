## 1. RabbitMQ消息大小限制，队列长度限制

1. 大小限制

RabbitMQ是基于amqp协议的实现，amqp0-9-1协议中规定了消息的大小是无限制的。原文：`To handle messages of any size without significant limit.`

在版本3.7中的源码，我们可以看到最大消息大小为2GiB
在版本3.8开始是512MiB

消息大小不要超过4MB

消息包大小由1K到10MB，当包大小达到4.5MB时，服务器的性能出现明显的异常，传输率尤其是每秒订阅消息的数量，出现波动，不稳定；同时有一部分订阅者的TCP连接出现断开的现象。可能是客户端底层或者RabbitMQ服务端在进行拆包，组包的时候，出现了明显的压力，而导致异常的发生。

超过4MB的消息，最好先进行分包

2. 队列长度限制

限制队列长度的方式有两种:
* 通过客户端在队列声明时使用队列的可选参数进行配置
* 通过服务器的policy命令设置

资料：https://blog.csdn.net/web18484626332/article/details/126487895




## RabbitMQ持久化消息

### RabbitMQ实现持久化消息需满足以下3个条件

1. delivery_mode=2
2. 使用durable=True声明exchange是持久化
3. 使用durable=True声明queue是持久化


### delivery_mode
delivery_mode=2指明message为持久的.

delivery_mode 投递消息模式

1. ram
2. disc

设置为disc后能从AMQP服务器崩溃中恢复消息--持久化

但效率比 ram:disc = 3:1

### durable
durable （默认false）

rabbitmq重启后queue和Exchange会被清除，包括数据。

注：

auto_delete: 当所有消费客户端连接断开后，自动删除队列。

exclusive: 仅创建者可以使用的私有队列，断开后自动删除。

RabbitMQ会自动删除这个队列，而不管这个队列是否被声明成持久性的（Durable =true)。

也就是说即使客户端程序将一个排他性的队列声明成了Durable的，只要调用了连接的Close方法或者客户端程序退出了，RabbitMQ都会删除这个队列。

注意这里是连接断开的时候，而不是通道断开。


### 添加用户
```
# create a user
rabbitmqctl add_user username password

# tag the user with "administrator" for full management UI and HTTP API access
rabbitmqctl set_user_tags username administrator

# First ".*" for configure permission on every entity
# Second ".*" for write permission on every entity
# Third ".*" for read permission on every entity
rabbitmqctl set_permissions -p "/" "username" ".*" ".*" ".*"

# Revokes permissions in a virtual host
rabbitmqctl clear_permissions -p "/" "username"
```


## RabbitMQ的TTL特性
https://blog.csdn.net/qq_42331185/article/details/131831383

TTL是RabbitMQ中消息或者队列的属性，注意是消息或者队列。表明一条消息或者该队列中的所有消息的最大存活时间，单位是：毫秒。
简单讲就是：设置了TTL的消息，在设置的时间内没有被消费的话，就会被认为是过期的，那么这条消息会被转到死信队列或者是丢弃。比如设置一条消息的TTL是5分钟，那么在5分钟内这条消息没有被消费掉，那么就属于过期的消息了，进而被死信或者丢弃

### 消息设置TTL
```
		// 创建一个交换机，不创建也可以，直接使用默认的
        channel.exchangeDeclare("direct1",BuiltinExchangeType.DIRECT);
        // 创建队列
        channel.queueDeclare(QUEUE_NAME, false, false, true, null);
		// 绑定队列和交换机
        channel.queueBind(QUEUE_NAME,"direct1",QUEUE_NAME);
		// 设置消息属性BasicProperties中的expiration
        AMQP.BasicProperties.Builder builder = new AMQP.BasicProperties.Builder();
        builder.expiration("10000");

        for (int i = 0; i < 10; i++) {
            channel.basicPublish("direct1", QUEUE_NAME, builder.build(), (msg + i).getBytes());
        }

```

查看消息属性，已经存在TTL了，十秒后观察队列如下，队列中的消息已被删除


### 队列属性设置TTL
```
	 	// 创建一个交换机，不创建也可以，直接使用默认的
        channel.exchangeDeclare("direct1", BuiltinExchangeType.DIRECT);

		// 创建队列并且声明属性x-message-ttl
        Map<String, Object> arguments = new HashMap<>();
        arguments.put("x-message-ttl", 10000);
        channel.queueDeclare(QUEUE_NAME, true, false, true, arguments);
		
		// 绑定交换机与队列
        channel.queueBind(QUEUE_NAME, "direct1", QUEUE_NAME);

        for (int i = 0; i < 10; i++) {
            channel.basicPublish("direct1", QUEUE_NAME, null, (msg + i).getBytes());
        }

```

从后台可以看到队列已经具备x-message-ttl属性了，这个属性对以后进入该队列的消息都会生效。但是我们再查看消息的属性的时候，消息本身就没有expiration这个属性了。


### 队列TTL
```
        channel.exchangeDeclare("direct1", BuiltinExchangeType.DIRECT);

		// 创建队列的时候，使用x-expires属性
        Map<String,Object>  arguments = new HashMap<>();
        arguments.put("x-expires",10000);
        channel.queueDeclare(QUEUE_NAME, true, false, true, arguments);

        channel.queueBind(QUEUE_NAME, "direct1", QUEUE_NAME);
        
        for (int i = 0; i < 10; i++) {
            channel.basicPublish("direct1", QUEUE_NAME, null, (msg + i).getBytes());
        }

```
通过后台观察队列的属性，发现存在x-expires ：10000的属性，说明设置成功，10秒后控制台如下，该队列被删除