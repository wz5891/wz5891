https://arthas.aliyun.com/

```
java -jar arthas-boot.jar
```


https://arthas.aliyun.com/doc/quick-start.html#_6-watch

使用trace优化性能
```
[arthas@63020]$ trace *SampleBaseinfoService addOrUpdate
Press Q or Ctrl+C to abort.
Affect(class count: 2 , method count: 2) cost in 430 ms, listenerId: 2
`---ts=2024-08-13 14:31:59;thread_name=http-nio-9200-exec-5;id=98;is_daemon=true;priority=5;TCCL=org.springframework.boot.web.embedded.tomcat.TomcatEmbeddedWebappClassLoader@73b8ab2c
    `---[13963.1153ms] topsoft.lims.core.moudule.samplemanage.service.SampleBaseinfoService$$EnhancerBySpringCGLIB$$13647d66:addOrUpdate()
        `---[100.00% 13963.052599ms ] org.springframework.cglib.proxy.MethodInterceptor:intercept()
            `---[100.00% 13962.9779ms ] topsoft.lims.core.moudule.samplemanage.service.SampleBaseinfoService:addOrUpdate()
                +---[0.01% 0.7617ms ] topsoft.product.commonuseage.utils.BeanCompareUtils:beanTranslate() #1113
                +---[0.00% 0.0143ms ] topsoft.lims.core.moudule.samplemanage.model.request.UpdateSampleBaseinfoRequest:getSampleId() #1114
                +---[0.00% 0.01ms ] topsoft.lims.core.moudule.samplemanage.entity.SampleBaseinfoEntity:getOrderId() #1116
                +---[0.06% 8.296ms ] topsoft.lims.core.moudule.common.service.OrderBaseinfoService:queryById() #1116
                +---[0.00% 0.0116ms ] topsoft.lims.core.moudule.samplemanage.entity.SampleBaseinfoEntity:getSampleType() #1119
                +---[0.00% 0.0052ms ] topsoft.lims.core.moudule.samplemanage.entity.SampleBaseinfoEntity:getSampleType() #1120
                +---[0.00% 0.010599ms ] topsoft.lims.core.moudule.samplemanage.entity.SampleBaseinfoEntity:setCheckSampleBarCode() #1121
                +---[0.00% 0.027ms ] topsoft.lims.core.moudule.samplemanage.service.SampleBaseinfoService:lambdaQuery() #1149
                +---[0.00% 0.0621ms ] com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper:eq() #1150
                +---[0.10% 13.973501ms ] com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper:count() #1151
                +---[0.06% 8.7382ms ] topsoft.lims.core.moudule.samplemanage.dao.SampleBaseinfoMapper:updateDateNullById() #1155
                +---[0.00% 0.0135ms ] topsoft.lims.core.moudule.samplemanage.entity.SampleBaseinfoEntity:getBarCode() #1157
                +---[0.00% 0.005801ms ] topsoft.lims.core.moudule.samplemanage.entity.SampleBaseinfoEntity:getBarCode() #1158
                +---[0.44% 61.025801ms ] topsoft.lims.core.moudule.SysOperateLogServiceAdapter:addLog() #1159
                +---[0.00% 0.0073ms ] topsoft.lims.core.moudule.samplemanage.entity.SampleBaseinfoEntity:getOrderId() #1162
                +---[1.04% 145.3536ms ] topsoft.lims.core.moudule.common.service.OrderBaseinfoService:handleSampleNum() #1162
                +---[0.00% 0.0155ms ] topsoft.lims.core.moudule.samplemanage.model.request.UpdateSampleBaseinfoRequest:getOrderTestprojectDtoList() #1166
                +---[0.00% 0.0499ms ] topsoft.lims.core.moudule.common.service.OrderTestprojectService:lambdaQuery() #1167
                +---[0.00% 0.034801ms ] com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper:eq() #1168
                +---[0.34% 47.3146ms ] com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper:list() #1169
                +---[0.00% 0.0081ms ] org.springframework.util.CollectionUtils:isEmpty() #1173
                +---[0.00% 0.633199ms ] topsoft.lims.core.moudule.samplemanage.service.SampleBaseinfoService:getBasedataProjectIdByUseChoose() #1182
                +---[0.00% 0.012601ms ] topsoft.lims.core.moudule.common.entity.OrderBaseinfoEntity:getLaboratoryId() #1186
                +---[1.78% 248.5111ms ] topsoft.lims.ability.chemistry.service.BasedataProjectBaseinfoService:queryProjectWithMethodAndTargetObjectDtoByProjectId() #1186
                +---[0.00% 0.027801ms ] topsoft.lims.ability.chemistry.service.BasedataTargetobjectService:queryByIdList() #1187
                +---[0.28% 38.6548ms ] topsoft.lims.core.moudule.common.service.OrderTestprojectService:arrangeProjectOneSample() #1188
                +---[0.03% 3.6322ms ] topsoft.lims.core.moudule.common.service.OrderTestprojectService:queryByBySampleidList() #1191
                +---[0.00% 0.123401ms ] topsoft.lims.core.moudule.common.service.OrderTestprojectService:operateLogBatchAddOrDel() #1193
                +---[0.00% 0.0063ms ] topsoft.lims.core.moudule.samplemanage.entity.SampleBaseinfoEntity:getOrderId() #1197
                +---[33.05% 4614.3425ms ] topsoft.lims.core.moudule.samplemanage.service.OrderPriceSumService:calAllPriceByOrderIdList() #1197
                +---[0.00% 0.006699ms ] topsoft.lims.core.moudule.samplemanage.entity.SampleBaseinfoEntity:getSampleId() #1199
                +---[0.76% 105.95ms ] topsoft.lims.core.moudule.samplemanage.service.SampleBaseinfoService:updatePreparationByOrderTestProject() #1199
                +---[0.00% 0.0063ms ] topsoft.lims.core.moudule.samplemanage.entity.SampleBaseinfoEntity:getSampleId() #1201
                +---[0.26% 35.813899ms ] topsoft.lims.core.moudule.common.service.OrderTestprojectService:handleSamplePreProject() #1201
                +---[4.53% 632.6015ms ] topsoft.lims.core.moudule.samplemanage.service.SampleBaseinfoService:handleSampleTestPlace() #1203
                +---[0.68% 94.894ms ] topsoft.lims.core.moudule.samplemanage.service.SampleBaseinfoService:updateSampleCheckStatus() #1205
                `---[56.56% 7896.765699ms ] topsoft.lims.core.moudule.testManage.service.aop.TestResultCreateWithNoFinishService:create() #1207
```