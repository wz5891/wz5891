解析pdf文件的web服务


环境 python3

代码
```
# coding=utf-8
from http.server import BaseHTTPRequestHandler
import cgi
import pdfplumber
import json
import io


class PostHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        path = str(self.path)     #获取请求的url
        if path=="/parser":
            form = cgi.FieldStorage(
                fp=self.rfile,
                headers=self.headers,
                environ={'REQUEST_METHOD': 'POST',
                        'CONTENT_TYPE': self.headers['Content-Type']
                        }
            )

            file = form['file']
            file_content = io.BytesIO(file.value)

            pdf = pdfplumber.open(file_content)
            length = len(pdf.pages)

            data = []

            # 循环处理页面
            for i in range(0, length):
                page = pdf.pages[i]

                tables = page.extract_tables()
                content = page.extract_text()

                pdf_page = {"index": i+1, "content": content, "tables": tables}
                data.append(pdf_page)

            json_text = json.dumps(data, ensure_ascii=False)

            # 返回响应
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(json_text.encode())
            self.wfile.flush()
        return

if __name__ == '__main__':
    from http.server import HTTPServer
    sever = HTTPServer(('', 8080), PostHandler)
    print('Listening : port = %d' % 8080)
    print('HttpServer Starting , use <Ctrl-C> to stop')
    sever.serve_forever()

```

制作docker镜像

Dockerfile
```
FROM python:3.7

RUN useradd -m -d /home/pdfparse -u 2023 -U pdfparse

WORKDIR /home/pdfparse

USER pdfparse

RUN pip install pdfplumber

COPY ./pdf_parse_server.py /home/pdfparse/pdf_parse_server.py

CMD ["python", "/home/pdfparse/pdf_parse_server.py"]

EXPOSE 8080
```

命令
```
docker build -t pdfparse:v1 .

docker run -p 8080:8080 --name pdfparse -d pdfparse:v1

docker save pdfparse:v1 | gzip > pdfparse.tar.gz
```

docker-compose.yml
```
version: "3"
services:
  pdfparse:
    image: pdfparse:v1
    container_name: pdfparse
    ports:
      - 8080:8080
    environment:
      - TZ=Asia/Shanghai
    restart: always
```


参考：
Python 教程
https://www.runoob.com/python/python-lists.html

pdfplumber 官方文档
https://pypi.org/project/pdfplumber/#extracting-form-values

pdfplumber使用实例
https://blog.csdn.net/zohan134/article/details/120725459