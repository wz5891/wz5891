# 依赖注入

官方文档

英文：https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#beans-dependencies

中文：https://springdoc.cn/spring/core.html#beans-annotation-config


## @Autowired
默认按类型匹配的方式，当Spring发现@Autowired注解时，就自动在代码上下文中找到其匹配的Bean，当仅有一个匹配的Bean时，Spring将其注入@Autowire标注的变量中。

## @Resource
用来指定Bean名称注入，javaEE的，Spring也支持，建议使用整个，全球通用的

(1) 后面没有任何内容，默认通过name属性去匹配bean，找不到再按type去匹配 
(2) 指定了name或者type则根据指定的类型去匹配bean 
(3) 指定了name和type则根据指定的name和type去匹配bean，任何一个不匹配都将报错


## @Qualifier
@Qualifier 就是 autowire=byName, @Qualifier通常和@Autowired一起使用，一般在两个或者多个bean是相同的类型时，spring注入会出现混乱，就需要使用@Qualifier(“xxBean”) 来指定Bean的名称。
