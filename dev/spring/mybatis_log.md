Mybatis的log-impl需要配置正确的实现类

```
# 这个配置会将执行的sql打印出来，在开发或测试的时候可以用
mybatis-plus:
  configuration:
    #log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
	# 这个配置会将执行的sql打印出来，这个可以存放在文件中 StdOutImpl的是只能打印到控制台
	log-impl: org.apache.ibatis.logging.slf4j.Slf4jImpl
```



 levle 需要是DEBUG等级
 ```
<logger name="com.xxx.mapper" level="DEBUG">
		<appender-ref ref="stdout"/>
		<appender-ref ref="mysql_log" />
</logger>
 ```