Alibaba Cloud Linux 3安装docker
系统内核版本查看：
```
[root@zzgxl testDocker]# uname -r
5.10.134-13.al8.x86_64
```
系统版本获取（Alibaba Cloud Linux 3 全面兼容CentOS 8）：
```
[root@zzgxl testDocker]# cat /etc/os-release
NAME="Alibaba Cloud Linux"
VERSION="3 (Soaring Falcon)"
ID="alinux"
ID_LIKE="rhel fedora centos anolis"
VERSION_ID="3"
PLATFORM_ID="platform:al8"
PRETTY_NAME="Alibaba Cloud Linux 3 (Soaring Falcon)"
ANSI_COLOR="0;31"
HOME_URL="https://www.aliyun.com/"
```
安装dnf，dnf是新一代的rpm软件包管理器：
```
yum -y install dnf
```
安装社区版Docker（docker-ce）：

运行以下命令，添加docker-ce的dnf源：
```
dnf config-manager --add-repo=https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```
运行以下命令，安装Alibaba Cloud Linux 3专用的dnf源兼容插件：
```
dnf -y install dnf-plugin-releasever-adapter --repo alinux3-plus
```
如果您不使用Alibaba Cloud Linux 3专用的dnf源兼容插件，将无法正常安装docker-ce。

运行以下命令，安装docker-ce：
```
dnf -y install docker-ce --nobest
```
运行以下命令，查看docker-ce是否成功安装：
```
dnf list docker-ce
```
运行以下命令，启动Docker服务。

```
systemctl start docker
```
运行以下命令，查看Docker服务的运行状态。
```
systemctl status docker
```