离线安装 Docker Rootless 安装篇

离线安装的文件包如何准备，参见[离线安装 Docker Rootless 准备篇](/docker/centos_install_rootless_docker_offline_prepare.md)

[百度云下载链接](https://pan.baidu.com/s/1-iCZdf5jR03HsASh5QfIvA?pwd=5h72)


## 安装newuidmap和newgidmap
将`shadow-utils46-newxidmap-4.6-4.el7.x86_64.rpm`上传到 `/root/softs`目录

安装
```bash
cd /root/softs
rpm -Uvh *.rpm
```


执行
```bash
cat <<EOT >>/etc/sysctl.conf
user.max_user_namespaces = 28633
EOT

sysctl --system
```

UID/GID 映射配置
```bash
echo "rootless:100000:65536" | tee /etc/subuid
echo "rootless:100000:65536" | tee /etc/subgid
```

## 创建用户
```bash
useradd rootless
echo 123456 | passwd rootless --stdin
```

## 安装 Docker Rootless

先切换用户
```bash
su rootless
```
将安装包`docker-24.0.7.tgz`，`docker-rootless-extras-24.0.7.tgz`上传到目录 `/home/rootless/softs`

安装
```bash
mkdir /home/rootless/bin
cd /home/rootless/bin
tar zxvf /home/rootless/softs/docker-24.0.7.tgz --strip-components=1
tar zxvf /home/rootless/softs/docker-rootless-extras-24.0.7.tgz --strip-components=1

export PATH=/home/rootless/bin:$PATH
./dockerd-rootless-setuptool.sh install

```


将以下内容添加到 `~/.bashrc` 文件中
```bash
export XDG_RUNTIME_DIR=/home/rootless/.docker/run
export PATH=/home/rootless/bin:$PATH
export DOCKER_HOST=unix:///home/rootless/.docker/run/docker.sock
```

运行以下命令使环境变量生效
```
source ~/.bashrc
```

## 启动Docker
创建用于启动的命令文件

```bash
cd /home/rootless
cat > docker_start.sh <<EOF
#!/bin/bash
nohup dockerd-rootless.sh &
EOF

chmod +x docker_start.sh
```

启动 Docker 守护进程
```
cd /home/rootless
./docker_start.sh
```

## 测试Docker
运行容器
```bash
docker run hello-world
```



## 限制

IPAddress shown in docker inspect is namespaced inside RootlessKit's network namespace. This means the IP address is not reachable from the host without nsenter-ing into the network namespace.
Host network (docker run --net=host) is also namespaced inside RootlessKit.


在使用RootlessKit的情况下，Docker inspect命令返回的IP地址是RootlessKit的网络命名空间中进行了隔离的。这意味着从宿主机上无法直接访问到这个IP地址，需要通过nsenter命令进入到网络命名空间中。

Host网络（docker run --net=host）也是RootlessKit中进行了隔离。


由于网络不能用，rootless实际也就没有什么用处！！


---

最终方案：还是采用 `userns-remap`方式，外加给非root账号docker执行权限。


[docker权限设置：让非root用户可以操作docker](docker/no_root_user_execute_docker_cmd.md)

[Docker中使用userns-remap做用户命名空间](docker/use_userns_remap.md)