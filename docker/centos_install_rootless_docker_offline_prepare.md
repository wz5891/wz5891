离线安装 Docker Rootless 准备篇


根据 [在线安装 Docker Rootless](/docker/centos_install_rootless_docker_online.md)的步骤，准备依赖的包


## 准备newuidmap和newgidmap 工具安装包

### 安装包下载工具 yumdownloader
```bash
yum install yum-utils -y
```

## 下载newuidmap和newgidmap包
```bash
curl -o /etc/yum.repos.d/vbatts-shadow-utils-newxidmap-epel-7.repo https://copr.fedorainfracloud.org/coprs/vbatts/shadow-utils-newxidmap/repo/epel-7/vbatts-shadow-utils-newxidmap-epel-7.repo

yumdownloader shadow-utils46-newxidmap --resolve --destdir=/home/root/shadow-utils46-newxidmap
```

## 准备docker rootless安装包

根据脚本 https://get.docker.com/rootless 下载对应的文件
```
wget https://download.docker.com/linux/static/stable/$(uname -m)/docker-rootless-extras-24.0.7.tgz
wget https://download.docker.com/linux/static/stable/$(uname -m)/docker-24.0.7.tgz
```