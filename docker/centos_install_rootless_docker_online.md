在线安装 Docker Rootless

Docker Rootless 基本概念

Rootless 模式允许以非 root 用户身份运行 Docker 守护进程（dockerd）和容器，以缓解 Docker 守护进程和容器运行时中潜在的漏洞。Rootless 模式是在 Docker v19.03 版本作为实验性功能引入的，在 Docker v20.10 版本 GA。

Rootless 模式目前对 Cgroups 资源控制，Apparmor 安全配置，Overlay 网络，存储驱动等还有一定的限制，暂时还不能完全取代 “Rootful” Docker。关于 Docker Rootless 的详细信息参见 Docker 官方文档 

[ Run the Docker daemon as a non-root user (Rootless mode)] (https://docs.docker.com/engine/security/rootless/#limiting-resources)

Rootless 模式利用 user namespaces 将容器中的 root 用户和 Docker 守护进程（dockerd）用户映射到宿主机的非特权用户范围内。Docker 此前已经提供了 --userns-remap 标志支持了相关能力，提升了容器的安全隔离性。Rootless 模式在此之上，让 Docker 守护进程也运行在重映射的用户名空间中。

![Alt text](image.png)

# 实践验证
环境准备
本文使用 Centos 7.9 操作系统的虚拟机进行实验。
```bash
[root@demo ~]# cat /etc/redhat-release
CentOS Linux release 7.9.2009 (Core)
```

## 用户准备
### 创建用户
```bash
useradd rootless
echo 123456 | passwd rootless --stdin
```
### 安装依赖
Rootless 模式可以在没有 root 权限的情况下运行 Docker 守护进程和容器， 但是需要安装 newuidmap和newgidmap 工具，以便在用户命名空间下创建从属(subordinate)用户和组的映射(remapping)。通过以下命令安装 newuidmap 和 newgidmap 工具。
```bash
cat <<EOF | sudo sh -x
curl -o /etc/yum.repos.d/vbatts-shadow-utils-newxidmap-epel-7.repo https://copr.fedorainfracloud.org/coprs/vbatts/shadow-utils-newxidmap/repo/epel-7/vbatts-shadow-utils-newxidmap-epel-7.repo
yum install -y shadow-utils46-newxidmap
cat <<EOT >/etc/sysctl.conf
user.max_user_namespaces = 28633
EOT
sysctl --system
EOF
```
### UID/GID 映射配置
从属用户和组的映射由两个配置文件来控制，分别是 /etc/subuid 和 /etc/subgid。使用以下命令为 rootless 用户设置 65536 个从属用户和组的映射。
```bash
echo "rootless:100000:65536" | tee /etc/subuid
echo "rootless:100000:65536" | tee /etc/subgid
```
对于 subuid，这一行记录的含义为：用户 rootless，在当前的 user namespace 中具有 65536 个从属用户，用户 ID 为 100000-165535，在一个子 user namespace 中，这些从属用户被映射成 ID 为 0-65535 的用户。subgid 的含义和 subuid 相同。

比如说用户 rootless 在宿主机上只是一个具有普通权限的用户。我们可以把他的一个从属 ID (比如 100000 )分配给容器所属的 user namespace，并把 ID 100000 映射到该 user namespace 中的 uid 0。此时即便容器中的进程具有 root 权限，但也仅仅是在容器所在的 user namespace 中，一旦到了宿主机中，顶多也就有 rootless 用户的权限而已。

## 安装 Rootless Docker
切换到 rootless 用户。
```
su - rootless
```
执行以下命令安装 Rootless Docker。
```
curl -fsSL https://get.docker.com/rootless | sh
```
安装成功后显示如下内容。

```bash
[rootless@localhost ~]$ curl -fsSL https://get.docker.com/rootless | sh
# Installing stable version 24.0.7
# Executing docker rootless install script, commit: 03fb81c
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 66.5M  100 66.5M    0     0  1704k      0  0:00:40  0:00:40 --:--:-- 1398k
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 19.4M  100 19.4M    0     0   689k      0  0:00:28  0:00:28 --:--:--  624k
+ PATH=/home/rootless/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/rootless/.local/bin:/home/rootless/bin
+ /home/rootless/bin/dockerd-rootless-setuptool.sh install
[INFO] systemd not detected, dockerd-rootless.sh needs to be started manually:

PATH=/home/rootless/bin:/sbin:/usr/sbin:$PATH dockerd-rootless.sh 

[INFO] Creating CLI context "rootless"
Successfully created context "rootless"
[INFO] Using CLI context "rootless"
Current context is now "rootless"

[INFO] Make sure the following environment variable(s) are set (or add them to ~/.bashrc):
# WARNING: systemd not found. You have to remove XDG_RUNTIME_DIR manually on every logout.
export XDG_RUNTIME_DIR=/home/rootless/.docker/run
export PATH=/home/rootless/bin:$PATH

[INFO] Some applications may require the following environment variable too:
export DOCKER_HOST=unix:///home/rootless/.docker/run/docker.sock
```

将以下内容添加到 `~/.bashrc` 文件中，添加完以后使用 `source ~/.bashrc` 命令使环境变量生效。
```bash
export XDG_RUNTIME_DIR=/home/rootless/.docker/run
export PATH=/home/rootless/bin:$PATH
export DOCKER_HOST=unix:///home/rootless/.docker/run/docker.sock
```
启动 Docker 守护进程
使用以下命令启动 Docker 守护进程。

```bash
nohup dockerd-rootless.sh &
```
运行容器
```bash
docker run hello-world
```


docker run -d -p 8080:80 nginx


[原文](https://blog.csdn.net/easylife206/article/details/132632629)