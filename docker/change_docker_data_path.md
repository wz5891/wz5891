调整Docker存储目录

## 停止docker

```
systemctl stop docker
```

如果出现如下警告
```
Warning: Stopping docker.service, but it can still be activated by: docker.socket
```

原因是：Docker默认开启自动唤醒机制，即docker默认在关闭状态下被访问会自动唤醒Docker。


解决办法是：
```
#停用Docker自动唤醒机制
systemctl stop docker.socket

# 停用docker
systemctl stop docker
```


想要开启自动唤醒机制执行
```
systemctl start docker.socket
```

## 修改docker存储目录

docker默认的存储路径是 `/var/lib/docker`


```
#停止 docker应用
systemctl stop  docker
 
#将docker 数据库目录以及其数据目录的所有数据迁移到新目录
cp -rp /var/lib/docker /data/docker_data
```


新建docker配置文件
```
touch /etc/docker/daemon.json
```
 
编辑 配置文件添加 "data-root"

```
vim /etc/docker/daemon.json

{
"data-root": "/data/docker_data"
}
```


重新加载配置文件
```
systemctl daemon-reload
```

重启docker应用
```
systemctl restart docker
```

检查，运行
```
docker info
```
查看  `Docker Root Dir` 是否指向新目录




## docker控制台日志
应用 log 配置文件，怎么控制输出到docker控制台的日志级别
