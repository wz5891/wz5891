新建docker配置文件
```
touch /etc/docker/daemon.json
```
 
编辑 配置文件添加 "registry-mirrors"

```
vim /etc/docker/daemon.json

{
    "registry-mirrors": [
        "https://docker.m.daocloud.io",
        "https://noohub.ru",
        "https://huecker.io",
        "https://dockerhub.timeweb.cloud"
  ]
}
```



重新加载配置文件
```
systemctl daemon-reload
```

重启docker应用
```
systemctl restart docker
```
