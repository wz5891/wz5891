Docker0网卡固定mac地址

https://blog.51cto.com/u_16175437/6796293



https://blog.csdn.net/2301_78526383/article/details/131563688

https://blog.csdn.net/qq_44797987/article/details/131190204


https://cloud.tencent.com/developer/article/2289786


```
# 查看docker网络信息
docker network ls

# 删除未使用的网络，主要是把发生网络冲突的那个网段给清掉
docker network prune
```


修改docker配置文件
vi /etc/docker/daemon.json
1
在配置文件中添加以下内容，其中default-address-pools的base表示CIDR地址，size表示docker创建的网络的掩码长度，CIDR的掩码长度应该小于size，否则docker将会出现网络失败。这里使用192.168网段地址，其中CIDR为16为掩码，划分的网络子网掩码24位，理论可以划分出2(32-16)-(32-24)=28=256个子网。

{
    "registry-mirrors": [
        "https://docker.mirrors.ustc.edu.cn/"
    ],
    "debug": true,
    "default-address-pools": [
        {
            "base": "192.168.1.1/16",
            "size": 24
        }
    ]
}

4）重新启动docker

# 请再次确认已将docker之前创建的网络删除
# 启动docker
systemctl start docker

设置bip无效
在daemon配置文件中单纯配置bip只对docker启动的容器有效，但是对docker-compose启动的容器无效。

Docker engine is started with --bip argument, but it is ignored by docker-compose, which set up an additional bridge interface in addition to the docker0 interface. It is named br-f4a912f7750b and has the typical Docker 172.17.x.x format, which happens to conflict with the network I’m currently using.
