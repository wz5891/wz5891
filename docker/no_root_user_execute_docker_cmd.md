docker权限设置：让非root用户可以操作docker

## 创建用户
```bash
#创建用户appdeploy
adduser  appdeploy
#修改用户appdeploy的密码
passwd  appdeploy
```


## 为用户appdeploy添加docker执行权限

### 方式一
```bash
#创建docker组
sudo groupadd docker
#添加appdeploy进入docker组
sudo gpasswd -a appdeploy docker
#重启docker服务
sudo systemctl restart docker
#接下来就可以使用你添加的用户（appdeploy）进行使用docker命令了
```

### 方式二
```bash
#修改文件权限
sudo chmod 666 /var/run/docker.sock
```


方式二每次docker重启后，都需要重新修改权限。因此推荐方式一。