1. 停止所有容器
docker stop $(docker ps -q)

2. 移除所有容器
docker rm $(docker ps -aq)

3. 查看容器网络
docker network ls

4. 删除容器网络
docker network rm 网络名称
