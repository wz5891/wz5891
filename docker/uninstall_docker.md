如何卸载Docker


1. 停止Docker服务
```bash
systemctl stop docker
```

2. 卸载Docker软件包
```bash
yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine

```

3. 查看是否有漏掉的docker依赖
```bash
yum list installed | grep docker

```

4. 删除 Docker 数据目录
```bash
sudo rm -rf /var/lib/docker
```