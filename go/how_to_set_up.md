## 下载地址
https://golang.google.cn/dl/

## 安装
下载安装包安装

## 配置GOPROXY
Go1.12版本之后，开始使用go mod模式来管理依赖环境了

由于国内访问不到默认的GOPROXY配置链接，所以我们需要换一个PROXY，推荐使用https://goproxy.io或https://goproxy.cn。

可以执行下面的命令修改GOPROXY：
```
go env -w GOPROXY=https://goproxy.cn,direct
```


## GO MOD设置
要启用go module支持首先要设置环境变量GO111MODULE，通过它可以开启或关闭模块支持，它有三个可选值：off、on、auto，默认值是auto。

GO111MODULE=off禁用模块支持，编译时会从GOPATH和vendor文件夹中查找包。
GO111MODULE=on启用模块支持，编译时会忽略GOPATH和vendor文件夹，只根据 go.mod下载依赖。
GO111MODULE=auto，当项目在$GOPATH/src外且项目根目录有go.mod文件时，开启模块支持。
通过以下命令修改
```
go env -w GO111MODULE=on
```
使用go module模式新建项目时，我们需要通过go mod init 项目名命令对项目进行初始化，该命令会在项目根目录下生成go.mod文件。例如，我们使用hello作为我们第一个Go项目的名称，执行如下命令。
```
go mod init hello
```
运行之前可以使用​go mod tidy​命令将所需依赖添加到go.mod文件中，并且能去掉项目中不需要的依赖



## 安装测试
创建工作目录 E:\>go_workspace

文件名: test.go，代码如下：
```
package main

import "fmt"

func main() {
   fmt.Println("Hello, World!")
}
```

使用 go 命令执行以上代码输出结果如下：
```
E:\>go_workspace>go run test.go

Hello, World!
```

## Vscode
安装 Go 插件

之后，编辑go 代码时，右下角提示要安装的插件全部安装。

