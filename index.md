---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "IT杂记"
  text: "IT杂记，记录学习"
  tagline: 工作学习笔记，分享IT技术
  actions:
    - theme: brand
      text: Java读取命令行参数
      link: dev/java/读取命令行参数.md
    - theme: alt
      text: Linux维护
      link: deploy/linux.md

features:
  - title: Feature A
    details: ...
  - title: Feature B
    details: ...
  - title: Feature C
    details: ...
---

