# 快速批量删除JENKINS构建清理磁盘空间并按参数保留最近构建


使用Jenkins脚本命令行批量修改任务设置清理磁盘空间

登录Jenkins -> Manage Jenkins -> Script Console(脚本命令行)执行如下命令


```groovy
import hudson.tasks.LogRotator
Jenkins.instance.allItems(Job).each { job ->
  println "$job.builds.number $job.name"
  if ( job.isBuildable() && job.supportsLogRotator()) {
    // 注释if所有任务统一设置策略，去掉注释后只更改没有配置策略的任务
    //if ( job.getProperty(BuildDiscarderProperty) == null) {
      job.setLogRotator(new LogRotator (60, 5, 30, 3))
    //}
      job.logRotate() //立马执行Rotate策略
    println "$job.builds.number $job.name 磁盘回收已处理"
  } else { println "$job.name 未修改，已跳过" }
}
return;
```


解释说明
LogRotator构造参数分别为：

daysToKeep: 构建记录将保存的天数

numToKeep: 最多此数目的构建记录将被保存

artifactDaysToKeep: 比此早的发布包将被删除，但构建的日志、操作历史、报告等将被保留

artifactNumToKeep: 最多此数目的构建将保留他们的发布包


如果是新建的Declarative Pipeline可以添加
```bash
options {
    buildDiscarder logRotator(artifactDaysToKeepStr: '30', artifactNumToKeepStr: '3', daysToKeepStr: '60', numToKeepStr: '30')
}
```