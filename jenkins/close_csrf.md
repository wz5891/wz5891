docker 安装jenkins 跨域csrf
```
docker run -d \
    -p 8888:8080 \
    -p 50000:50000 \
    --privileged=true\
    -v /opt/jenkins_home:/var/jenkins_home \
    -v /etc/localtime:/etc/localtime \
    --restart=always \
    --name=jk \
    -e JAVA_OPTS=-Dhudson.security.csrf.GlobalCrumbIssuerConfiguration.DISABLE_CSRF_PROTECTION=true \
    jenkins/jenkins:latest-jdk8
```