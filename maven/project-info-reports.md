

Apache Maven 项目信息报告插件
Maven Project Info Reports 插件用于生成有关项目的报告信息。

目标概述
项目信息报告插件具有以下目标：

project-info-reports:ci-management用于生成项目持续集成管理报告。
project-info-reports:dependencies用于生成项目依赖关系报告。
project-info-reports:dependency-convergence用于为（reactor）构建生成项目依赖收敛报告。
project-info-reports:dependency-info用于生成要添加到构建工具的代码片段。
project-info-reports:dependency-management用于生成项目依赖管理报告。
project-info-reports:distribution-management用于生成项目分发管理报告。
project-info-reports:help用于显示项目信息报告插件的帮助信息。
project-info-reports:index用于生成项目索引页面。
project-info-reports:issue-management用于生成项目问题管理报告。
project-info-reports:licenses用于生成项目许可证报告。
project-info-reports:mailing-lists用于生成项目邮件列表报告。
project-info-reports:modules用于生成项目模块报告。
project-info-reports:plugin-management用于生成项目插件管理报告。
project-info-reports:plugins用于生成项目插件报告。
project-info-reports:team用于生成项目团队报告。
project-info-reports:scm用于生成项目源代码管理报告。
project-info-reports:summary用于生成项目总结报告。


生成项目依赖关系报告
```
mvn project-info-reports:dependencies
```


```
<plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <version>3.2.2</version>
      </plugin>
```