每个 BSON 类型都同时具有整数和字符串标识符，如下表所列：

| 类型       | 数值 | 别名         | 注意 |
| :--------- | :--- | :----------- | :--- |
| 双精度     | 1    | "double"     |      |
| 字符串     | 2    | "string"     |      |
| 对象       | 3    | "object"     |      |
| 阵列       | 4    | "array"      |      |
| 二进制数据 | 5    | "binData"    |      |
| ObjectId   | 7    | "objectId"   |      |
| 布尔       | 8    | "bool"       |      |
| Date       | 9    | "date"       |      |
| null       | 10   | "null"       |      |
| 正则表达式 | 11   | "regex"      |      |
| JavaScript | 13   | "javascript" |      |
| 32 位整数  | 16   | "int"        |      |
| 时间戳     | 17   | "timestamp"  |      |
| 64 位整型  | 18   | "long"       |      |
| Decimal128 | 19   | "decimal"    |      |
| 最小键     | -1   | "minKey"     |      |
| Max key    | 127  | "maxKey"     |      |