Mongodb数据库的导出和导入总结


需求:
当我们进行数据库迁移的时候，自然而然就会用到数据库的导入和导出，这里讲解下Mongodb的数据库的 导出和导入。

解决方案：
1. 数据导出：mongoexport
1.1.概念：
Mongdb中的mongoexport 工具可以将collection 导出成JSON格式或者CSV格式的文件。可以通过参数指定导出的数据项，也可以根据指定的条件导出数据。

1.2. 语法：
```
mongoexport -d dbname -c collectionname -o fiepath --type json/csv -f field
```
参数说明：
-d: 数据库名
-c: collection名
-o: 输出文件路径
--type: 输出的格式。默认为json
-f: 输出的字段，如果type为CSV，则需要加上 -f "字段名"

1.3. 实例：
```
mongoexport -d wcx2020 -c articles -o D:\mongodbBackup\mongosql.json --type json
```

2. 数据导入：mongoimport
2.1. 语法：
```
mongoexport -d dbname -c collectionname --file filepath -type json/csv -f field
```
参数说明：
-d: 数据库名
-c: collection名
--file: 导入文件路径
--type: 输出的格式。默认为json
-f: 导入的字段名

2.2. 实例：
```
mongoimport -d wcx2020 -c articles -file D:\mongodbBackup\mongosql.json --type json
```

3. 数据备份(导出所有数据)
3.1. 语法：
```
mongodump -h connection -d dbname -o filepath
```
参数说明：

-h: 连接IP和端口号（默认127.0.0.1:27017）

-d: 数据库名

-o: 导出文件路径

3.2. 实例：
```
mongodump -h 127.0.0.1 -d wcx2020 -o D:\mongodbBackup\
```

4. 数据恢复
4.1语法：
```
mongorestore -h connection -d dbname --dir filepath
```
参数说明：
-h: 连接IP和端口号（默认127.0.0.1:27017）
-d: 数据库名
--dir: 备份的数据库的路径
4.2. 实例：
```
mongorestore -h 127.0.0.1 -d wcx2020 --dir D:\mongodbBackup
```
4.3. 注意事项：
恢复的时候可以加--drop 覆盖原来的数据库（慎用），最好提前备份好当前数据库；同时要更改数据库名，不然恢复会失败。