```
mongo 127.0.0.1:27017

use admin;
db.auth("root", "123456")

db.grantRolesToUser('devA', ['readWriteAnyDatabase']);
db.grantRolesToUser('devB', ['readWriteAnyDatabase']);

db.grantRolesToUser('devB', [{ role: 'readWrite', db: 'client' }]);
```

```
mongo 127.0.0.1:27017

use admin;

db.createUser({ user:"root", pwd:"123456", roles:["root"] })
db.createUser({ user:"admin", pwd:"123456", roles:["readWrite", "dbAdmin"] })
```


https://quickref.cn/docs/mongodb.html