一、下载
进入版本历史

https://www.mongodb.com/try/download/community-edition/releases/archive

找到4.0.1下载地址进行下载

https://fastdl.mongodb.org/win32/mongodb-win32-x86_64-2008plus-ssl-4.0.1.zip

下载完成，解压后更名为mongodb-4.0.1

二、配置
新建mongodb-data文件夹，
在里面新建logs文件夹，在logs文件夹新建mongo.log文件，
在mongodb-data文件夹新建mongo.conf文件，内容如下：

```
# 数据库路径
dbpath=D:\soft\mongodb-data
 
# 日志输出文件路径
logpath=D:\soft\mongodb-data\logs\mongo.log
 
# 错误日志采用追加模式
logappend=true
 
# 启用日志文件，默认启用 
journal=true
 
# 这个选项可以过滤掉一些无用的日志信息，若需要调试使用请设置为false  
quiet=true
 
# 端口号 默认为27017
port=27017
注意：path路径请根据实际路径自行修改
```
三、安装
cd进入mongodb-4.0.1/bin，然后执行：
```
mongod --config "D:\soft\mongodb-data\mongo.conf"
D:\soft\mongodb-4.0.1\bin>mongod --config "D:\soft\mongodb-data\mongo.conf"
2024-05-11T00:55:10.613+0800 I CONTROL  [main] Automatically disabling TLS 1.0,
to force-enable TLS 1.0 specify --sslDisabledProtocols 'none'
```
执行完成后，关掉窗口

四、启动
cd进入mongodb-4.0.1/bin，然后执行：
```
mongod --dbpath "D:\soft\mongodb-data"
D:\soft\mongodb-4.0.1\bin>mongod --dbpath "D:\soft\mongodb-data"
2024-05-11T00:57:03.018+0800 I CONTROL  [main] Automatically disabling TLS 1.0,
to force-enable TLS 1.0 specify --sslDisabledProtocols 'none'
2024-05-11T00:57:03.378+0800 I CONTROL  [initandlisten] MongoDB starting : pid=6
360 port=27017 dbpath=D:\soft\mongodb-data 64-bit host=PC-20221205RLEH
2024-05-11T00:57:03.378+0800 I CONTROL  [initandlisten] targetMinOS: Windows 7/W
indows Server 2008 R2
2024-05-11T00:57:03.379+0800 I CONTROL  [initandlisten] db version v4.0.1
2024-05-11T00:57:03.380+0800 I CONTROL  [initandlisten] git version: 54f1582fc6e
b01de4d4c42f26fc133e623f065fb
2024-05-11T00:57:03.380+0800 I CONTROL  [initandlisten] allocator: tcmalloc
2024-05-11T00:57:03.381+0800 I CONTROL  [initandlisten] modules: none
2024-05-11T00:57:03.382+0800 I CONTROL  [initandlisten] build environment:
2024-05-11T00:57:03.382+0800 I CONTROL  [initandlisten]     distmod: 2008plus-ss
l
2024-05-11T00:57:03.383+0800 I CONTROL  [initandlisten]     distarch: x86_64
2024-05-11T00:57:03.383+0800 I CONTROL  [initandlisten]     target_arch: x86_64
2024-05-11T00:57:03.384+0800 I CONTROL  [initandlisten] options: { storage: { db
Path: "D:\soft\mongodb-data" } }
2024-05-11T00:57:03.385+0800 I STORAGE  [initandlisten] Detected data files in D
:\soft\mongodb-data created by the 'wiredTiger' storage engine, so setting the a
ctive storage engine to 'wiredTiger'.
2024-05-11T00:57:03.385+0800 I STORAGE  [initandlisten] wiredtiger_open config:
create,cache_size=7657M,session_max=20000,eviction=(threads_min=4,threads_max=4)
,config_base=false,statistics=(fast),log=(enabled=true,archive=true,path=journal
,compressor=snappy),file_manager=(close_idle_time=100000),statistics_log=(wait=0
),verbose=(recovery_progress),
2024-05-11T00:57:03.541+0800 I STORAGE  [initandlisten] WiredTiger message [1715
360223:540574][6360:1996501872], txn-recover: Main recovery loop: starting at 1/
22784
2024-05-11T00:57:03.682+0800 I STORAGE  [initandlisten] WiredTiger message [1715
360223:682582][6360:1996501872], txn-recover: Recovering log 1 through 2
2024-05-11T00:57:03.763+0800 I STORAGE  [initandlisten] WiredTiger message [1715
360223:763587][6360:1996501872], txn-recover: Recovering log 2 through 2
2024-05-11T00:57:03.834+0800 I STORAGE  [initandlisten] WiredTiger message [1715
360223:834591][6360:1996501872], txn-recover: Set global recovery timestamp: 0
2024-05-11T00:57:03.840+0800 I RECOVERY [initandlisten] WiredTiger recoveryTimes
tamp. Ts: Timestamp(0, 0)
2024-05-11T00:57:03.846+0800 I CONTROL  [initandlisten]
2024-05-11T00:57:03.846+0800 I CONTROL  [initandlisten] ** WARNING: Access contr
ol is not enabled for the database.
2024-05-11T00:57:03.846+0800 I CONTROL  [initandlisten] **          Read and wri
te access to data and configuration is unrestricted.
2024-05-11T00:57:03.847+0800 I CONTROL  [initandlisten]
2024-05-11T00:57:03.847+0800 I CONTROL  [initandlisten] ** WARNING: This server
is bound to localhost.
2024-05-11T00:57:03.847+0800 I CONTROL  [initandlisten] **          Remote syste
ms will be unable to connect to this server.
2024-05-11T00:57:03.849+0800 I CONTROL  [initandlisten] **          Start the se
rver with --bind_ip <address> to specify which IP
2024-05-11T00:57:03.849+0800 I CONTROL  [initandlisten] **          addresses it
 should serve responses from, or with --bind_ip_all to
2024-05-11T00:57:03.850+0800 I CONTROL  [initandlisten] **          bind to all
interfaces. If this behavior is desired, start the
2024-05-11T00:57:03.851+0800 I CONTROL  [initandlisten] **          server with
--bind_ip 127.0.0.1 to disable this warning.
2024-05-11T00:57:03.851+0800 I CONTROL  [initandlisten]
2024-05-11T00:57:03.851+0800 I CONTROL  [initandlisten] Hotfix KB2731284 or late
r update is not installed, will zero-out data files.
2024-05-11T00:57:03.853+0800 I CONTROL  [initandlisten]
2024-05-11T00:57:04.976+0800 I FTDC     [initandlisten] Initializing full-time d
iagnostic data capture with directory 'D:/soft/mongodb-data/diagnostic.data'
2024-05-11T00:57:04.981+0800 I NETWORK  [initandlisten] waiting for connections
on port 27017
2024-05-11T00:57:41.882+0800 I NETWORK  [listener] connection accepted from 127.
0.0.1:10570 #1 (1 connection now open)
2024-05-11T00:57:41.894+0800 I NETWORK  [conn1] received client metadata from 12
7.0.0.1:10570 conn1: { application: { name: "MongoDB Shell" }, driver: { name: "
MongoDB Internal Client", version: "4.0.1" }, os: { type: "Windows", name: "Micr
osoft Windows 7", architecture: "x86_64", version: "6.1 SP1 (build 7601)" } }
2024-05-11T01:20:28.299+0800 I STORAGE  [conn1] createCollection: mgo.mgo with g
enerated UUID: e193d1e0-dcda-4da4-bbd3-6888eb4cd393
2024-05-11T01:24:11.967+0800 I COMMAND  [conn1] dropDatabase mgo - starting
2024-05-11T01:24:11.967+0800 I COMMAND  [conn1] dropDatabase mgo - dropping 0 co
llections
2024-05-11T01:24:11.969+0800 I COMMAND  [conn1] dropDatabase mgo - finished
```
窗口不要关闭

另外打开一个cmd窗口进行测试
cd进入mongodb-4.0.1/bin，然后执行：
```
mongo
```
这样就进入MongoDB的命令行模式了。

默认数据库test
```
> db
test
```
五、简单操作
5.1 创建数据库

语法
MongoDB 创建数据库的语法格式如下：

use <db_name>
如果数据库不存在，则创建数据库，否则切换到指定数据库。

实例
以下实例我们创建了数据库 mgo:

> use mgo
switched to db mgo
> db
mgo
> 
查看数据库，命令是show dbs：

> show dbs
admin   0.000GB
config  0.000GB
local   0.000GB
我们发现，数据库中并没有刚刚新建的mgo数据库，如果要显示数据库，必须插入数据：

> db.mgo.insertOne({"author":"csdn-svygh123"})
{
        "acknowledged" : true,
        "insertedId" : ObjectId("663e575ce895002292e8fa71")
}
> show dbs
admin   0.000GB
config  0.000GB
local   0.000GB
mgo     0.000GB
5.2 删除数据库

语法
MongoDB 删除数据库的语法格式如下：

db.dropDatabase()
删除的是当前的数据库

实例
以下实例我们要删除数据库 mgo:

先使用show dbs查询数据库：

> show dbs
admin   0.000GB
config  0.000GB
local   0.000GB
mgo     0.000GB
再切换到mgo数据库：

> use mgo
switched to db mgo
执行删除：

> db.dropDatabase()
{ "dropped" : "mgo", "ok" : 1 }
查询数据库：

> show dbs
admin   0.000GB
config  0.000GB
local   0.000GB
5.3 创建集合（相当于表）

语法
MongoDB 创建集合的语法格式如下：

db.createCollection(name, options)
查看帮助：db.help() 

> db.help()
DB methods:
        db.adminCommand(nameOrDocument) - switches to 'admin' db, and runs comma
nd [just calls db.runCommand(...)]
        db.aggregate([pipeline], {options}) - performs a collectionless aggregat
ion on this database; returns a cursor
        db.auth(username, password)
        db.cloneDatabase(fromhost) - deprecated
        db.commandHelp(name) returns the help for the command
        db.copyDatabase(fromdb, todb, fromhost) - deprecated
        db.createCollection(name, {size: ..., capped: ..., max: ...})
        db.createView(name, viewOn, [{$operator: {...}}, ...], {viewOptions})
        db.createUser(userDocument)
        db.currentOp() displays currently executing operations in the db
        db.dropDatabase()
        db.eval() - deprecated
        db.fsyncLock() flush data to disk and lock server for backups
        db.fsyncUnlock() unlocks server following a db.fsyncLock()
        db.getCollection(cname) same as db['cname'] or db.cname
        db.getCollectionInfos([filter]) - returns a list that contains the names
 and options of the db's collections
        db.getCollectionNames()
        db.getLastError() - just returns the err msg string
        db.getLastErrorObj() - return full status object
        db.getLogComponents()
        db.getMongo() get the server connection object
        db.getMongo().setSlaveOk() allow queries on a replication slave server
        db.getName()
        db.getPrevError()
        db.getProfilingLevel() - deprecated
        db.getProfilingStatus() - returns if profiling is on and slow threshold
        db.getReplicationInfo()
        db.getSiblingDB(name) get the db at the same server as this one
        db.getWriteConcern() - returns the write concern used for any operations
 on this db, inherited from server object if set
        db.hostInfo() get details about the server's host
        db.isMaster() check replica primary status
        db.killOp(opid) kills the current operation in the db
        db.listCommands() lists all the db commands
        db.loadServerScripts() loads all the scripts in db.system.js
        db.logout()
        db.printCollectionStats()
        db.printReplicationInfo()
        db.printShardingStatus()
        db.printSlaveReplicationInfo()
        db.dropUser(username)
        db.repairDatabase()
        db.resetError()
        db.runCommand(cmdObj) run a database command.  if cmdObj is a string, tu
rns it into {cmdObj: 1}
        db.serverStatus()
        db.setLogLevel(level,<component>)
        db.setProfilingLevel(level,slowms) 0=off 1=slow 2=all
        db.setWriteConcern(<write concern doc>) - sets the write concern for wri
tes to the db
        db.unsetWriteConcern(<write concern doc>) - unsets the write concern for
 writes to the db
        db.setVerboseShell(flag) display extra information in shell output
        db.shutdownServer()
        db.stats()
        db.version() current version of the server

db.createCollection(name, {size: ..., capped: ..., max: ...})
capped: 布尔值，是否固定大小

size: 大小，单位B(字节）

max: 文档最大数

实例
在 test 数据库中创建 mgo集合：

> use test
switched to db test
> db.createCollection("mgo")
{ "ok" : 1 }
查询集合：

> show collections
mgo
> show tables
mgo
创建固定集合：

> db.createCollection("fixedtb", { capped : true, size : 1024, max : 1 } )
{ "ok" : 1 }
插入数据到集合：

> db.fixedtb.insert({"author" : "svygh123"})
WriteResult({ "nInserted" : 1 })
注意：集合可以不用新建才插入，当不存在的集合插入数据时，集合会自动新建。

5.4 删除集合（表）

语法
MongoDB 删除集合的语法格式如下：

db.collection.drop()
实例
在 test 数据库中删除fixedtb集合：

> use test
switched to db test
> show collections
fixedtb
mgo
> db.fixedtb.drop()
true
> show collections
mgo
