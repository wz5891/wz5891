---
layout: doc
title: MyBatis适配多种数据库
---

## 概述
Mybatis支持一套代码要适配多种数据库。

在一个“namespace”，判断唯一的标志是id+databaseId。

例如：
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="demo.TimeMapper">

    <select id="getTime" resultType="string">
        select now() from dual
    </select>

    <select id="getTime" resultType="string" databaseId="sqlite">
        SELECT CURRENT_TIMESTAMP
    </select>
</mapper>
```
以上getTime语句，在sqllite数据库中，将执行第2条，在MySQL,H2数据库中将执行第1条。


## 获取数据库产品名称
首先得明确要兼容哪些数据库，他们的数据库产品名称是什么。得益于SPI设计，java语言制定了一个java.sql.DatabaseMetaData接口（jdbc接口），要求各个数据库的驱动都必须提供自己的产品名。因此我们如果想要兼容某数据库，只要在对应的驱动包中找到其对DatabaseMetaData的实现即可。

Sqlite
```
package org.sqlite.jdbc3;
public abstract class JDBC3DatabaseMetaData extends org.sqlite.core.CoreDatabaseMetaData {
    public String getDatabaseProductName() {
        return "SQLite";
    }
    。。。。。。
}
```

H2
```
package org.h2.jdbc;
public class JdbcDatabaseMetaData extends TraceObject implements DatabaseMetaData {
    public String getDatabaseProductName() {
        return "H2";
    }
    。。。。。。
}
```

MySQL
```
package com.mysql.cj.jdbc;
public class DatabaseMetaData implements java.sql.DatabaseMetaData {
    @Override
    public String getDatabaseProductName() throws SQLException {
        return "MySQL";
    }
}
```

## 启用多数据库适配支持

MyBatis 启用多数据库适配支持，需要配置`databaseIdProvider`：
```
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.ac.cn//DTD Config 3.0//EN"
        "http://mybatis.ac.cn/dtd/mybatis-3-config.dtd">
<configuration>
    <properties resource="jdbc.properties"/>

    <environments default="dev">
        <environment id="dev">
            <transactionManager type="JDBC"/>
            <dataSource type="POOLED">
                <property name="driver" value="${driver}"/>
                <property name="url" value="${url}"/>
                <property name="username" value="${username}"/>
                <property name="password" value="${password}"/>
            </dataSource>
        </environment>
    </environments>

    <databaseIdProvider type="DB_VENDOR">
        <property name="MySQL" value="mysql"/>
        <property name="SQLite" value="sqlite"/>
        <property name="H2" value="h2"/>
    </databaseIdProvider>

    <mappers>
        <mapper class="demo.TimeMapper"/>
    </mappers>
</configuration>
```


若是在SpringBoot中:
```
    @Bean
    public DatabaseIdProvider databaseIdProvider() throws SQLException {
        DatabaseIdProvider databaseIdProvider = new VendorDatabaseIdProvider();
        Properties properties = new Properties();
        // Key值(即产品名)来源于数据库，需要提前查清楚 ，
        // value值(即databaseId)可以随便填，你填“1” "2" "3"也行，但建议有明确意义，像下面这样
        properties.setProperty("MySQL", "mysql");
        properties.setProperty("DB2", "db2");
        properties.setProperty("H2", "h2");
        databaseIdProvider.setProperties(properties);
        return databaseIdProvider;
    }
 
    @Bean
    public SqlSessionFactory testdbSqlSessionFactory(@Qualifier("testdbDataSource") DataSource testdbDataSource)
            throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(testdbDataSource);
        //set 注入databaseIdProvider 到当前SqlSessionFactoryBean 
        sessionFactory.setDatabaseIdProvider(databaseIdProvider()); 
        sessionFactory.setMapperLocations(
                new PathMatchingResourcePatternResolver().getResources(TestDbDataSourceConfig.MAPPER_LOCATION));
        return sessionFactory.getObject();
    }
```




## 完整示例
pom.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.example</groupId>
    <artifactId>mybatis-study</artifactId>
    <version>1.0-SNAPSHOT</version>


    <dependencies>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.5.16</version>
        </dependency>

        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.26</version>
        </dependency>

        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <version>1.4.192</version>
        </dependency>

        <dependency>
            <groupId>org.xerial</groupId>
            <artifactId>sqlite-jdbc</artifactId>
            <version>3.34.0</version>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.7.30</version>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>1.2.3</version>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-core</artifactId>
            <version>1.2.3</version>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13.2</version>
            <scope>test</scope>
        </dependency>

    </dependencies>
</project>
```

logback.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration debug="false">
    <!--控制台日志， 控制台输出 -->
    <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <!--格式化输出：%d表示日期，%thread表示线程名，%-5level：级别从左显示5个字符宽度,%msg：日志消息，%n是换行符-->
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n</pattern>
        </encoder>
    </appender>

    <!-- 日志输出级别 -->
    <root level="DEBUG">
        <appender-ref ref="STDOUT" />
    </root>
</configuration>
```

demo.TimeMapper
```java
package demo;

public interface TimeMapper {
    String getTime();

    String getVersion();
}
```

demo\TimeMapper.xml
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="demo.TimeMapper">

    <select id="getTime" resultType="string">
        select now() from dual
    </select>

    <select id="getTime" resultType="string" databaseId="sqlite">
        SELECT CURRENT_TIMESTAMP
    </select>


    <select id="getVersion" resultType="java.lang.String" databaseId="mysql">
        select version()
    </select>

    <select id="getVersion" resultType="java.lang.String" databaseId="h2">
        select H2VERSION()
    </select>

    <select id="getVersion" resultType="java.lang.String" databaseId="sqlite">
        select sqlite_version()
    </select>
</mapper>
```

mybatis-config.xml
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.ac.cn//DTD Config 3.0//EN"
        "http://mybatis.ac.cn/dtd/mybatis-3-config.dtd">
<configuration>
    <properties resource="jdbc.properties"/>

    <environments default="dev">
        <environment id="dev">
            <transactionManager type="JDBC"/>
            <dataSource type="POOLED">
                <property name="driver" value="${driver}"/>
                <property name="url" value="${url}"/>
                <property name="username" value="${username}"/>
                <property name="password" value="${password}"/>
            </dataSource>
        </environment>
    </environments>

    <databaseIdProvider type="DB_VENDOR">
        <property name="MySQL" value="mysql"/>
        <property name="SQLite" value="sqlite"/>
        <property name="H2" value="h2"/>
    </databaseIdProvider>

    <mappers>
        <mapper class="demo.TimeMapper"/>
    </mappers>
</configuration>
```


jdbc.properties
```
#driver=com.mysql.cj.jdbc.Driver
#url=jdbc:mysql://localhost:3306/mybatis?characterEncoding=utf8&useSSL=true&serverTimezone=UTC
#username=root
#password=1

#driver=org.h2.Driver
#url=jdbc:h2:file:./h2_study
#username=root
#password=1


driver=org.sqlite.JDBC
url=jdbc:sqlite:./sqlite_study
username=root
password=1

```


demo\DatabaseIdProviderTest.java
```java
package demo;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class DatabaseIdProviderTest {
    private static final Logger log = LoggerFactory.getLogger(DatabaseIdProviderTest.class);
    @org.junit.Test
    public void test() {
        try {
            String resource = "mybatis-config.xml";
            InputStream inputStream = Resources.getResourceAsStream(resource);
            SqlSessionFactory sqlSessionFactory =
                    new SqlSessionFactoryBuilder().build(inputStream);

            SqlSession sqlSession = sqlSessionFactory.openSession();
            TimeMapper mapper = sqlSession.getMapper(TimeMapper.class);
            String time = mapper.getTime();
            log.info("Time=====================>[{}]",time);

            String version = mapper.getVersion();
            log.info("Version=======================>[{}]",version);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```


运行结果：

sqlite
```
2024-09-17 19:56:31.151 [main] DEBUG org.apache.ibatis.transaction.jdbc.JdbcTransaction - Setting autocommit to false on JDBC Connection [org.sqlite.jdbc4.JDBC4Connection@77e9807f]
2024-09-17 19:56:31.167 [main] DEBUG demo.TimeMapper.getTime - ==>  Preparing: SELECT CURRENT_TIMESTAMP
2024-09-17 19:56:31.183 [main] DEBUG demo.TimeMapper.getTime - ==> Parameters: 
2024-09-17 19:56:31.198 [main] DEBUG demo.TimeMapper.getTime - <==      Total: 1
2024-09-17 19:56:31.198 [main] INFO  demo.DatabaseIdProviderTest - Time=====================>[2024-09-17 11:56:31]
2024-09-17 19:56:31.214 [main] DEBUG demo.TimeMapper.getVersion - ==>  Preparing: select sqlite_version()
2024-09-17 19:56:31.214 [main] DEBUG demo.TimeMapper.getVersion - ==> Parameters: 
2024-09-17 19:56:31.214 [main] DEBUG demo.TimeMapper.getVersion - <==      Total: 1
2024-09-17 19:56:31.214 [main] INFO  demo.DatabaseIdProviderTest - Version=======================>[3.34.0]
```

h2
```
2024-09-17 19:57:09.244 [main] DEBUG org.apache.ibatis.transaction.jdbc.JdbcTransaction - Setting autocommit to false on JDBC Connection [conn0: url=jdbc:h2:file:./h2_study user=ROOT]
2024-09-17 19:57:09.260 [main] DEBUG demo.TimeMapper.getTime - ==>  Preparing: select now() from dual
2024-09-17 19:57:09.291 [main] DEBUG demo.TimeMapper.getTime - ==> Parameters: 
2024-09-17 19:57:09.322 [main] DEBUG demo.TimeMapper.getTime - <==      Total: 1
2024-09-17 19:57:09.322 [main] INFO  demo.DatabaseIdProviderTest - Time=====================>[2024-09-17 19:57:09.291]
2024-09-17 19:57:09.322 [main] DEBUG demo.TimeMapper.getVersion - ==>  Preparing: select H2VERSION()
2024-09-17 19:57:09.322 [main] DEBUG demo.TimeMapper.getVersion - ==> Parameters: 
2024-09-17 19:57:09.322 [main] DEBUG demo.TimeMapper.getVersion - <==      Total: 1
2024-09-17 19:57:09.322 [main] INFO  demo.DatabaseIdProviderTest - Version=======================>[1.4.192]
```

mysql
```
2024-09-17 19:57:37.705 [main] DEBUG org.apache.ibatis.transaction.jdbc.JdbcTransaction - Setting autocommit to false on JDBC Connection [com.mysql.cj.jdbc.ConnectionImpl@1cbb87f3]
2024-09-17 19:57:37.705 [main] DEBUG demo.TimeMapper.getTime - ==>  Preparing: select now() from dual
2024-09-17 19:57:37.749 [main] DEBUG demo.TimeMapper.getTime - ==> Parameters: 
2024-09-17 19:57:37.780 [main] DEBUG demo.TimeMapper.getTime - <==      Total: 1
2024-09-17 19:57:37.780 [main] INFO  demo.DatabaseIdProviderTest - Time=====================>[2024-09-17 11:57:37]
2024-09-17 19:57:37.780 [main] DEBUG demo.TimeMapper.getVersion - ==>  Preparing: select version()
2024-09-17 19:57:37.780 [main] DEBUG demo.TimeMapper.getVersion - ==> Parameters: 
2024-09-17 19:57:37.780 [main] DEBUG demo.TimeMapper.getVersion - <==      Total: 1
2024-09-17 19:57:37.780 [main] INFO  demo.DatabaseIdProviderTest - Version=======================>[8.0.11]
```

## 源码下载

https://gitee.com/wz5891/mybatis-study/tree/master/adapter_multiple_db