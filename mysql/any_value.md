MySQL中的any_value()函数

MySQL5.7之后，sql_mode中ONLY_FULL_GROUP_BY模式默认设置为打开状态。

ONLY_FULL_GROUP_BY的语义就是确定select target list中的所有列的值都是明确语义，简单的说来，在此模式下，target list中的值要么是来自于聚合函数（sum、avg、max等）的结果，要么是来自于group by list中的表达式的值。

网上有很多通过修改sql_mode的方式来解决此问题。

但除此方法，MySQL也提供了any_value()函数来抑制ONLY_FULL_GROUP_BY值被拒绝

                        
https://blog.csdn.net/weixin_45817985/article/details/133803836