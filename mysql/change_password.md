
```
# 进入mysql
mysql -u root -p
输入之前正确的密码进入

# 设置新密码
set password = password('xxx')

# 设置密码永不过期
# alter user 'root'@'localhost'password expire never;

# 更新权限
flush privileges
```

对于其它用户，修改方式与root用户一样