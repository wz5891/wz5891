
```
delete from a where a.id in (select b.a_id from b);
```

在表 a 和表 b 的数据量都非常大的时候，执行该语句的速度会超级超级慢，原因是该语句不会使用索引扫描，反而会扫描全表，造成sql执行极慢。


经过不懈的查资料，终于发现了一个利用空间换时间的办法，优化之后能够显著提升sql执行效率。

具体优化方式如下：
```
delete from a where a.id in (select c.a_id from (select b.a_id from b) c);
```
建立一个临时表 c 存储表 b 中查询出来的结果，这种方式能显著提升sql的执行效率。