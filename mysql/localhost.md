localhost、% 的含义
localhost 表示 只能本机能连接MySQL
% 表示 任何客户机都能连接MySQL
 

localhost、% 的包含关系
版本	用户中的%是否包括localhost
MySQL5.1	不包括
MySQL5.6	不包括
MySQL5.7	包括
MySQL8.0	包括
 

修改 localhost 为 %
```
mysql> update user set host='%' where user='username';
mysql> flush privileges; 
```
