## 介绍
MySQL Utilities 是一组基于python语言编写的python库的命令行实用工具集,依赖于python 2.6。该工具提供了MySQL数据库运维工程中常用的一些工具，诸如克隆、复制、比较、差异、导出、导入、安装、配置、索引、磁盘查看等等。

下载地址：
https://cdn.mysql.com/archives/mysql-utilities/mysql-utilities-1.6.5-winx64.msi


## 使用
安装后，打开 MySQL Utilities Console
输入 help 可获取帮助


### 比较两个数据库的差异，并生成差异SQL语句
```
mysqldiff --skip-table-options --quiet --server1=username:pwd@ip:port --server2=username:pwd@ip:port testdb:testdb2 --difftype=sql > d:\test_diff.sql
```