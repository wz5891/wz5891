## 查看与关闭进程
```
show processlist

kill 进程id
```


## 查看锁
在MySQL 5.7中，您可以使用performance_schema和information_schema来查看锁的信息。以下是一些查询示例：

查看当前的锁定情况：
```
SELECT * FROM performance_schema.data_locks;
```
查看锁等待的情况：
```
SELECT * FROM performance_schema.data_lock_waits;
```
查看锁定的对象（例如表）：
```
SELECT * FROM performance_schema.metadata_locks;
```
查看锁定的事务：
```
SELECT * FROM information_schema.innodb_trx;
```
请注意，performance_schema需要启用才能使用这些表。如果未启用，您可以通过设置performance_schema变量为ON来启用它。
```
SET GLOBAL performance_schema = ON;
```
确保在查询数据时考虑到您的MySQL用户权限，并且在生产环境中谨慎使用这些命令，因为它们可能会对性能产生影响。


```

SELECT * FROM INFORMATION_SCHEMA.INNODB_LOCK_WAITS;

SELECT * FROM INFORMATION_SCHEMA.INNODB_LOCKS;

SELECT * FROM INFORMATION_SCHEMA.INNODB_TRX;
```



## 查看innodb引擎状态
常用的地方一、死锁分析 二、innodb内存使用情况
```
show engine innodb status
```

通过show engine innodb status 查看内存使用情况：

1、show variables like 'innodb_buffer_pool_size' 查看buffer pool 的内存配置
```
show variables like 'innodb_buffer_pool_size';
+-------------------------+-----------+
| Variable_name           | Value     |
+-------------------------+-----------+
| innodb_buffer_pool_size | 268435456 |
+-------------------------+-----------+
1 row in set (0.01 sec)

mysql> select 268435456/1024/1024 as innodb_buffer_pool_size_in_MB;
+-------------------------------+
| innodb_buffer_pool_size_in_MB |
+-------------------------------+
|                  256.00000000 |
+-------------------------------+
1 row in set (0.00 sec)
```
2、通过show engine innodb status 查看内存使用明细
```
show engine innodb status ;

----------------------
BUFFER POOL AND MEMORY
----------------------
Total large memory allocated 274857984
Dictionary memory allocated 116177
Buffer pool size   16382
Free buffers       16002
Database pages     380
Old database pages 0
Modified db pages  0
Pending reads      0
Pending writes: LRU 0, flush list 0, single page 0
Pages made young 0, not young 0
0.00 youngs/s, 0.00 non-youngs/s
Pages read 345, created 35, written 37
0.00 reads/s, 0.00 creates/s, 0.00 writes/s
No buffer pool page gets since the last printout
Pages read ahead 0.00/s, evicted without access 0.00/s, Random read ahead 0.00/s
LRU len: 380, unzip_LRU len: 0
I/O sum[0]:cur[0], unzip sum[0]:cur[0]
```
