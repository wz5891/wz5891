mysql5.7慢查询配置，即慢查询检查，slow log
## 慢查询配置

```
show VARIABLES like 'slow_query_log';      -- 是否开启了慢查询
set GLOBAL slow_query_log=1;               -- 启用慢查询
show VARIABLES like '%long_query_time%';   -- 多慢算慢呢？默认10秒 
set GLOBAL long_query_time=0;              -- 任何SQL都记录为慢SQL，方便我们观察慢SQL日志
show VARIABLES like '%log_queries_not_using_indexes%';      -- 记录没有使用到索引的SQL, 默认是OFF不打开
set global log_output='FILE';               -- 慢查询日志的记录方式：文件 FILE、表 TABLE，默认是文件
show VARIABLES like 'slow_query_log_file';  -- 慢查询日志的存储log文件名，例如：mysql57-slowsql.log
```

以上这些set语句的配置，都可以在my.cnf配置文件里设置，比如在my.cnf里添加一行新配置：long_query_time=0.01  代表慢于0.01秒的SQL将被记录为慢SQL


## 慢查询日志文件

这里仅列举当slow_query_log_file设置为FILE日志文件格式的例子。

根据`slow_query_log_file`可以找到`slow log`的日志文件名，然后到mysql的data目录下找slow log文件，查看慢SQL日志文件内容，每一次慢查询的记录在日志文件里长这样：
```
SET timestamp=1623770266;         -- 代表执行记录时间
SELECT * FROM `mytest`.`t_user` LIMIT 0;    -- 慢SQL
# Time: 2021-06-15T15:17:46.832361Z         -- 执行开始时间
# User@Host: root[root] @ localhost [::1]  Id:     4        -- 执行User是root，执行主机是localhost，执行线程是4
# Query_time: 0.000153  Lock_time: 0.000000 Rows_sent: 0  Rows_examined: 0    -- Query_time查询了多久毫秒，Lock_time锁定了多久毫秒，Rows_sent查询了多少结果集条数发给用户，Rows_examined扫描了多少行数，才得到了最终结果集。
```


## 使用mysqldumpslow命令分析慢SQL

先来查看下slow log文件本身长啥样：

 

centos8下，可在该路径：/usr/bin/mysqldumpslow  下找到mysqldumpslow这个执行命令文件，可对slow log文件进行指定命令查找： 
```
./mysqldumpslow -S r -t 2 /var/lib/mysql/localhost-slow.log    
```
这里-S参数后面跟的r是row的缩写，意思是从slow log里查找按照返回行数最高排序，默认是at即平均执行时间。-r 后面跟着的数字是返回几个结果，类似于limit
 
```
  ./mysqldumpslow --help       -- 查看命令有哪些可用参数
```
几个重要参数罗列：

```
-s order (c,t,l,r,at,al,ar) 
         c:总次数
         t:总时间
         l:锁的时间
         r:获得的结果行数
         at,al,ar :指t,l,r平均数  【例如：at = 总时间/总次数】
-s 对结果进行排序，怎么排，根据后面所带的 (c,t,l,r,at,al,ar)，缺省为at
-g PATTERN   grep: only consider stmts that include this string：通过grep来筛选语句。
```