mysql迁移数据库用户和权限

在迁移mysql数据库主机的时候，系统数据库mysql中存储的数据库用户及其权限需要人工迁移，我整理了一套迁移方法，供参考：

1、导出mysql库中的user表
mysqldump -uroot -p'密码' mysql user > /tmp/user.sql
2、导出mysql库中用户权限

2.1. 获取mysql库中用户的列表信息
getuserlist.sh 脚本内容

mysql -B -N -uroot -p'密码' -S /var/lib/mysql/mysql.sock -P 3306 -e "SELECT CONCAT('\'', user,'\'@\'', host, '\'') FROM user WHERE user NOT IN('replication','root','','mysql.session','mysql.sys')" mysql > /tmp/mysql_users.txt
2.2 获取用户权限信息
getuserPrv.sh 脚本内容

while read line; do mysql -B -N -uroot -p'密码' -S /var/lib/mysql/mysql.sock -P 3306 -e "SHOW GRANTS FOR $line"; done < /tmp/mysql_users.txt > /tmp/mysql_all_users_sql.sql

2.3. 对上面生成sql脚本文件进行修改

在每行的结尾增加;做为结束符

sed -i 's/$/;/' /tmp/mysql_all_users_sql.sql
在最后一行增加 "flush privileges;" 来刷新权限

sed -i '$a flush privileges;' /tmp/mysql_all_users_sql.sql
3、在目标mysql上导入user用户及权限

将上面处理过的sql脚本文件/tmp/user.sql、/tmp/mysql_all_users_sql.sql 在目标mysql实例上执行
注意：如果不想导入root@localhost等用户，需要编辑/tmp/user.sql，将不想导入的用户手工删掉。
