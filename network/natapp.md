NATAPP1分钟快速新手教程

官网
https://natapp.cn/


## 获取隧道authtoken
登录后，在我的隧道（含一个免费隧道） 获取 authtoken。

## 下载客户端
目标服务器上下载 natapp 客户端(https://natapp.cn/ 找操作系统的对应版本)

## 开启客户端
### config.ini 方式
natapp客户同级目录创建 `config.ini`文件，内容如下：

```
#将本文件放置于natapp同级目录 程序将读取 [default] 段
#在命令行参数模式如 natapp -authtoken=xxx 等相同参数将会覆盖掉此配置
#命令行参数 -config= 可以指定任意config.ini文件
[default]
authtoken=                      #对应一条隧道的authtoken
clienttoken=                    #对应客户端的clienttoken,将会忽略authtoken,若无请留空,
log=none                        #log 日志文件,可指定本地文件, none=不做记录,stdout=直接屏幕输出 ,默认为none
loglevel=ERROR                  #日志等级 DEBUG, INFO, WARNING, ERROR 默认为 DEBUG
http_proxy=                     #代理设置 如 http://10.123.10.10:3128 非代理上网用户请务必留空
```

将前面获取的 authtoken填进去

然后运行
```
./natapp
```

### 通过参数方式
```
./natapp -authtoken=7bdb3f793355574c
```

运行成功后，会提示可访问的公网地址