https://www.cnblogs.com/echohye/p/16722833.html





## 生成ssl证书
openssl在centos中是标配，所以直接在centos中操作，因为要多个域名和IP，故而需要编辑一个配置文件

1.创建配置文件req.cnf
```
# 定义输入用户信息选项的"特征名称"字段名，该扩展字段定义了多项用户信息。
distinguished_name = req_distinguished_name

# 生成自签名证书时要使用的证书扩展项字段名，该扩展字段定义了要加入到证书中的一系列扩展项。
x509_extensions = v3_req

# 如果设为no，那么 req 指令将直接从配置文件中读取证书字段的信息，而不提示用户输入。
prompt = no

[req_distinguished_name]
#国家代码，一般都是CN(大写)
C = CN
#省份
ST = gd
#城市
L = gz
#企业/单位名称
O = echohye
#企业部门
OU = echohye
#证书的主域名
CN = 192.168.11.111

##### 要加入到证书请求中的一系列扩展项 #####
[v3_req]
keyUsage = critical, digitalSignature, keyAgreement
extendedKeyUsage = serverAuth
subjectAltName = @alt_names

[ alt_names ]
IP.1 = 192.168.11.111
# IP.2 = 192.168.11.222

```
在放置该文件的目录下执行以下命令
```
openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout server.key -out server.crt -config req.cnf -sha256
```

openssl 命令参数说明

req          大致有3个功能：生成证书请求文件、验证证书请求文件和创建根CA
-x509        说明生成自签名证书
-nodes       openssl req在自动创建私钥时，将总是加密该私钥文件，并提示输入加密的密码。可以使用"-nodes"选项禁止加密私钥文件。
-days        指定所颁发的证书有效期。
-newkey      实际上，"-x509"选项和"-new"或"-newkey"配合使用时，可以不指定证书请求文件，它在自签署过程中将在内存中自动创建证书请求文件
              "-newkey"选项和"-new"选项类似，只不过"-newkey"选项可以直接指定私钥的算法和长度，所以它主要用在openssl req自动创建私钥时。
rsa:2048     rsa表示创建rsa私钥，2048表示私钥的长度。
-keyout      指定私钥保存位置。
-out         新的证书请求文件位置。
-config      指定req的配置文件，指定后将忽略所有的其他配置文件。如果不指定则默认使用/etc/pki/tls/openssl.cnf中req段落的值
会生成server.crt和server.key两个文件

## 服务器端配置证书(nginx)
```

server {
        listen       8081 ssl;
        server_name  192.168.11.131;
        ssl_certificate /home/ssl/server.crt;
        ssl_certificate_key /home/ssl/server.key;
        ssl_session_cache shared:SSL:10m;
        ssl_session_timeout 120m;
        ssl_prefer_server_ciphers on;
        ssl_session_tickets off;
        ssl_stapling_verify on;

        location /{
            root   html;
            index  index.html index.htm;
        }	

        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }

```
