Vue 每次打包会生成唯一文件名的js/css,所以只需要配置index.html不缓存就可以解决浏览器缓存文件的问题。
```
location / {
    limit_except GET POST {
        deny all;
    }
    alias /usr/share/nginx/html/main-webapp-vue/;
    index index.html;
    try_files $uri $uri/ /usr/share/nginx/html/main-webapp-vue/index.html;
	
	# js、css、图片缓存100天
	if ($request_filename ~* .*\.(js|css|woff|png|jpg|jpeg)$)
	{
		expires    100d;
		#add_header Cache-Control "max-age = 8640000";
	}
	
	# html不缓存
	if ($request_filename ~* .*\.(?:htm|html)$)
	{
		add_header Cache-Control "no-store";
	}
}
```