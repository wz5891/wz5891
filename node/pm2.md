pm2 简介
PM2 是一款非常优秀的 Node 进程管理工具，它有着丰富的特性：能够充分利用多核 CPU且能够负载均衡、能够帮助应用在崩溃后、指定时间(cluster model)和超出最大内存限制等情况下实现自动重启。

PM2 是开源的基于 Nodejs 的应用进程管理器，包括守护进程，监控，日志的一整套完整的功能

https://blog.csdn.net/sunyctf/article/details/130655852