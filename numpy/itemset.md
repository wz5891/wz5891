```
import numpy as np

# Creating a one-dimensional array
arr = np.array([1, 2, 3, 4])

# Setting the 2nd element (index 1) to 10
arr.itemset(1, 10)


print(arr)
```

若在 numpy2.0及以上版本运行，会提示：
```
AttributeError: `itemset` was removed from the ndarray class in NumPy 2.0. Use `arr[index] = value` instead.
```

修改为
```
arr[1] = 10
```