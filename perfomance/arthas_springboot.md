---
title: 使用Arthas在线查找SpringBoot项目问题
---

## Arthas上手
[Arthas(阿尔萨斯) ](https://arthas.aliyun.com/doc/)是 Alibaba 开源的 Java 诊断工具
```
curl -O https://arthas.aliyun.com/arthas-boot.jar
java -jar arthas-boot.jar
```

或
```
./as.sh
```

## Docker镜像添加Arthas
创建`Dockerfile`
```dockerfile
FROM topsoft/tomcat-with-fonts:v3

LABEL authors="liwei"
LABEL description="添加Arthas"

ADD arthas.tar.gz /arthas
```

制作镜像
```sh
docker build -t topsoft/tomcat-with-fonts:v3_arthas .
```

> [Arthas下载链接](https://arthas.aliyun.com/download/latest_version?mirror=aliyun)


## 查询 UserController 类加载器
sc 查找加载 UserController 的 ClassLoader
`sc -d *UserController | grep classLoaderHash`

## 修改日志级别
使用 logger
使用 logger 命令可以相较于 ognl 更加便捷的实现 logger level 的动态设置。

使用 logger 命令获取对应 logger 信息
```
logger --name com.example.demo.arthas.user.UserController
```
单独设置 UserController 的 logger level
```
logger --name com.example.demo.arthas.user.UserController --classLoaderClass org.springframework.boot.loader.LaunchedURLClassLoader --level WARN
```
再次获取对应 logger 信息，可以发现已经是 WARN 了：
```
logger --name com.example.demo.arthas.user.UserController
```
修改 logback 的全局 logger level
```
logger --name ROOT --classLoaderClass org.springframework.boot.loader.LaunchedURLClassLoader --level WARN
```
运行下面命令可以看到 ROOT 的 level 已经修改为了 WARN
```
logger --name ROOT --classLoaderClass org.springframework.boot.loader.LaunchedURLClassLoader
```

```
logger --name org.apache.http --classLoaderClass org.springframework.boot.loader.LaunchedURLClassLoader --level TRACE
```
## 查看方法参数与返回值
```
watch demo.MathGame primeFactors -x 2
```
## 查看Bean实例
Vmtool 获取 HelloWorldService 对象实例，并调用函数
```
vmtool --action getInstances --className com.example.demo.arthas.aop.HelloWorldService --express 'instances[0].getHelloMessage()'


vmtool --action getInstances --className topsoft.food.instrument.file.loader.service.impl.FileGetterFtpImpl --express 'instances[0].host'
```


