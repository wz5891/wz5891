Ubuntu上的Docker，可以执行以下命令:
```
启动：sudo systemctl start docker
停止：sudo systemctl stop docker
重启：sudo systemctl restart docker
```


# 安装并使用Jupyter Notebook
```
apt install jupyter-notebook jupyter
```

生成密码 
输入`python`进入命令行
```
>>> from notebook.auth import passwd
>>> passwd()
Enter password: 
Verify password: 
'argon2:$argon2id$v=19$m=10240,t=10,p=8$5xZHPzRGcD7pT/zUR8B6nQ$z7QQUzfRb8pD9owQNxc9sg'
>>>
``` 
此处密码是123456


生成配置文件
```
jupyter notebook --generate-config
```

修改配置文件
```
c.NotebookApp.ip='*' # 如果这里修过过后启动服务报错 则修改为c.NotebookApp.ip='0.0.0.0'
c.NotebookApp.password=u'sha1****' #就之前保存的验证密码
c.NotebookApp.open_browser =False # 设置是否自动打开浏览器
c.NotebookApp.port =8888  # 设置端口
c.NotebookApp.allow_remote_access = True
```

启动
```
jupyter notebook --allow-root

nohup jupyter notebook --allow-root &
```





```
[1Panel Log]: =================感谢您的耐心等待，安装已经完成================== 
[1Panel Log]:  
[1Panel Log]: 请用浏览器访问面板: 
[1Panel Log]: 外网地址: http://120.235.10.4:25210/b8127ab0d5 
[1Panel Log]: 内网地址: http://192.168.123.121:25210/b8127ab0d5 
[1Panel Log]: 面板用户: 988fc867dd 
[1Panel Log]: 面板密码: f2b3cfbfc7 
[1Panel Log]:  
[1Panel Log]: 项目官网: https://1panel.cn 
[1Panel Log]: 项目文档: https://1panel.cn/docs 
[1Panel Log]: 代码仓库: https://github.com/1Panel-dev/1Panel 
[1Panel Log]:  
[1Panel Log]: 如果使用的是云服务器，请至安全组开放 25210 端口 
[1Panel Log]:  
[1Panel Log]: 为了您的服务器安全，在您离开此界面后您将无法再看到您的密码，请务必牢记您的密码。 
[1Panel Log]:  
[1Panel Log]: ================================================================ 
```