## 一、POI 4中Word元素介绍
段落（Paragraph）
段落是Word文档中的基本结构单位，包含了文本内容、格式等信息。段落可以有不同的对齐方式（如左对齐、居中对齐、右对齐等），字体样式（如字体大小、字体颜色、加粗、倾斜等），行距等格式设置。
表格（Table）
表格由行（Row）和列（Column）组成。表格中的每个单元格（Cell）都可以包含文本、格式设置甚至嵌套表格等内容。表格可以有边框样式、表头、单元格对齐方式等属性。
图片（Picture）
图片在Word文档中是一种特殊的元素。它有自己的尺寸、位置、环绕方式（如嵌入型、四周型环绕等）等属性。
文本（Text）
文本是段落中的基本组成部分，每个段落中的文本可以有不同的字体格式、字符样式等。
页眉（Header）和页脚（Footer）
页眉位于页面的顶部，页脚位于页面的底部。它们通常包含页码、文档标题、作者等信息。页眉和页脚可以在整个文档中保持一致，也可以针对不同的章节进行不同的设置。
目录（Table of Contents）
目录是对文档内容结构的一种索引，通常列出文档中的标题及其对应的页码。目录的格式和级别可以根据文档的需求进行定制。


POI 中 Run 的定义和功能
在Apache POI库中，特别是在处理Word文档时，Run是一个非常重要的概念。它代表文档中的一个文本运行（text run），可以包含文本、字体样式、颜色等多种属性。通过Run对象，可以对文档中的文本进行精细的控制和格式设置。

文本格式化：通过不同的Run对象，可以实现文本的加粗、斜体、下划线等多种格式的设置。
段落样式调整：结合其他POI类，如XWPFDefaultRunStyle，可以对Run对象的样式进行更详细的调整。

运行（XWPFRun）是段落中的一个连续文本片段，这些文本片段具有相同的属性（如字体、颜色、大小等）。换句话说，一个段落可以包含多个运行，每个运行内的文本具有相同的样式，而不同运行之间的文本样式可以不同。这种设计允许在单个段落内实现复杂的文本格式化，而不需要将整个段落视为一个单一的格式化单元。

理解运行的概念对于使用Apache POI处理Word文档非常重要，因为它允许你以精细的方式控制文档的格式。通过操作运行，你可以实现复杂的文本布局和样式，而不仅仅是简单的文本添加。

## 二、POI读取及操作各元素示例代码
段落操作示例
依赖引入：
```
<dependency> 
    <groupId>org.apache.poi</groupId>  
    <artifactId>poi</artifactId> 
    <version>4.1.2</version> 
</dependency> 
<dependency> 
    <groupId>org.apache.poi</groupId>  
    <artifactId>poi - ooxml</artifactId> 
    <version>4.1.2</version> 
</dependency> 
```
代码示例：
```
import org.apache.poi.xwpf.usermodel.XWPFDocument;  
import org.apache.poi.xwpf.usermodel.XWPFParagraph;  
import java.io.FileInputStream;  
import java.io.FileOutputStream;  
import java.io.IOException;  
public class ParagraphExample { 
    public static void main(String[] args) throws IOException { 
        // 读取Word文档 
        FileInputStream fis = new FileInputStream("input.docx");  
        XWPFDocument doc = new XWPFDocument(fis); 
        // 遍历段落 
        for (XWPFParagraph para : doc.getParagraphs())  { 
            System.out.println(para.getText());  
        } 
        // 创建新段落并设置格式 
        XWPFParagraph newPara = doc.createParagraph();  
        newPara.setAlignment(ParagraphAlignment.CENTER);  
        // 保存修改后的文档 
        FileOutputStream fos = new FileOutputStream("output.docx");  
        doc.write(fos);  
        fos.close();  
        fis.close();  
    } 
} 
```
表格操作示例
代码示例：
```
import org.apache.poi.xwpf.usermodel.XWPFDocument;  
import org.apache.poi.xwpf.usermodel.XWPFTable;  
import org.apache.poi.xwpf.usermodel.XWPFTableRow;  
import java.io.FileInputStream;  
import java.io.FileOutputStream;  
import java.io.IOException;  
public class TableExample { 
    public static void main(String[] args) throws IOException { 
        FileInputStream fis = new FileInputStream("input.docx");  
        XWPFDocument doc = new XWPFDocument(fis); 
        // 遍历表格 
        for (XWPFTable table : doc.getTables())  { 
            for (XWPFTableRow row : table.getRows())  { 
                for (int i = 0; i < row.getTableCells().size();  i++) { 
                    System.out.println(row.getCell(i).getText());  
                } 
            } 
        } 
        // 创建新表格 
        XWPFTable newTable = doc.createTable(3,  3); 
        XWPFTableRow firstRow = newTable.getRow(0);  
        firstRow.getCell(0).setText("Header  1"); 
        firstRow.getCell(1).setText("Header  2"); 
        firstRow.getCell(2).setText("Header  3"); 
        // 保存修改后的文档 
        FileOutputStream fos = new FileOutputStream("output.docx");  
        doc.write(fos);  
        fos.close();  
        fis.close();  
    } 
} 
```
图片操作示例
代码示例：
```
import org.apache.poi.xwpf.usermodel.XWPFDocument;  
import org.apache.poi.xwpf.usermodel.XWPFPicture;  
import org.apache.poi.xwpf.usermodel.XWPFRun;  
import java.io.FileInputStream;  
import java.io.FileOutputStream;  
import java.io.IOException;  
public class PictureExample { 
    public static void main(String[] args) throws IOException { 
        FileInputStream fis = new FileInputStream("input.docx");  
        XWPFDocument doc = new XWPFDocument(fis); 
        // 遍历段落中的图片 
        for (XWPFParagraph para : doc.getParagraphs())  { 
            for (XWPFRun run : para.getRuns())  { 
                if (run.getEmbeddedPictures()!=  null) { 
                    for (XWPFPicture picture : run.getEmbeddedPictures())  { 
                        System.out.println(picture.getPictureData().getFileName());  
                    } 
                } 
            } 
        } 
        // 在文档末尾添加一张图片（假设图片已存在，这里只是示例） 
        XWPFParagraph newPara = doc.createParagraph();  
        XWPFRun newRun = newPara.createRun();  
        FileInputStream picFis = new FileInputStream("test.jpg");  
        newRun.addPicture(picFis,  Document.PICTURE_TYPE_JPEG, "test.jpg",  
                Units.toEMU(100),  Units.toEMU(100));  
        // 保存修改后的文档 
        FileOutputStream fos = new FileOutputStream("output.docx");  
        doc.write(fos);  
        fos.close();  
        fis.close();  
    } 
} 
```
文本操作示例（主要是对段落中的文本操作）
代码示例：
```
import org.apache.poi.xwpf.usermodel.XWPFDocument;  
import org.apache.poi.xwpf.usermodel.XWPFParagraph;  
import org.apache.poi.xwpf.usermodel.XWPFRun;  
import java.io.FileInputStream;  
import java.io.FileOutputStream;  
import java.io.IOException;  
public class TextExample { 
    public static void main(String[] args) throws IOException { 
        FileInputStream fis = new FileInputStream("input.docx");  
        XWPFDocument doc = new XWPFDocument(fis); 
        // 遍历段落并修改文本格式 
        for (XWPFParagraph para : doc.getParagraphs())  { 
            for (XWPFRun run : para.getRuns())  { 
                if (run.getText(0)!=  null) { 
                    run.setFontSize(16);  
                    run.setBold(true);  
                    run.setColor("FF0000");  
                } 
            } 
        } 
        // 在文档末尾添加新文本 
        XWPFParagraph newPara = doc.createParagraph();  
        XWPFRun newRun = newPara.createRun();  
        newRun.setText("This  is new text."); 
        newRun.setFontFamily("Arial");  
        // 保存修改后的文档 
        FileOutputStream fos = new FileOutputStream("output.docx");  
        doc.write(fos);  
        fos.close();  
        fis.close();  
    } 
} 
```
页眉和页脚操作示例
代码示例：
```
import org.apache.poi.xwpf.usermodel.XWPFDocument;  
import org.apache.poi.xwpf.usermodel.XWPFHeader;  
import org.apache.poi.xwpf.usermodel.XWPFFooter;  
import java.io.FileInputStream;  
import java.io.FileOutputStream;  
import java.io.IOException;  
public class HeaderFooterExample { 
    public static void main(String[] args) throws IOException { 
        FileInputStream fis = new FileInputStream("input.docx");  
        XWPFDocument doc = new XWPFDocument(fis); 
        // 设置页眉 
        XWPFHeader header = doc.getHeader();  
        if (header!= null) { 
            XWPFParagraph para = header.getParagraphArray(0);  
            if (para == null) { 
                para = header.createParagraph();  
            } 
            para.createRun().setText("This  is a header."); 
        } 
        // 设置页脚 
        XWPFFooter footer = doc.getFooter();  
        if (footer!= null) { 
            XWPFParagraph para = footer.getParagraphArray(0);  
            if (para == null) { 
                para = footer.createParagraph();  
            } 
            para.createRun().setText("This  is a footer. Page "); 
            XWPFRun pageRun = para.createRun();  
            pageRun.setText(String.valueOf(doc.getProperties().getExtendedProperties().getUnderlyingProperties().getPages()));  
        } 
        // 保存修改后的文档 
        FileOutputStream fos = new FileOutputStream("output.docx");  
        doc.write(fos);  
        fos.close();  
        fis.close();  
    } 
} 
```
目录操作示例
代码示例：
```
import org.apache.poi.xwpf.usermodel.XWPFDocument;  
import org.apache.poi.xwpf.usermodel.XWPFParagraph;  
import org.apache.poi.xwpf.usermodel.XWPFRun;  
import java.io.FileInputStream;  
import java.io.FileOutputStream;  
import java.io.IOException;  
public class TOCExample { 
    public static void main(String[] args) throws IOException { 
        FileInputStream fis = new FileInputStream("input.docx");  
        XWPFDocument doc = new XWPFDocument(fis); 
        // 假设文档中已经有标题等内容，这里简单示例创建目录相关的文本 
        XWPFParagraph tocPara = doc.createParagraph();  
        XWPFRun tocRun = tocPara.createRun();  
        tocRun.setText("Table  of Contents"); 
        tocRun.addBreak();  
        // 这里可以根据标题级别等实际情况来准确构建目录内容（比如查找标题对应的页码等） 
        // 保存修改后的文档 
        FileOutputStream fos = new FileOutputStream("output.docx");  
        doc.write(fos);  
        fos.close();  
        fis.close();  
    } 
} 
```
请注意，以上代码只是基本的示例，实际应用中可能需要根据具体的需求进行更多的完善和优化。