## Git 与 SVN 区别

![img](assets/0D32F290-80B0-4EA4-9836-CA58E22569B3.jpg)
```
- **1、Git 是分布式的，SVN 不是**：这是 Git 和其它非分布式的版本控制系统，例如 SVN，CVS 等，最核心的区别。
- **2、Git 把内容按元数据方式存储，而 SVN 是按文件：**所有的资源控制系统都是把文件的元信息隐藏在一个类似 .svn、.cvs 等的文件夹里。
- **3、Git 分支和 SVN 的分支不同：**分支在 SVN 中一点都不特别，其实它就是版本库中的另外一个目录。
- **4、Git 没有一个全局的版本号，而 SVN 有：**目前为止这是跟 SVN 相比 Git 缺少的最大的一个特征。
- **5、Git 的内容完整性要优于 SVN：**Git 的内容存储使用的是 SHA-1 哈希算法。这能确保代码内容的完整性，确保在遇到磁盘故障和网络问题时降低对版本库的破坏。
```


## Git安装与配置

### 安装

安装包下载地址：https://gitforwindows.org/

![image-20230722122612094](assets/image-20230722122612094.png)

安装过程中，保持默认选项，完成安装之后，就可以使用命令行的 git 工具（已经自带了 ssh 客户端）了，另外还有一个图形界面的 Git 项目管理工具。

在开始菜单里找到"Git"->"Git Bash"，会弹出 Git 命令窗口，你可以在该窗口进行 Git 操作。也可以直接在 Cmd中使用Git操作



### 配置

用户主目录下的 .gitconfig 文件，用来配置相应的工作环境变量。

配置个人的用户名称和电子邮件地址：

```
$ git config --global user.name "zhangshang"
$ git config --global user.email zhangshang@163.com
```

查看已有的配置信息，可以使用 git config --list 命令：

```bash
$ git config --list
http.postbuffer=2M
user.name=zhangshang
user.email=zhangshang@163.com
```



## Git 工作流程

![img](assets/git-process-16899990786807.png)



## Git 工作区、暂存区和版本库


```
- **工作区：**就是你在电脑里能看到的目录。
- **暂存区：**英文叫 stage 或 index。一般存放在 **.git** 目录下的 index 文件（.git/index）中，所以我们把暂存区有时也叫作索引（index）。
- **版本库：**工作区有一个隐藏目录 **.git**，这个不算工作区，而是 Git 的版本库。
```

![img](assets/1352126739_7909.jpg)


```
- 图中左侧为工作区，右侧为版本库。在版本库中标记为 "index" 的区域是暂存区（stage/index），标记为 "master" 的是 master 分支所代表的目录树。
- 图中我们可以看出此时 "HEAD" 实际是指向 master 分支的一个"游标"。所以图示的命令中出现 HEAD 的地方可以用 master 来替换。
- 图中的 objects 标识的区域为 Git 的对象库，实际位于 ".git/objects" 目录下，里面包含了创建的各种对象及内容。
- 当对工作区修改（或新增）的文件执行 **git add** 命令时，暂存区的目录树被更新，同时工作区修改（或新增）的文件内容被写入到对象库中的一个新的对象中，而该对象的ID被记录在暂存区的文件索引中。
- 当执行提交操作（git commit）时，暂存区的目录树写到版本库（对象库）中，master 分支会做相应的更新。即 master 指向的目录树就是提交时暂存区的目录树。
- 当执行 **git reset HEAD** 命令时，暂存区的目录树会被重写，被 master 分支指向的目录树所替换，但是工作区不受影响。
- 当执行 **git rm --cached <file>** 命令时，会直接从暂存区删除文件，工作区则不做出改变。
- 当执行 **git checkout .** 或者 **git checkout -- <file>** 命令时，会用暂存区全部或指定的文件替换工作区的文件。这个操作很危险，会清除工作区中未添加到暂存区中的改动。
- 当执行 **git checkout HEAD .** 或者 **git checkout HEAD <file>** 命令时，会用 HEAD 指向的 master 分支中的全部或者部分文件替换暂存区和以及工作区中的文件。这个命令也是极具危险性的，因为不但会清除工作区中未提交的改动，也会清除暂存区中未提交的改动。
```


## Git 基本操作

![img](assets/git-command.jpg)

Git 常用的是以下 6 个命令：**git clone**、**git push**、**git add** 、**git commit**、**git checkout**、**git pull**



**说明：**

- workspace：工作区
- staging area：暂存区/缓存区
- local repository：版本库或本地仓库
- remote repository：远程仓库



### 创建仓库

在本地创建一个文件夹，然后在文件夹中运行以下命令：

```
$ mkdir my-repo
$ cd my-repo
$ git init
```

### 添加文件到仓库

在仓库中添加一个文件：

```
$ echo "Hello, World!" > README.md
$ git add README.md
```

### 提交文件

提交文件并生成一个新的提交记录：

```
$ git commit -m "Initial commit"
```

### 查看提交记录

查看提交记录：

```
$ git log
```



## Git命令速查

![img](assets/011500266295799.jpg)