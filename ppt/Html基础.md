# HTML 简介

文本标记语言（英语：HyperText Markup Language，简称：HTML）是一种用于创建网页的标准标记语言。

您可以使用 HTML 来建立自己的 WEB 站点，HTML 运行在浏览器上，由浏览器来解析。

- HTML 不是一种编程语言，而是一种**标记**语言
- 标记语言是一套**标记标签** (markup tag)
- HTML 使用标记标签来**描述**网页
- HTML 文档包含了HTML **标签**及**文本**内容
- 

```html
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>我的第一张测试网页</title>
</head>
<body>
 
<h1>我的第一个标题</h1>
 
<p>我的第一个段落。</p>
 
</body>
</html>
```

![image-20230722181327942](assets/image-20230722181327942.png)



# HTML 编辑器

推荐使用 VS Code

[Visual Studio Code - Code Editing. Redefined](https://code.visualstudio.com/)





# HTML 元素

HTML 文档由 HTML 元素定义。



开始标签常被称为**起始标签（opening tag）**，结束标签常称为**闭合标签（closing tag）**



## 元素语法

- HTML 元素以**开始标签**起始
- HTML 元素以**结束标签**终止
- **元素的内容**是开始标签与结束标签之间的内容
- 某些 HTML 元素具有**空内容（empty content）**
- 空元素**在开始标签中进行关闭**（以开始标签的结束而结束）
- 大多数 HTML 元素可拥有**属性**

如：以下是一个段落元素

```
<p>我的第一个段落。</p>
```



## HTML 属性

- HTML 元素可以设置**属性**
- 属性可以在元素中添加**附加信息**
- 属性一般描述于**开始标签**
- 属性总是以名称/值对的形式出现，**比如：name="value"**。