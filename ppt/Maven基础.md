# 基础概念

Maven 翻译为"专家"、"内行"，是 Apache 下的一个纯 Java 开发的开源项目。基于项目对象模型（缩写：POM）概念，Maven利用一个中央信息片断能管理一个项目的构建、报告和文档等步骤。

Maven 是一个项目管理工具，可以对 Java 项目进行构建、依赖管理。



# 安装与配置

## 安装

Maven基于JDK，这里假定你已经安装好了JDK1.8



Maven 下载地址：http://maven.apache.org/download.cgi



![image-20230722123216286](assets/image-20230722123216286.png)



## 配置

右键 "此电脑"，选择 "属性"，之后点击 "高级系统设置"，点击"环境变量"，来设置环境变量

新建一个环境变量 MAVEN_HOME

![image-20230722123712526](assets/image-20230722123712526.png)

修改环境变量 Path，添加一个 `%MAVEN_HOME%\bin` 项目

![image-20230722123758897](assets/image-20230722123758897.png)



# Maven POM

POM( Project Object Model，项目对象模型 ) 是 Maven 工程的基本工作单元，是一个XML文件，包含了项目的基本信息，用于描述项目如何构建，声明项目依赖，等等。

执行任务或目标时，Maven 会在当前目录中查找 POM。它读取 POM，获取所需的配置信息，然后执行目标。

POM 中可以指定以下配置：

- 项目依赖
- 插件
- 执行目标
- 项目构建 profile
- 项目版本
- 项目开发者列表
- 相关邮件列表信息



父（Super）POM是 Maven 默认的 POM。所有的 POM 都继承自一个父 POM（无论是否显式定义了这个父 POM）。父 POM 包含了一些可以被继承的默认设置

Maven 使用 effective pom（Super pom 加上工程自己的配置）来执行相关的目标，它帮助开发者在 pom.xml 中做尽可能少的配置，当然这些配置可以被重写。

使用以下命令来查看 Super POM 默认配置：

```
mvn help:effective-pom
```



# Maven 构建生命周期

![img](assets/maven-package-build-phase.png)

| 阶段          | 处理     | 描述                                                     |
| :------------ | :------- | :------------------------------------------------------- |
| 验证 validate | 验证项目 | 验证项目是否正确且所有必须信息是可用的                   |
| 编译 compile  | 执行编译 | 源代码编译在此阶段完成                                   |
| 测试 Test     | 测试     | 使用适当的单元测试框架（例如JUnit）运行测试。            |
| 包装 package  | 打包     | 创建JAR/WAR包如在 pom.xml 中定义提及的包                 |
| 检查 verify   | 检查     | 对集成测试的结果进行检查，以保证质量达标                 |
| 安装 install  | 安装     | 安装打包的项目到本地仓库，以供其他项目使用               |
| 部署 deploy   | 部署     | 拷贝最终的工程包到远程仓库中，以共享给其他开发人员和工程 |



**生命周期**
maven的生命周期是抽象的，它本身不做任何实际的工作。实际的工作都由插件来完成。生命周期好比接口，插件好比实现类。
maven 有三个独立的生命周期，clean、default、site。



**命周期阶段**

每个生命周期包含一些生命周期阶段，这些阶段是有顺序的，后面的阶段依赖于前面的阶段。
clean生命周期包含的阶段：pre-clean、clean、post-clean

default生命周期包含的阶段：
1 process-sources、2 compile、3 process-test-sources、4 test-compile、5 test、6 package、 verify、8 install、9 deploy
1 处理主资源文件、2 编译主代码、3 处理测试资源文件、4 编译测试代码、5 运行测试、6 将编译好的代码打包、---- 、8 将包安装到本地仓库、9 将包安装到远程仓库

site生命周期包含的阶段：
pre-site、site(生成项目站点文档)、post-site、site-deploy(把生成的项目站点发布到服务器上)

生命周期阶段：mvn clean package ,表示执行生命周期阶段clean、package。
执行顺序：clean --> resources --> compile --> testResources --> testCompile --> test --> jar --> shade --> install
clear --> 清理target目录
resources --> 处理主资源
compile --> 编译主代码至target/classes目录
testResources --> 处理测试资源
testCompile --> 编译测试代码
test --> 测试
jar --> 打包到target目录下
shade --> 给hello-world-1.0-SNAPSHOT\META-INF\MANIFEST.MF 目录里添加Main-Class信息，如：Main-Class: com.juvenxu.mvnbook.helloworld.HelloWorld
install --> 把没有Main-Class信息的包添加到本地仓库。



![img](assets/v2-0b661424f03e596bbbcc5ebec7ab788f_r.jpg)

![img](assets/u=2679618276,646881780&fm=253&fmt=auto&app=138&f=PNG.png)

![img](assets/28843cda50d86b79595a2cb71161704d.png)

![img](assets/u=3907798195,3523202087&fm=253&fmt=auto&app=138&f=JPEG.jpeg)

# Maven 仓库

Maven 仓库有三种类型：

- 本地（local） 用户目录下都有一个路径名为 .m2/repository/ 的仓库目录
- 中央（central）Maven 中央仓库是由 Maven 社区提供的仓库，不需要配置，通过网络才能访问
- 远程（remote）开发人员自己定制仓库（私服）

## Maven 依赖搜索顺序

当我们执行 Maven 构建命令时，Maven 开始按照以下顺序查找依赖的库：

- **步骤 1** － 在本地仓库中搜索，如果找不到，执行步骤 2，如果找到了则执行其他操作。
- **步骤 2** － 在中央仓库中搜索，如果找不到，并且有一个或多个远程仓库已经设置，则执行步骤 4，如果找到了则下载到本地仓库中以备将来引用。
- **步骤 3** － 如果远程仓库没有被设置，Maven 将简单的停滞处理并抛出错误（无法找到依赖的文件）。
- **步骤 4** － 在一个或多个远程仓库中搜索依赖的文件，如果找到则下载到本地仓库以备将来引用，否则 Maven 将停止处理并抛出错误（无法找到依赖的文件）。



Maven 仓库默认在国外， 国内使用难免很慢，我们可以更换为阿里云的仓库。

修改 maven 根目录下的 conf 文件夹中的 settings.xml 文件，在 mirrors 节点上，添加内容如下：

```
<mirror>
  <id>aliyunmaven</id>
  <mirrorOf>*</mirrorOf>
  <name>阿里云公共仓库</name>
  <url>https://maven.aliyun.com/repository/public</url>
</mirror>
```



# Maven 构建 Java 项目

要创建一个简单的 Java 应用，我们将使用 **maven-archetype-quickstart** 插件。



```
E:>cd E:\temp
E:\temp>mvn archetype:generate "-DgroupId=com.demo.book" "-DartifactId=book" "-DarchetypeArtifactId=maven-archetype-quickstart" "-DinteractiveMode=false"
```

参数说明：

- **-DgroupId**: 组织名，公司网址的反写 + 项目名称
- **-DartifactId**: 项目名-模块名
- **-DarchetypeArtifactId**: 指定 ArchetypeId，maven-archetype-quickstart，创建一个简单的 Java 应用
- **-DinteractiveMode**: 是否使用交互模式



![image-20230722175405953](assets/image-20230722175405953.png)



pom.xml

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.demo.book</groupId>
  <artifactId>book</artifactId>
  <packaging>jar</packaging>
  <version>1.0-SNAPSHOT</version>
  <name>book</name>
  <url>http://maven.apache.org</url>
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
</project>
```



App.java

```java
package com.demo.book;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
```



AppTest.java

```java
package com.demo.book;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
}
```



# Maven 构建 & 项目测试

```
E:\temp>cd E:\temp\book
E:\temp\book>mvn clean package
[INFO] Scanning for projects...
[INFO]
[INFO] -------------------------< com.demo.book:book >-------------------------
[INFO] Building book 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ book ---
[INFO]
......
[INFO]
[INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ book ---
[INFO] Surefire report directory: E:\temp\book\target\surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running com.demo.book.AppTest
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.004 sec

Results :

Tests run: 1, Failures: 0, Errors: 0, Skipped: 0

[INFO]
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ book ---
[INFO] Building jar: E:\temp\book\target\book-1.0-SNAPSHOT.jar
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  2.225 s
[INFO] Finished at: 2023-07-22T17:58:54+08:00
[INFO] ------------------------------------------------------------------------
```



# Maven 快照(SNAPSHOT)

快照是一种特殊的版本，指定了某个当前的开发进度的副本。不同于常规的版本，Maven 每次构建都会在远程仓库中检查新的快照。 



Maven 在日常工作中会自动获取最新的快照， 你也可以在任何 maven 命令中使用 -U 参数强制 maven 下载最新的快照构建。

```
mvn clean package -U
```