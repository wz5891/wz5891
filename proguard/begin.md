https://www.guardsquare.com/manual/home



https://github.com/Guardsquare/proguard



https://blog.csdn.net/ThinkPet/article/details/131196712



https://www.cnblogs.com/codhome/p/13621169.html
(service也可混淆)

主要的坑,springboot项目配置注意项#
启动类不能混淆
```
//混淆会把Bean的名称重复，这里要求SpringBoot生成唯一的BeanName
@SpringBootApplication
@MapperScan("com.dsys.project.dao")
@ComponentScan("com.dsys")
@ServletComponentScan("com.dsys")
@EnableCaching
@EnableAsync
public class App {
    public static class CustomGenerator implements BeanNameGenerator {

        @Override
        public String generateBeanName(BeanDefinition definition, BeanDefinitionRegistry registry) {
            return definition.getBeanClassName();
        }
    }

    /***
     * 由于proguard混淆貌似不能指定在basePackages下面类名混淆后唯一，不同包名经常有a.class，b.class,c.class之类重复的类名，因此spring容器初始化bean的时候会报错。
     *庆幸的是，我们可以通过改变spring的bean的命名策略来解决这个问题，把包名带上，就唯一了
     * @param args
     */
    public static void main(String[] args) {
        new SpringApplicationBuilder(App.class)
                .beanNameGenerator(new CustomGenerator())
                .run(args);
    }
}
```


https://blog.csdn.net/weixin_46654114/article/details/136694198
多模块

你需要在你的启动类模块的pom.xml文件中添加如下配置,
```
<assembly>
                        <inclusions>
                            <inclusion>
                                <groupId>com.chen</groupId>
                                <artifactId>chen-system</artifactId>
                            </inclusion>
                            <inclusion>
                                <groupId>com.chen</groupId>
                                <artifactId>chen-common</artifactId>
                            </inclusion>
                        </inclusions>
                    </assembly>
                </configuration>
```



https://wvengen.github.io/proguard-maven-plugin/


https://blog.csdn.net/qq_34250494/article/details/122987426



