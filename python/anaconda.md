
# 安装

环境变量设置
右击此电脑—属性—高级系统—环境变量—系统变量—path—编辑—新建
```
D:\anaconda
D:\anaconda\Scripts\
D:\anaconda\Library\bin
D:\anaconda\Library\mingw-w64\bin
```


```
conda --version
conda info
```


## conda配置

查看配置
```
conda config --show
```

### 修改路径
conda创建虚拟环境的默认路径为 C:\Users\username\.conda\envs\
conda安装包的默认路径为 C:\Users\username\.conda\pkgs\
若不想占用C盘空间，需要修改 conda 虚拟环境的默认路径 和 安装包的默认路径


打开 C:\Users(用户)\username\ .condarc 文件
```
envs_dirs:
  - D:\Anaconda3\envs
pkgs_dirs:
  - D:\Anaconda3\pkgs
```

### 增加国内镜像
```
channels:
  - http://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge/
  - http://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
  - http://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
show_channel_urls: true
```

## 虚拟环境
查看虚拟环境
```
conda env list
```

创建虚拟环境
```
conda create -n yolov5
# 含 jupyter notebook
conda create -n yolov5 jupyter notebook
conda create -n yolov5 jupyter notebook python=3.8.2
```

切换虚拟环境
```
activate yolov5
```

在虚拟环境yolov5中运行pip install，相关的包将安装在 `envs_dirs\yolov5\lib\site-packages` 中。(区分不同的虚拟环境)



## conda env base 与其它的关系
conda env base 是 conda 环境管理器中的一个特殊环境，它是 conda 环境管理的基础环境。它是所有新创建的 conda 环境的父环境，这些环境继承了 base 环境中的包和库。

如果你尝试删除 base 环境，conda 会给出一个警告，因为这样做可能会破坏 conda 的安装，并可能导致 conda 无法正常工作。

如果你想要创建一个新的环境，你可以使用以下命令：
```
conda create --name myenv
```
在这个命令中，myenv 是新环境的名称。这个新环境将会从 base 环境继承所有的包和库。

## 其它命令
```
conda list：查看环境中的所有包
conda install XXX：安装 XXX 包
conda remove XXX：删除 XXX 包
conda env list：列出所有环境
conda create -n XXX：创建名为 XXX 的环境
conda create -n env_name jupyter notebook ：创建虚拟环境
activate noti（或 source activate noti）：启用/激活环境
conda env remove -n noti：删除指定环境
deactivate（或 source deactivate）：退出环境
jupyter notebook ：打开Jupyter Notebook
conda config --remove-key channels ：换回默认源


conda -V #查看是否安装成功和版本
conda config --show #查看相关配置
conda env list #查看环境列表
conda create -n your_env_name python=x.x #创建新环境
conda remove -p your_env_name #根据环境名删除环境
conda remove -p your_env_path #根据环境路径删除环境
conda activate your_env_name --all #激活环境
conda deactivate your_env_name #退出环境
```