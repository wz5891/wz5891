查看 pip 当前安装位置
```
pip show pip

pip config list
```

修改安装位置
编辑 `用户目录/pip/pip.ini`文件的内容
```
[global]
target = /path/to/custom/location
```




国内的pip源
```
阿里云：https://mirrors.aliyun.com/pypi/simple/
清华：https://pypi.tuna.tsinghua.edu.cn/simple
中国科技大学 https://pypi.mirrors.ustc.edu.cn/simple/
华中理工大学：http://pypi.hustunique.com/
山东理工大学：http://pypi.sdutlinux.org/
豆瓣：http://pypi.douban.com/simple/
```

Linux 环境下替换镜像源：
```
mkdir -p ~/.pip
touch  ~/.pip/pip.conf
gedit ~/.pip/pip.conf # 拷贝下面内容进入
```

```
[global]
timeout = 6000
index-url = https://mirrors.aliyun.com/pypi/simple/
trusted-host = mirrors.aliyun.com
```