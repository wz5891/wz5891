1. 准备好swagger-ui的链接，如：

https://asset.waketzheng.top/swagger-ui.css

https://asset.waketzheng.top/swagger-ui-bundle.js

2. 安装插件fastapi-cdn-host（只适用0.100以上版本的fastapi，旧版本请自行参考官网示例）

pip install fastapi-cdn-host
3. 启用插件

import fastapi_cdn_host
from fastapi import FastAPI
 
app = FastAPI()
fastapi_cdn_host.patch_docs(
    app,
    CdnHostItem('https://asset.waketzheng.top/swagger-ui.css').export(),
)
