打开“Anaconda Prompt (miniconda3)”，进入命令行界面。首先我们需要将 pip 升级到最新版本，输入以下命令：

```
pip3 install --upgrade pip
```
接下来安装 Jupyter Notebook：
```
pip3 install jupyter
```
安装完成后，在命令行内输入 `jupyter notebook` ，系统就会自动在浏览器里打开 Jupyter Notebook。



Jupyter Notebook 配置
初次打开 Jupyter Notebook 时，会默认显示 home 文件夹，即 Windows 系统中的 C:\Users\<你的用户名> 位置。

但我需要将其默认打开的文件夹更改为 D 盘中的 Projects 文件夹，所以需要对 Jupyter Notebook 作一定的配置。

首先我们需要生成 Jupyter Notebook 的配置文件，在命令行界面中输入：
```
jupyter notebook --generate-config
```
该配置文件会以 .py 的格式（Python 文件）生成在 C:\Users\<你的用户名>\.jupyter\ 中。找到该文件并使用文本编辑器（比如 VS Code 或记事本）打开它。

然后在配置文件中新加一行代码即可：
```
c.NotebookApp.notebook_dir = "D:\\Projects"
```
将上述代码字符串里的 `D:\\Projects` 替换为你设置的默认打开文件夹的地址即可。