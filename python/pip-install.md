## pip install
pip install -e .粗浅理解+白话分析
主要是记一下有关pip install -e .的事情，一直没有理解清楚，ChatGPT也解释的很官方。直白来说这个命令是我在开发某一个库的时候使用的，可以让对代码的修改实时生效

为什么要有这个命令呢？其实是为了让正在开发的某个包的修改能够直接生效而无需重复安装。比如我在开发module1，然后我在另一个项目 project1 （或者module1 项目中的测试代码）中使用到了module1，按道理是此时的module1只是一段代码不是一个像torch, os 一样的包，是没法直接导入的。但这时候只需要在module1那里 pip install -e . 后，首先把module1 当成了一个包装到了环境中，然后后续对module1 的改动都可以直接起作用而不用重新安装这个包。

pip install . ：安装后的模块freeze在pip/conda依赖下，换句话说，再修改本地的原项目文件，不会导致对应模块发生变化。
pip install -e .：-e 理解为 editable，修改本地文件，调用的模块以最新文件为准。



## pip install 与 conda install 的使用区别

[支持语言]：

pip 是 python 官方推荐的包下载工具，但是只能安装python包
conda 是一个跨平台（支持linux, mac, win）的通用包和环境管理器，它除了支持python外，还能安装各种其他语言的包，例如 C/C++, R语言等
 
[Repo源]：

pip 从PyPI（Python Package Index）上拉取数据。上面的数据更新更及时，涵盖的内容也更加全面
conda 从 Anaconda.org 上拉取数据。虽然Anaconda上有一些主流Python包，但在数量级上明显少于PyPI，缺少一些小众的包
 
[包的内容]：

pip 里的软件包为wheel版或源代码发行版。wheel属于已编译发新版的一种，下载好后可以直接使用；而源代码发行版必须要经过编译生成可执行程序后才能使用，编译的过程是在用户的机子上进行的
conda 里的软件包都是二进制文件，下载后即可使用，不需要经过编译
 
[环境隔离]：

pip 没有内置支持环境隔离，只能借助其他工具例如virtualenv or venv实现环境隔离
conda 有能力直接创建隔离的环境
 
[依赖关系]：

pip安装包时，尽管也对当前包的依赖做检查，但是并不保证当前环境的所有包的所有依赖关系都同时满足。当某个环境所安装的包越来越多，产生冲突的可能性就越来越大。
conda会检查当前环境下所有包之间的依赖关系，保证当前环境里的所有包的所有依赖都会被满足
 
[库的储存位置]：

在conda虚拟环境下使用 pip install 安装的库： 如果使用系统的的python，则库会被保存在 ~/.local/lib/python3.x/site-packages 文件夹中；如果使用的是conda内置的python，则会被保存到 anaconda3/envs/current_env/lib/site-packages中
conda install 安装的库都会放在anaconda3/pkgs目录下。这样的好处就是，当在某个环境下已经下载好了某个库，再在另一个环境中还需要这个库时，就可以直接从pkgs目录下将该库复制至新环境而不用重复下载



推荐使用conda创建虚拟环境，能用conda安装的就先用conda，不行再使用pip安装。