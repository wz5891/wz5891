RabbitMQ 延迟高启动慢连接超时问题解决

 本文详细解析了虚拟机hostname配置不正确导致的RabbitMQ性能问题，包括连接速度变慢和高延迟现象。通过修改hostname并将其正确指向localhost，有效解决了这一问题，提升了虚拟机与RabbitMQ的运行效率。
摘要由CSDN通过智能技术生成
虚拟机的hostname 没有正确配置会导致虚拟机的连接速度变慢 , 在RabbitMQ中格外明显 , 会导致其延迟极高
查看hostname
```
hostname
bestksl
```

```
vim /etc/sysconfig/network

NETWORKING=yes
HOSTNAME=bestksl
```

打开 hosts文件

将hostname 指向 localhost

问题解决!
