```
spring:
  # rabbitmq配置信息
  rabbitmq:
    listener:
      type: direct
      direct:
        acknowledge-mode: manual
        consumers-per-queue: 1
        prefetch: 3
```