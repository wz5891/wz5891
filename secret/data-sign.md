## 数字签名PDF文档如何验证

http://yz.cnca.cn/ver-ep/ve/v/kn/ca_what.jsp?id=9&title=%E6%95%B0%E5%AD%97%E7%AD%BE%E5%90%8DPDF%E6%96%87%E6%A1%A3%E5%A6%82%E4%BD%95%E9%AA%8C%E8%AF%81


大多数现代 PDF 查看器（如 Adobe Acrobat Reader DC、Foxit Reader 等）内置了对数字签名的支持，可以直接显示签名状态和相关信息。用户只需打开带有数字签名的 PDF 文件，查看器会自动检测并提示签名的有效性。
