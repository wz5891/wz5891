通过openssl生成pfx证书

通过centos7上自带的openssl工具来生成。首先创建一个pfxcert目录。然后进入此目录。
## 1.生成.key文件（内含被加密后的私钥），要求输入一个自定义的密码
```
[root@localhost cert]# openssl genrsa -des3 -out server.key 2048
Generating RSA private key, 2048 bit long modulus
…+++
…+++
e is 65537 (0x10001)
Enter pass phrase for server.key: （此处需要键入自定义的密码）
Verifying - Enter pass phrase for server.key:（此处需要键入上面的密码，其确认作用）
```

-des3：对私钥进行加密的算法，key文件中的私钥是被对称加密过的。再解密时，获取对称加密密钥和口令，盐值，迭代次数
有关，根据这些推算出来，但这里的口令不是指的加密密钥。

## 2.生成.crt文件(公钥证书，这里没有先显示的生成csr文件，然后再根据csr文件生成crt文件)
```
[root@localhost cert]# openssl req -utf8 -new -x509 -key server.key -out server.crt -days 18250
Enter pass phrase for server.key: *****（此处需要键入key文件的密码）
You are about to be asked to enter information that will be incorporated
into your certificate request.//表明下面键入的信息会存入csr中，进而根据这些信息创建证书
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter ‘.’, the field will be left blank.
-----
Country Name (2 letter code) [XX]:CN
State or Province Name (full name) []:Beijing
Locality Name (eg, city) [Default City]:Beijing
Organization Name (eg, company) [Default Company Ltd]:Shihua
Organizational Unit Name (eg, section) []:Shihua
Common Name (eg, your name or your server’s hostname) []:Shihua
Email Address []:12345@qq.com
```
## 3、生成.pfx文件(内含私钥和公钥证书)
```
[root@localhost cert]# openssl pkcs12 -export -out server.pfx -inkey server.key -in server.crt
Enter pass phrase for server.key: *****（此处需要键入key文件的密码）
Enter Export Password: （此处需要键入自定义的导入pfx证书的密码，最好设置防止他人窃取使用我们的私钥）
Verifying - Enter Export Password:（对上方密码的确认）

```
注意：这里的 Export Password密码和key文件的密码不是一回事。
Export Password密码是来保护我们的pfx文件的。
server.key的密码是用来访问其里面的私钥的。
个人感觉使用pfx文件中的私钥时，不用再输入key文件对应的口令了，所以这里在使用pfx文件时，只需要使用ExportPassword密码就行，不需要再使用server.key的密码了。

pfx文件在项目中的使用配置：

server.ssl.key-store=classpath:server.pfx
server.ssl.key-store-password=admin123#    //这里的密码就是设置的Export

Password。这里的私钥一般我们是导不出来的，只能导出公钥证书。
server.ssl.keyStoreType=PKCS12
