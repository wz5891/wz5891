在 Spring Boot 应用程序中，可以通过命令行参数或环境变量来指定配置文件和日志文件的位置
1、使用命令行参数
```
java -jar your-app.jar --spring.config.location=file:/path/to/application.properties --logging.file=/path/to/logs/your-app.log
```
在上述命令中，`--spring.config.location`` 参数用于指定配置文件的位置`，`--logging.file` 参数用于指定日志文件的位置。

2、使用环境变量
```
export SPRING_CONFIG_LOCATION=file:/path/to/application.properties
export LOGGING_FILE=/path/to/logs/your-app.log
java -jar your-app.jar
```
通过设置  `SPRING_CONFIG_LOCATION` 环境变量来指定配置文件的位置，通过设置 `LOGGING_FILE`` 环境变量来指定日志文件的位置。

3、在应用程序的配置文件中指定

在 `application.properties` 或 `application.yml` 配置文件中，可以直接指定配置文件和日志文件的位置。例如，在 `application.properties` 中：
```
spring.config.location=file:/path/to/application.properties
logging.file=/path/to/logs/your-app.log
```
以上是几种常见的指定配置文件和日志文件位置的方式，你可以根据实际情况选择适合你的方式。在 Kubernetes 部署中，你可以使用上述方式之一，并将配置文件和日志文件挂载到容器中，以便在容器中访问和使用它们。

