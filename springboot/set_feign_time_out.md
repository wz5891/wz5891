
springboot中这样设置feign请求超时时间：
```
feign:
  client:
    config:
      default:
        connectTimeout: 10000 #毫秒
        readTimeout: 10000 #毫秒
```


参考：https://zhuanlan.zhihu.com/p/650025543