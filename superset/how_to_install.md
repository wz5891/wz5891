Docker安装Superset流程

## 1. 首先获取镜像
```
docker pull amancevice/superset
```
   
   

## 2. 生成SSL
接下来我们运行一些额外的程序：
```
openssl rand -base64 42
```
生成一串复杂的字符串：nzUP0CMXcCaQI+fQ2XZIJLp1aQJLBA/bJXk/dZQFtMeicMHe/5btzRus

## 3. 创建Superset容器
创建容器，并且指定用户密码：
```
docker run -d --name superset -p 8088:8088 -e SUPERSET_SECRET_KEY="nzUP0CMXcCaQI+fQ2XZIJLp1aQJLBA/bJXk/dZQFtMeicMHe/5btzRus" amancevice/superset
docker exec -it superset superset-init
```
用户密码这里可以使用：回车、回车，输入两次密码；比如设置密码为admin

```
Username [admin]: User first name [admin]:
User last name [user]:
Email [admin@fab.org]:
Password:
Repeat for confirmation:
logging was configured successfully
2023-11-14 07:12:59,565:INFO:superset.utils.logging_configurator:logging was configured successfully
2023-11-14 07:12:59,570:INFO:root:Configured event logger of type <class 'superset.utils.log.DBEventLogger'>
xxxxxx
```

注意：如果出现如下提示（A Default SECRET_KEY was detected, please use superset_config.py to override it.），则说明没有按照博客写的第二步进行，修改方法非常繁琐，最好重头再来，如果需要修改，请参考github的issue


## 4. 更新数据库
运行如下代码：
```
docker exec -it superset superset db upgrade # 更新数据库
```

## 5. 测试访问Superset
大功告成，输入：http://127.0.0.1:8088/，输入用户密码admin，即可进入Superset的页面