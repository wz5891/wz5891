## 软件与网络
参见：./qemu-setup.md


## 启动ArchLinux
启动命令
```
qemu-system-loongarch64 -m 4G -smp 4 --cpu la464 --machine virt -bios QEMU_EFI_7.2.fd -net nic -net tap,ifname=tap0 -serial stdio -device virtio-gpu-pci -device nec-usb-xhci,id=xhci,addr=0x1b -device usb-tablet,id=tablet,bus=xhci.0,port=1 -device usb-kbd,id=keyboard,bus=xhci.0,port=2 -device virtio-blk-pci,drive=test -drive id=test,file=archlinux-mate-2024.04.23-loong64.qcow2,if=none
```

登录
loogarch / loogarch


启动ssh
```
su root
systemctl start sshd
```

## 安装Docker
```
pacman -S docker
pacman -S docker-compose
systemctl start docker
systemctl enable docker
```
## 参考
小白也能懂之如何在自己的Windows电脑上使用QEMU虚拟机启动龙芯Loongnix系统的操作办法
https://www.modb.pro/db/1733672145120206848

基于 Windows 平台利用 QEMU 部署 ARM 架构的虚拟机
https://blog.csdn.net/clancy_pinkie/article/details/135250263





https://bbs.loongarch.org/d/248-loongnix-docker


https://mirrors.wsyu.edu.cn/loongarch/archlinux/images/


https://segmentfault.com/a/1190000041356982?utm_source=sf-similar-article



openEuler 22.03 x86架构下docker运行arm等架构的容器——筑梦之路（含 loongarch 架构的支持）

https://blog.csdn.net/qq_34777982/article/details/134056598


Loong Arch Linux 虚拟机运行
https://loongarchlinux.org/pages/vmrun/


Loong Arch Linux 用户论坛
https://bbs.loongarch.org/?q=docker



.修改镜像容器地址，可在守护进程配置中修改

vi /etc/docker/daemon.json

{
  "registry-mirrors": ["https://cr.loongnix.cn"]
}
##########################################################################################
https://bbs.loongarch.org/d/421-x86-loong64

```
LCPU 维护了 qemu-user-static 容器，可以模拟运行 loong64 架构的程序。具体脚本如下：

docker run --rm --privileged loongcr.lcpu.dev/multiarch/archlinux --reset -p yes

# 运行 loong64 容器

docker run --rm -it --name hello loongcr.lcpu.dev/lcpu/debian bash


docker pull --rm -it --name hello cr.loongnix.cn/library/centos:8.3
```
##########################################################################################

https://github.com/lcpu-club/loong64-dockerfiles

loong-dockerfiles
Dockerfiles for LoongArch (new-world).

How to run loong64 containers on x86 platform
We maintain a qemu-user-static container, which will act as the simulator. Follow the instructions below:

# Register qemu-user-static 
docker run --rm --privileged loongcr.lcpu.dev/multiarch/archlinux --reset -p yes
# Run loong64 containers
docker run --rm -it --name hello loongcr.lcpu.dev/lcpu/debian:unstable-240513 bash