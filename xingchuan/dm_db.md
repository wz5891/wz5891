## 官网
https://eco.dameng.com/download/

仅供开发者用于学习、测试、开发用途，1 年免费试用期。


https://eco.dameng.com/document/dm/zh-cn/start/dm-version-differences.html

根据不同的应用需求与配置， DM 提供了多种不同的产品系列：

DM Standard Edition 标准版
DM 标准版是为政府部门、中小型企业及互联网/内部网应用提供的数据管理和分析平台。它拥有数据库管理、安全管理、开发支持等所需的基本功能，支持多用户并发访问等。该版本以其前所未有的易用性和高性价比，为政府或企业提供支持其操作所需的基本能力，并能够根据用户需求完美升级到企业版。

DM Enterprise Edition 企业版
DM 企业版是伸缩性良好、功能齐全的数据库，无论是用于驱动网站、打包应用程序，还是联机事务处理、决策分析或数据仓库应用， DM 企业版都能作为专业的服务平台。DM 企业版支持多 CPU，支持 PB 级海量数据存储和大量的并发用户，并为高端应用提供了数据复制、数据守护等高可靠性、高性能的数据管理能力，完全能够支撑各类企业应用。

DM Security Edition 安全版
DM 安全版拥有企业版的所有功能，并重点加强了其安全特性，引入强制访问控制功能，采用数据库管理员 (DBA)、数据库审计员 (AUDITOR)、数据库安全员 (SSO)、数据库对象操作员 (DBO) 四权分立安全机制，支持 KERBEROS、操作系统用户等多种身份鉴别与验证，支持透明、半透明等存储加密方式以及审计控制、通信加密等辅助安全手段，使 DM 安全级别达到 B1 级，适合于对安全性要求更高的政府或企业敏感部门选用。

在线体验
https://eco.dameng.com/tour/


## Docker 安装 达梦数据库
https://eco.dameng.com/document/dm/zh-cn/start/dm-install-docker.html


镜像导入后，使用 docker run 启动容器，启动命令如下：

```
docker run -d -p 30236:5236 --restart=always --name=dm8_test --privileged=true -e LD_LIBRARY_PATH=/opt/dmdbms/bin -e PAGE_SIZE=16 -e EXTENT_SIZE=32 -e LOG_SIZE=1024 -e UNICODE_FLAG=1  -e INSTANCE_NAME=dm8_test -v /opt/data:/opt/dmdbms/data dm8:dm8_20240422_rev215128_x86_rh6_64
image.png
```


```
version: '3'
services:
  dm8:
    restart: always
    privileged: true
    #network_mode: host
    image: dm8_single:dm8_20240715_rev232765_x86_rh6_64
    container_name: dm8
    volumes:
      - ./data/db:/opt/dmdbms/data
      - ./data/log:/opt/dmdbms/log
    environment:
      LD_LIBRARY_PATH: /opt/dmdbms/data
      PAGE_SIZE: 16
      EXTENT_SIZE: 32
      LOG_SIZE: 1024
      UNICODE_FLAG: 1
      INSTANCE_NAME: dm8
      CASE_SENSITIVE: 0
      SYSDBA_PWD: 123456789
    ports:
      - 5236:5236
networks:
  default:
    name: mydocker_net
    external: true
```

容器运行相关参数说明：

参数名	参数描述
-d	-detach 的简写，在后台运行容器，并且打印容器 id。
-p	指定容器端口映射，比如 -p 30236:5236 是将容器里数据库的 5236 端口映射到宿主机 30236 端口，外部就可以通过宿主机 ip 和 30236 端口访问容器里的数据库服务。
--restart	指定容器的重启策略，默认为 always，表示在容器退出时总是重启容器。
--name	指定容器的名称。
--privileged	指定容器是否在特权模式下运行。
-v	指定在容器创建的时候将宿主机目录挂载到容器内目录，默认为/home/mnt/disks
使用 -e 命令指定数据库初始化参数时，需要注意的是目前只支持预设以下九个 DM 参数。

参数名	参数描述	备注
PAGE_SIZE	页大小，可选值 4/8/16/32，默认值：8	设置后不可修改
EXTENT_SIZE	簇大小，可选值 16/32/64，默认值：16	设置后不可修改
CASE_SENSITIVE	1:大小写敏感；0：大小写不敏感，默认值：1	设置后不可修改
UNICODE_FLAG	字符集选项；0:GB18030;1:UTF-8;2:EUC-KR，默认值：0	设置后不可修改
INSTANCE_NAME	初始化数据库实例名字，默认值：DAMENG	可修改
SYSDBA_PWD	初始化实例时设置 SYSDBA 的密码，默认值：SYSDBA001	可修改
BLANK_PAD_MODE	空格填充模式，默认值：0	设置后不可修改
LOG_SIZE	日志文件大小，单位为：M，默认值：256	可修改
BUFFER	系统缓存大小，单位为：M，默认值：1000	可修改
注意
1.SYSDBA_PWD 预设的时候，密码长度为 9~48 个字符，docker 版本使用暂不支持特殊字符为密码。
2.-e 设置的时候 初始化参数必须使用大写，不可使用小写。

通过以下命令可以查看 Docker 镜像中数据库初始化的参数。

Copy
docker inspect dm8_test



## DM达梦数据库的-key、授权和许可证的问题

其实上述三种表述都是同一类问题，如何使用 DM 数据库的试用版或者正式版的问题。

问题一：Key 文件去哪获取。
可以联系达梦商务，电话：400 991 6599 转 1 号商务线。

问题二：没有 Key 有功能限制吗？
只有连接数和试用期限限制，其他功能没有限制。

问题三：试用安装数据库的时候报错：file dm.key no found,use default license!。
可以忽略该报错，但当前版本为试用版，试用期一年。


license的安装，见 达梦安装文件  dmdbms/doc/DM8安装手册.pdf

## 迁移指南
https://eco.dameng.com/document/dm/zh-cn/start/tool-dm-migrate.html



## nested exception is dm.jdbc.driver.DMException: 字符串截断

```
### Cause: dm.jdbc.driver.DMException: 字符串截断
; 字符串截断; nested exception is dm.jdbc.driver.DMException: 字符串截断
```

字段长度不够，才会触发这样的报错。

汉字长度 17，而数据库"TITLE" VARCHAR(50)，从通常的理解应该是可以放下的，那么查阅达梦数据库文档得知

MySQL 中 varchar(1) 可以存一个汉字，DM 数据库是以字节为单位。gb18030 字符集，varchar(2) 才可以存一个汉字；UTF-8 字符集，varchar(3) 才可以存一个汉字。此种情况下，为了保证汉字可以完整的被存储，扩大字段是合理的。



## 自增
```
SELECT @@IDENTITY
```
https://eco.dameng.com/community/question/59f104f4518c987e485a48abcda84cd1


## 备份
https://blog.csdn.net/qq_37358909/article/details/126505330

https://www.modb.pro/db/34529



## select .. order by .. limit 慢
```
select * from order where create_time>'2024-01-01' order by order_id desc limit 10
```

order_id 是主键，建立的索引是 asc 的。
该语句执行时需要按order_id倒序排列，不会走order_id的索引。

处理办法是：
order_id 索引按desc建立，或者按 create_time 排序，且 create_time建立desc索引



## 导出导入
https://blog.csdn.net/u011323200/article/details/143485190



https://www.jianshu.com/p/1ed3e8ae31b3


1、整库导出

[dmdba@dm8 ~]$ dexp USERID=sysdba/dameng123@localhost:5236 file=dexp.dmp DIRECTORY=/dm8  FULL=y
2、基于模式导出

[dmdba@dm8 ~]$ dexp USERID=sysdba/dameng123@localhost:5236 file=dmhr.dmp DIRECTORY=/dm8  SCHEMAS=DMHR
3、基于用户导出

[dmdba@dm8 ~]$ dexp USERID=sysdba/dameng123@localhost:5236 file=test.dmp DIRECTORY=/dm8  OWNER=TEST
4、基于表导出

[dmdba@dm8 ~]$ dexp USERID=sysdba/dameng123@localhost:5236 file=test1.dmp DIRECTORY=/dm8  TABLES=TEST.test1



整库导入

[dmdba@dm8 ~]$ dimp USERID=sysdba/dameng123@localhost:5236 file=dexp.dmp DIRECTORY  =/dm8 FULL=y
2、导入用户

[dmdba@dm8 ~]$ dimp USERID=sysdba/dameng123@localhost:5236 file=dexp.dmp DIRECTORY  =/dm8 OWNER=TEST
3、导入表

[dmdba@dm8 ~]$ dimp USERID=sysdba/dameng123@localhost:5236 file=test1.dmp DIRECTORY=/dm8  TABLES=TEST.test1
4、 基于模式导入

[dmdba@dm8 ~]$ dimp USERID=sysdba/dameng123@localhost:5236 file=dmhr.dmp DIRECTORY=/dm8 SCHEMAS=DMHR




## 备份
```
docker exec -it dm8 sh -c "cd /opt/dmdbms/bin && ./dexp USERID=sysdba/123456789@localhost:5236 file=dexp_$(date +%Y%m%d%H
%M%S).dmp DIRECTORY=/opt/dmdbms/data/DAMENG/mybak  FULL=y"
```