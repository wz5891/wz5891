## 服务器信息
[鲲鹏.png](服务器配置)

华为昆鹏920处理器
CPU
```
[root@ecs-a640 ~]# cat /etc/os-release .
NAME="openEuler"
VERSION="20.03 (LTS)"
ID="openEuler"
VERSION_ID="20.03"
PRETTY_NAME="openEuler 20.03 (LTS)"
ANSI_COLOR="0;31"

[root@ecs-a640 ~]# lscpu
Architecture:                    aarch64
CPU op-mode(s):                  64-bit
Byte Order:                      Little Endian
CPU(s):                          1
On-line CPU(s) list:             0
Thread(s) per core:              1
Core(s) per socket:              1
Socket(s):                       1
NUMA node(s):                    1
Vendor ID:                       HiSilicon
Model:                           0
Model name:                      Kunpeng-920
Stepping:                        0x1
CPU max MHz:                     2400.0000
CPU min MHz:                     2400.0000
BogoMIPS:                        200.00
L1d cache:                       64 KiB
L1i cache:                       64 KiB
L2 cache:                        512 KiB
L3 cache:                        32 MiB
NUMA node0 CPU(s):               0
Vulnerability Itlb multihit:     Not affected
Vulnerability L1tf:              Not affected
Vulnerability Mds:               Not affected
Vulnerability Meltdown:          Not affected
Vulnerability Spec store bypass: Not affected
Vulnerability Spectre v1:        Mitigation; __user pointer sanitization
Vulnerability Spectre v2:        Not affected
Vulnerability Srbds:             Not affected
Vulnerability Tsx async abort:   Not affected
Flags:                           fp asimd evtstrm aes pmull sha1 sha2 crc32 atomics fphp asimdhp cpuid asimdrdm jscvt fcma dcpop asimddp a
                                 simdfhm
[root@ecs-a640 ~]# 
```
下载地址
https://www.openeuler.org/zh/download/archive/detail/?version=openEuler%2020.03%20LTS%20SP4

## Docker 安装

https://www.hikunpeng.com/document/detail/zh/kunpengcpfs/ecosystemEnable/Docker/kunpengdocker_03_0001.html

https://blog.csdn.net/weixin_38299857/article/details/144152973


```
yumdownloader docker --resolve

docker-engine-18.09.0-202.oe1.aarch64.rpm  libcgroup-0.41-23.oe1.aarch64.rpm



rpm -Uvh *.rpm
```


```
curl -L "https://github.com/docker/compose/releases/download/v2.6.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

vim /etc/hosts
140.82.114.4 github.com

#添加执行权限
chmod +x /usr/local/bin/docker-compose
#检查docker compose版本
docker-compose version
```


echo "https://github.com/docker/compose/releases/download/v2.6.1/docker-compose-$(uname -s)-$(uname -m)"

## 镜像匹配



### MySQL5.7

```
docker pull mysql:8.4.3
```


```
docker run --rm --name my_mysql -p 3306:3306 mysql:8.4.3
```

### MongoDb4.4.5
```
docker pull mongodb/mongodb-community-server:8.0.4-ubuntu2204
```

```
docker run --rm --name my_mongodb mongodb/mongodb-community-server:8.0.4-ubuntu2204
```

### Redis5.5.0
```
docker pull redis:5.0.14
```

```
docker run --rm --name my_redis redis:5.0.14
```
启动报：<jemalloc>: Unsupported system page size

https://blog.csdn.net/toyangdon/article/details/104941161
### JDK1.8

Dockerfile
```
FROM ubuntu:20.04
LABEL authors="Jack"

RUN apt-get update && \
apt-get install -y fontconfig && \
apt-get install -y curl iputils-ping telnet unzip vim


# 清理缓存以减小镜像大小
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ADD win-fonts.tar.gz /usr/share/fonts/win
RUN fc-cache -fv && \
fc-list

ADD jdk-8u202-linux-arm64-vfp-hflt.tar.gz /usr/local/java/

ENV JAVA_HOME /usr/local/java/jdk1.8.0_202
ENV CLASSPATH .:${JAVA_HOME}/lib
ENV PATH ${JAVA_HOME}/bin:$PATH

ENV LANG zh_CN.UTF-8
ENV TIME_ZONE Asia/Shanghai

ADD arthas.tar.gz /arthas

CMD ["java","-version"]
```

```
docker build --network host  -t  openjdk:8-jdk-alpine-topsoft . 
```

```
docker run --rm --name my_java -p 8080:8080 openjdk:8-jdk-alpine-topsoft
```


JDK 8u202
‌JDK 8u202是Oracle JDK 8的最后一个免费版本‌。这意味着在JDK 8u202之后，Oracle将不再提供免费的JDK 8版本更新，

### RabbitMQ3.8.3
```
root docker pull rabbitmq:3.8.3-management
```

```
docker run --rm --name my_rabbitmq -p 8080:8080 rabbitmq:3.8.3-management
```

### Nginx1.25.2
```
docker pull nginx:1.26.2
docker run --rm --name my_nginx -p 80:80 nginx:1.26.2
```
