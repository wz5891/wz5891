！！！！！loongarch 镜像无法安装！！！！！

## 软件与网络
参见：./qemu-setup.md


## 创建虚拟磁盘
在 cmd 窗口中输入
```
qemu-img create -f qcow2 kylin.img 30G
```
qcow2 是一种支持快照和压缩等高级特性的虚拟磁盘格式，通常用于虚拟机的存储。


## 创建虚拟机
```
qemu-system-loongarch64 -m 4096 -cpu la464 -smp 4,cores=4,threads=1,sockets=1 -M virt -bios QEMU_EFI_7.2.fd -net nic -net tap,ifname=tap0 -device nec-usb-xhci -device usb-kbd -device usb-mouse -device VGA -drive if=none,file=Kylin-Server-V10-SP3-General-Release-2303-LoongArch64.iso,id=cdrom,media=cdrom -device virtio-scsi-device -device scsi-cd,drive=cdrom -drive if=none,file=kylin.img,id=hd0 -device virtio-blk-device,drive=hd0
```


qemu-system-aarch64.exe -m 8192 -cpu cortex-a72 -smp 4,cores=4,threads=1,sockets=1 -M virt -bios D:\qemu\kylinv10\QEMU_EFI.fd -net nic -net tap,ifname=tap0 -device nec-usb-xhci -device usb-kbd -device usb-mouse -device VGA -drive if=none,file=D:\OS\Kylin\ARM\Kylin-Desktop-V10-SP1-General-Release-2303-ARM64.iso,id=cdrom,media=cdrom -device virtio-scsi-device -device scsi-cd,drive=cdrom -drive if=none,file=D:\qemu\kylinv10\Kylin-Desktop-10-SP1-ARM64.img,id=hd0 -device virtio-blk-device,drive=hd0

# -m 4196 表示分配给虚拟机的内存最大4196MB，可以直接使用 -m 4G

# -cpu la464 指定CPU类型，还可以选择 la132，max 等
```
// 查看cpu类型
qemu-system-loongarch64 -cpu help
la132-loongarch-cpu
la464-loongarch-cpu
max-loongarch-cpu
```

# -smp 4,cores=4,threads=1,sockets=1 指定虚拟机最大使用的CPU核心数等

# -M virt 指定虚拟机类型为virt，具体支持的类型可以使用 qemu-system-aarch64 -M help 查看

# -bios D:\qemu\kylinv10\QEMU_EFI.fd 指定UEFI固件文件

# -net nic -net tap,ifname=tap0 启用网络功能（ifname=tap0中的tap0，修改为前面步骤中自己修改后的网卡名称）

# -device nec-usb-xhci -device usb-kbd -device usb-mouse 启用USB鼠标等设备

# -device VGA 启用VGA视图，对于图形化的Linux这条很重要！

# -drive if=none,file=D:\OS\Kylin\ARM\Kylin-Desktop-V10-SP1-General-Release-2303-ARM64.iso,id=cdrom,media=cdrom 指定光驱使用镜像文件

# -device virtio-scsi-device -device scsi-cd,drive=cdrom 指定光驱硬件类型

# -drive if=none,file=D:\qemu\kylinv10\\Kylin-Desktop-10-SP1-ARM64.img 指定硬盘镜像文件


## 再次启动虚拟机
无需再次指定iso文件启动
```
qemu-system-aarch64 -m 8192 -cpu cortex-a72 -smp 4,cores=4,threads=1,sockets=1 -M virt -bios D:\qemu\kylinv10\QEMU_EFI.fd -net nic -net tap,ifname=tap0 -device nec-usb-xhci -device usb-kbd -device usb-mouse -device VGA -drive if=none,file=,id=cdrom,media=cdrom -device virtio-scsi-device -device scsi-cd,drive=cdrom -drive if=none,file=D:\qemu\kylinv10\Kylin-Desktop-10-SP1-ARM64.img,id=hd0 -device virtio-blk-device,drive=hd0
```



## 安装 ssh 的过程
```
sudo apt-get install openssh-server
```
E:dpkg 被中断，您必须手工运行'sudo dpkg --configure -a' 解决此问题。

## 参考
小白也能懂之如何在自己的Windows电脑上使用QEMU虚拟机启动龙芯Loongnix系统的操作办法
https://www.modb.pro/db/1733672145120206848



Windows系统x86机器安装龙芯（loongarch64）3A5000虚拟机系统详细教程
https://blog.csdn.net/u012402739/article/details/136324338



windows qemu安装飞腾Aarch64 Loongarch64 操作系统 亲测
https://blog.csdn.net/kchmmd/article/details/134077508