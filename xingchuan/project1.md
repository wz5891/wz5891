# 服务器配置
## 服务器品牌
清华同方

## CPU
品牌:龙芯，
型号:LS 3C5000L，
数量:2颗，
参数：16核，2.2GHZ


## 内存
品牌：紫光，
型号:SCC32GP13H4F1C-32AA，
数量:4条，
参数：32GB DDR4 ECC

## 操作系统
银河麒麟高级服务器操作系统(国防版)V10

## 硬盘
固态硬盘：华电DGD67256S3C27 256GB 2.5寸 SAA SSD(主控芯片为GK2302H，全盘写次数>=3000次)，数量为1块
机械硬盘:希捷 ST2000NM000B SATA 3.0 6Gb/S 2TB 3.5寸，数量为2块

# CPU
龙芯3C5000L是龙芯中科专门面向服务器领域的通用处理器。基于龙芯3A5000处理器，片上集成共16个高性能LA464处理器核，采用全新的龙芯自主指令系统（LoongArch®），在提高集成度的同时保持系统和软件与龙芯3A5000完全兼容。
64位超标量处理器核LA464； 支持LoongArch ®指令集； 支持128/256位向量指令； 四发射乱序执行； 4个定点单元、2个向量单元和2个访存单元。


前几天（2021-04-20），龙芯宣布推出LoongArch指令集， 放弃了以往的MIPS授权，拥有2500多条自主指令，还可以翻译MIPS、ARM及x86指令。


LoongArch对MIPS指令的翻译效率是100%，对ARM可以达到90%。
最难的当属x86，在Linux下翻译的效率可达80%，Windows下的效率还要减少到70%，不过后续还会有更多的优化。


LoongArch是一种新的RISC ISA，在一定程度上类似于MIPS和RISC-V。LoongArch指令集 包括一个精简32位版（LA32R）、一个标准32位版（LA32S）、一个64位版（LA64）。 LoongArch定义了四个特权级（PLV0~PLV3），其中PLV0是最高特权级，用于内核；而PLV3 是最低特权级，用于应用程序。本文档介绍了LoongArch的寄存器、基础指令集、虚拟内 存以及其他一些主题。



LoongArch是一种RISC指令集架构（ISA），不同于现存的任何一种ISA，而Loongson（即龙 芯）是一个处理器家族。龙芯包括三个系列：Loongson-1（龙芯1号）是32位处理器系列， Loongson-2（龙芯2号）是低端64位处理器系列，而Loongson-3（龙芯3号）是高端64位处理 器系列。旧的龙芯处理器基于MIPS架构，而新的龙芯处理器基于LoongArch架构。以龙芯3号 为例：龙芯3A1000/3B1500/3A2000/3A3000/3A4000都是兼容MIPS的，而龙芯3A5000（以及将 来的型号）都是基于LoongArch的。



我没有 LoongArch 硬件，我的软件该怎么测试？
大部分情况下，您可以使用 QEMU 完成测试。 系统级模拟（模拟一台完整的龙芯架构计算机）与用户态模拟（基于当前宿主系统的 Linux 内核提供一个龙芯架构的 Linux 系统调用界面）均可以支持。 QEMU 的使用方法不属于本文范畴，请参考其他在线资料。

注：截至 2022-07-23，LoongArch 的 target 支持已经完整合入 QEMU 主线，但 7.1 版本仍然存在一些 bugs，以至于这个版本的 LoongArch linux-user 模拟实质上不可用， 系统模拟也受到一定影响。 QEMU 7.2 应该可以开箱即用。


LoongArch 常见问题解答
https://blog.xen0n.name/posts/tinkering/loongarch-faq/



# docker
龙芯软件仓库
http://www.loongnix.cn/zh/


龙芯系统loongarch64安装 docker
https://zhuanlan.zhihu.com/p/683041325


# 下载镜像时指定平台
```
docker pull --platform=linux/loongarch cr.loongnix.cn/library/nginx:1.24.0
docker pull --platform=linux/loongarch cr.loongnix.cn/library/mysql:5.7.39
docker pull --platform=linux/loongarch cr.loongnix.cn/library/mongo:4.4.5
docker pull --platform=linux/loongarch cr.loongnix.cn/library/redis:5.0.14
docker pull --platform=linux/loongarch cr.loongnix.cn/library/rabbitmq:3.8.18-management
docker pull --platform=linux/loongarch cr.loongnix.cn/library/openjdk:8u352
docker pull --platform=linux/loongarch cr.loongnix.cn/library/tomcat:9.0.45
```


基于 Windows 平台利用 QEMU 部署 ARM 架构的虚拟机
https://www.modb.pro/db/1733672145120206848



# paddle
https://www.bilibili.com/read/cv27939940/

龙芯的 Pypi 源就配置好了。其中https://pypi.loongnix.cn/loongson/pypi是龙芯的附加源，https://pypi.tuna.tsinghua.edu.cn/simple是清华的Pypi源。




飞腾/鲲鹏 CPU 安装说明
https://www.paddlepaddle.org.cn/inference/guides/hardware_support/cpu_phytium_cn.html

龙芯 CPU 安装说明
https://www.paddlepaddle.org.cn/lite/v2.10/guides/hardware_support/cpu_loongson_cn.html


centos 8.5 x86_64平台 通过qemu-user-static运行arm64 docker 制作 银河麒麟操作系统国产化 v10 arm64 镜像
https://blog.csdn.net/tonyhi6/article/details/139482873

qemu-user-static：利用x86机器编译支持arm架构的docker镜像
https://blog.csdn.net/ccgshigao/article/details/109631585

x86 平台利用 qemu-user-static 实现 arm64 平台 docker 的运行和构建
https://blog.csdn.net/edcbc/article/details/139366049



Windows系统x86机器安装龙芯（loongarch64）3A5000虚拟机系统详细教程
https://blog.csdn.net/u012402739/article/details/136324338




1. 安装¶
1.1 安装PaddlePaddle¶
如果您没有基础的Python运行环境，请参考运行环境准备。

您的机器安装的是CUDA 11，请运行以下命令安装
pip install paddlepaddle-gpu
您的机器是CPU，请运行以下命令安装
pip install paddlepaddle
更多的版本需求，请参照飞桨官网安装文档中的说明进行操作。

1.2 安装PaddleOCR whl包¶
pip install paddleocr
对于Windows环境用户：直接通过pip安装的shapely库可能出现[winRrror 126] 找不到指定模块的问题。建议从这里下载shapely安装包完成安装。


## python

loongson@loongson-pc:~$ mkdir -p ~/.pip
loongson@loongson-pc:~$ touch ~/.pip/pip.conf
loongson@loongson-pc:~$ cat ~/.pip/pip.conf
[global]
timeout = 60
index-url = https://pypi.loongnix.cn/loongson/pypi
extra-index-url = https://pypi.tuna.tsinghua.edu.cn/simple
[install]
trusted-host = 
    pypi.loongnix.cn
    pypi.tuna.tsinghua.edu.cn


pacman -S python-pip