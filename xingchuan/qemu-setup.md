## 软件
首先需要下载一个qemu7.2，全称为：qemu-w64-setup-20221230.exe，下载地址为：https://qemu.weilnetz.de/w64/2022/
之后需要下载EFI驱动，全称为：QEMU_EFI_7.2.fd，下载地址为：https://mirrors.wsyu.edu.cn/loongarch/archlinux/images/
最后我们需要下载loongnix，全称为：Loongnix-20.3.mate.gui.loongarch64.cn.qcow2，下载地址为：https://pkg.loongnix.cn/loongnix/isos/Loongnix-20.3/


https://pkg.loongnix.cn/loongnix/isos/Loongnix-20.3/Loongnix-20.3.mate.gui.loongarch64.cn.qcow2
https://mirrors.wsyu.edu.cn/loongarch/archlinux/images/QEMU_EFI_7.2.fd
https://qemu.weilnetz.de/w64/2022/qemu-w64-setup-20221230.exe

## 网络
参照 https://www.modb.pro/db/1733672145120206848 文章中，添加 tap0 网卡，并将物理网卡共享给 tap0


