中科方德 LS 3C5000L

CPU系列：Loongson-64bit
CPU型号：Loongson-3C5000L

## 安装 docker
```
yum install docker
```
## 安装 docker-compose
```
pip install docker-compose
```

## 镜像安装

### docker
龙芯软件仓库
http://www.loongnix.cn/zh/


### 下载镜像时指定平台
```
docker pull --platform=linux/loongarch cr.loongnix.cn/library/nginx:1.24.0
docker pull --platform=linux/loongarch cr.loongnix.cn/library/mysql:5.7.39
docker pull --platform=linux/loongarch cr.loongnix.cn/library/mongo:4.4.5
docker pull --platform=linux/loongarch cr.loongnix.cn/library/redis:5.0.14
docker pull --platform=linux/loongarch cr.loongnix.cn/library/rabbitmq:3.8.18-management
docker pull --platform=linux/loongarch cr.loongnix.cn/library/openjdk:8u352
docker pull --platform=linux/loongarch cr.loongnix.cn/library/tomcat:9.0.45
```
